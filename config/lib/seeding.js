'use strict';

var fs = require('fs'),
  _ = require('lodash'),
  async = require('async'),
  mongoose = require('mongoose'),
  admin = null;

module.exports.start = function start() {
  async.autoInject({
    users: users,
    sequences: sequences,
    references: references,
    combinations: combinations,
    taxes: taxes,
    suppliers: suppliers,
    customers: customers,
    items: items,
    banks: banks,
    checks: checks,
    calendar: calendar,
    company: company
  }, function(err) {
    if (err) {
      console.log('Database Seeding:\t\t\t' + err);
    } else {
      console.log('Database Seeding:\t\t\tSuccess');
    }
  });
};

function users(done) {
  var results = [];
  var User = mongoose.model('User');
  var data = fs.readFileSync('../data/users.json', 'utf8');
  var users = JSON.parse(data);

  async.each(users, function(u, next) {
    var user = new User(u);
    if (user.username === 'administrator') {
      admin = user;
    }
    user.user = admin;
    user.save(function(err) {
      if (err) {
        return next(err);
      }
      results.push(user);
      next();
    });
  }, function(err) {
    if (err) {
      console.log('Database Seeding:\t\t\tError seeding users');
      return done(err);
    }
    done(null, results);
  });
}

function sequences(done) {
  var results = [];
  var Sequence = mongoose.model('Sequence');
  var data = fs.readFileSync('../data/sequences.json', 'utf8');
  var sequences = JSON.parse(data);

  async.each(sequences, function(s, next) {
    var sequence = new Sequence(s);
    sequence.save(function(err) {
      if (err) {
        return next(err);
      }
      results.push(user);
      next();
    });
  }, function(err) {
    if (err) {
      console.log('Database Seeding:\t\t\tError seeding sequences');
      return done(err);
    }
    done(null, results);
  });
}