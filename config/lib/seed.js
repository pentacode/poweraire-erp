'use strict';

var _ = require('lodash'),
  config = require('../config'),
  mongoose = require('mongoose'),
  async = require('async');

// global seed options object
var data = {};

module.exports.start = function start() {
  // Deep clone data from config
  data = _.clone(config.seedDB.data, true);

  return new Promise(function (resolve, reject) {
    seedUsers(data.users)
      .then(seedSequences(data.sequences))
      .then(seedReferences(data.references))
      .then(seedCompanyProfile(data.companyProfile))
      .then(function(){
        console.log('Database Seeding:\t\t\tSuccessful');
        resolve();
      })
      .catch(function(err){
        console.log('Database Seeding:\t\t\t' + err);
        reject();
      });
  });
};

function seedUsers(users) {
  return new Promise(function(resolve, reject){
    if (users && users.length > 0) {
      var User = mongoose.model('User');
      async.each(users, function(u, next){
        var user = new User(u);
        user.save(function(err){
          if (err) {
            next(err);
          } else {
            console.log('Database Seeding:\t\t\tUser ' + user.username + ' added');
            next();
          }
        });
      }, function(err){
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    } else {
      console.log('Database Seeding:\t\t\tNo user seeded');
      resolve();
    }
  });
}

function seedSequences(sequences) {
  return function() {
    return new Promise(function(resolve, reject){
      if (sequences && sequences.length > 0) {
        var Sequence = mongoose.model('Sequence');
        async.each(sequences, function(s, next){
          var sequence = new Sequence(s);
          sequence.save(function(err){
            if (err) {
              next(err);
            } else {
              console.log('Database Seeding:\t\t\tSequence ' + sequence.model + ' added');
              next();
            }
          });
        }, function(err){
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        console.log('Database Seeding:\t\t\tNo sequence seeded');
        resolve();
      }
    });
  };
}

function seedReferences(references) {
  return function() {
    return new Promise(function(resolve, reject){
      if (references && references.length > 0) {
        var Reference = mongoose.model('Reference');
        async.each(references, function(r, next) {
          var reference = new Reference(r);
          reference.save(function(err){
            if (err) {
              next(err);
            } else {
              console.log('Database Seeding:\t\t\tReference ' + reference.code + '-' + reference.value + '-' + reference.descriptionOne + ' added');
              next();
            }
          });
        }, function(err){
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        console.log('Database Seeding:\t\t\tNo reference seeded');
        resolve();
      }
    });
  };
}

function seedCompanyProfile(companyProfile) {
  return function() {
    return new Promise(function(resolve, reject) {
      if (companyProfile) {
        var Company = mongoose.model('Company');
        var company = new Company(companyProfile);
        company.save(function(err){
          if (err) {
            reject(err);
          } else {
            console.log('Database Seeding:\t\t\tCompany Profile seeded');
            resolve();
          }
        });
      } else {
        console.log('Database Seeding:\t\t\tNo company profile seeded');
        resolve();
      }
    });
  };
}
