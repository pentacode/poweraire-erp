'use strict';

var cfenv = require('cfenv'),
  appEnv = cfenv.getAppEnv();
var cfMongoUrl = (function() {
  if (appEnv.getService('poweraire-erp')) {
    var mongoCreds = appEnv.getService('poweraire-erp').credentials;
    return mongoCreds.uri || mongoCreds.url;
  } else {
    throw new Error('No service names "poweraire-erp" bound to the application.');
  }
}());

var getCred = function (serviceName, credProp) {
  return appEnv.getService(serviceName) ?
    appEnv.getService(serviceName).credentials[credProp] : undefined;
};

module.exports = {
  port: appEnv.port,
  db: {
    uri: cfMongoUrl,
    options: {
      user: '',
      pass: ''
    }
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // By default we want logs to go to process.out so the Cloud Foundry Loggregator will collect them
    options: {}
  },
  facebook: {
    clientID: getCred('mean-facebook', 'id') || 'APP_ID',
    clientSecret: getCred('mean-facebook', 'secret') || 'APP_SECRET',
    callbackURL: '/api/auth/facebook/callback'
  },
  twitter: {
    clientID: getCred('mean-twitter', 'key') || 'CONSUMER_KEY',
    clientSecret: getCred('mean-twitter', 'secret') || 'CONSUMER_SECRET',
    callbackURL: '/api/auth/twitter/callback'
  },
  google: {
    clientID: getCred('mean-google', 'id') || 'APP_ID',
    clientSecret: getCred('mean-google', 'secret') || 'APP_SECRET',
    callbackURL: '/api/auth/google/callback'
  },
  linkedin: {
    clientID: getCred('mean-linkedin', 'id') || 'APP_ID',
    clientSecret: getCred('mean-linkedin', 'secret') || 'APP_SECRET',
    callbackURL: '/api/auth/linkedin/callback'
  },
  github: {
    clientID: getCred('mean-github', 'id') || 'APP_ID',
    clientSecret: getCred('mean-github', 'secret') || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback'
  },
  paypal: {
    clientID: getCred('mean-paypal', 'id') || 'CLIENT_ID',
    clientSecret: getCred('mean-paypal', 'secret') || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: false
  },
  mailer: {
    from: getCred('mean-mail', 'from') || 'MAILER_FROM',
    options: {
      service: getCred('mean-mail', 'service') || 'MAILER_SERVICE_PROVIDER',
      auth: {
        user: getCred('mean-mail', 'username') || 'MAILER_EMAIL_ID',
        pass: getCred('mean-mail', 'password') || 'MAILER_PASSWORD'
      }
    }
  },
  seedDB: {
    seed: false,
    data: {
      users: [{
        username: 'admin',
        password: 'admin',
        provider: 'local',
        email: 'admin@poweraire.com.ph',
        firstName: 'Admin',
        lastName: 'Local',
        displayName: 'Admin Local',
        roles: ['Admin', 'Bank Accounts', 'Checks', 'Combinations', 'Company Profile', 'Customers', 'Items', 'References', 'Suppliers', 'Taxes', 'Users', 'On-hand Quantity', 'Transactions', 'Purchase Orders', 'Receipts', 'Invoices'],
        status: true
      }],
      sequences: [{
        model: 'Purchase Order',
        value: 1
      }, {
        model: 'Receipt',
        value: 1
      }, {
        model: 'Customer',
        value: 1
      }, {
        model: 'Supplier',
        value: 1
      },{
        model: 'Transaction',
        value: 1
      }],
      references: [{
        code: 'COUNTRY',
        value: 'PH',
        descriptionOne: 'Philippines',
        status: true
      }, {
        code: 'COUNTRY',
        value: 'US',
        descriptionOne: 'United States',
        status: true
      }, {
        code: 'CURRENCY',
        value: 'PHP',
        descriptionOne: 'Philippines Peso',
        status: true
      }, {
        code: 'CURRENCY',
        value: 'USD',
        descriptionOne: 'United States Dollar',
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Poweraire Engineering',
        segmentNumber: 1,
        status: true
      }, {
        code: 'COMBINATION',
        value: '001',
        descriptionOne: 'Head Office',
        segmentNumber: 2,
        status: true
      }, {
        code: 'COMBINATION',
        value: '001',
        descriptionOne: 'Installation',
        segmentNumber: 3,
        status: true
      }, {
        code: 'COMBINATION',
        value: '002',
        descriptionOne: 'Repair',
        segmentNumber: 3,
        status: true
      }, {
        code: 'COMBINATION',
        value: '1000',
        descriptionOne: 'Accounts Payable',
        segmentNumber: 4,
        accountType: 'Liability',
        status: true
      }, {
        code: 'COMBINATION',
        value: '2000',
        descriptionOne: 'Accounts Receivable',
        segmentNumber: 4,
        accountType: 'Asset',
        status: true
      }, {
        code: 'COMBINATION',
        value: '3000',
        descriptionOne: 'Cash in Bank',
        accountType: 'Asset',
        segmentNumber: 4,
        status: true
      }, {
        code: 'COMBINATION',
        value: '10001',
        descriptionOne: 'AP Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '10002',
        descriptionOne: 'AP Non Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '20001',
        descriptionOne: 'AR Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '20002',
        descriptionOne: 'AR Non Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '30001',
        descriptionOne: 'BDO',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '30002',
        descriptionOne: 'BPI',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Future Use',
        segmentNumber: 6,
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Future Use',
        segmentNumber: 7,
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Tenants',
        descriptionOne: 'Tenants',
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Card Customers',
        descriptionOne: 'Card Customers',
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Installation Customers',
        descriptionOne: 'Installation Customers',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Trade',
        descriptionOne: 'Trade',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Non-trade',
        descriptionOne: 'Non-trade',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Services',
        descriptionOne: 'Services',
        status: true
      }, {
        code: 'UOM',
        value: 'PCS',
        descriptionOne: 'Piece/s',
        status: true
      }, {
        code: 'UOM',
        value: 'BOX',
        descriptionOne: 'Box/es',
        status: true
      }, {
        code: 'UOM',
        value: 'UNIT',
        descriptionOne: 'Unit/s',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '15 Net Days',
        descriptionOne: '15 Net Days',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '30 Net Days',
        descriptionOne: '30 Net Days',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '45 Net Days',
        descriptionOne: '45 Net Days',
        status: true
      }, {
        code: 'PAYMENT_METHOD',
        value: 'Cash',
        descriptionOne: 'Cash',
        status: true
      }, {
        code: 'PAYMENT_METHOD',
        value: 'Check',
        descriptionOne: 'Check',
        status: true
      }, {
        code: 'CATEGORY',
        value: 'NON_TRADE.OFFICE_TOOLS.BOND_PAPERS',
        descriptionOne: 'Non-trade',
        descriptionTwo: 'Office Tools',
        descriptionThree: 'Bond Papers',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW1',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 1',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW2',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 2',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW3',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 3',
        status: true
      }],
      companyProfile: {
        name: 'Poweraire Engineering Services',
        address: '#35 Wilson St. Greenhills, San Juan (near Cardinal Santos)',
        phone: '584-2522; 584-2366; 0922-317-4567',
        tin: '000-000-000-000',
        fax: '584-2032'
      }
    }
  }
};
