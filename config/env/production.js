'use strict';

module.exports = {
  secure: {
    ssl: true,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.pem'
  },
  port: process.env.PORT || 8443,
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/mean',
    options: {
      user: '',
      pass: ''
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: process.env.LOG_FORMAT || 'combined',
    options: {
      // Stream defaults to process.stdout
      // Uncomment/comment to toggle the logging to a log on the file system
      stream: {
        directoryPath: process.env.LOG_DIR_PATH || process.cwd(),
        fileName: process.env.LOG_FILE || 'access.log',
        rotatingLogs: { // for more info on rotating logs - https://github.com/holidayextras/file-stream-rotator#usage
          active: process.env.LOG_ROTATING_ACTIVE === 'true' ? true : false, // activate to use rotating logs
          fileName: process.env.LOG_ROTATING_FILE || 'access-%DATE%.log', // if rotating logs are active, this fileName setting will be used
          frequency: process.env.LOG_ROTATING_FREQUENCY || 'daily',
          verbose: process.env.LOG_ROTATING_VERBOSE === 'true' ? true : false
        }
      }
    }
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'APP_ID',
    clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/facebook/callback'
  },
  twitter: {
    clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
    clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
    callbackURL: '/api/auth/twitter/callback'
  },
  google: {
    clientID: process.env.GOOGLE_ID || 'APP_ID',
    clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/google/callback'
  },
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'APP_ID',
    clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/linkedin/callback'
  },
  github: {
    clientID: process.env.GITHUB_ID || 'APP_ID',
    clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback'
  },
  paypal: {
    clientID: process.env.PAYPAL_ID || 'CLIENT_ID',
    clientSecret: process.env.PAYPAL_SECRET || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: false
  },
  mailer: {
    from: process.env.MAILER_FROM || 'MAILER_FROM',
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
        pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
      }
    }
  },
  seedDB: {
    seed: true,
    data: {
      users: [{
        username: 'admin',
        password: 'admin',
        provider: 'local',
        email: 'admin@poweraire.com.ph',
        firstName: 'Admin',
        lastName: 'Local',
        displayName: 'Admin Local',
        roles: ['Admin', 'Bank Accounts', 'Calendar', 'Checks', 'Combinations', 'Company Profile', 'Customers', 'Items', 'References', 'Suppliers', 'Taxes', 'Users', 'On-hand Quantity', 'Transactions', 'Purchase Orders', 'Receipts', 'Invoices', 'Journal Vouchers', 'General Ledger'],
        status: true
      }],
      sequences: [{
        model: 'Purchase Order',
        value: 1
      }, {
        model: 'JournalVoucher',
        value: 1
      },{
        model: 'Receipt',
        value: 1
      }, {
        model: 'Customer',
        value: 1
      }, {
        model: 'Supplier',
        value: 1
      },{
        model: 'Transaction',
        value: 1
      },{
        model: 'BatchInventory',
        value: 1
      },{
        model: 'BatchGeneralLedger',
        value: 1
      },{
        model: 'BatchAccPayable',
        value: 1
      },{
        model: 'BatchAccReceivable',
        value: 1
      },{
        model: 'BatchPurchasing',
        value: 1
      },{
        model: 'JournalVoucherBatchSchema',
        value: 1
      }],
      references: [{
        code: 'COUNTRY',
        value: 'PH',
        descriptionOne: 'Philippines',
        status: true
      }, {
        code: 'COUNTRY',
        value: 'US',
        descriptionOne: 'United States',
        status: true
      }, {
        code: 'CURRENCY',
        value: 'PHP',
        descriptionOne: 'Philippines Peso',
        status: true
      }, {
        code: 'CURRENCY',
        value: 'USD',
        descriptionOne: 'United States Dollar',
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Poweraire Engineering',
        segmentNumber: 1,
        status: true
      }, {
        code: 'COMBINATION',
        value: '001',
        descriptionOne: 'Head Office',
        segmentNumber: 2,
        status: true
      }, {
        code: 'COMBINATION',
        value: '001',
        descriptionOne: 'Installation',
        segmentNumber: 3,
        status: true
      }, {
        code: 'COMBINATION',
        value: '002',
        descriptionOne: 'Repair',
        segmentNumber: 3,
        status: true
      }, {
        code: 'COMBINATION',
        value: '1000',
        descriptionOne: 'Accounts Payable',
        segmentNumber: 4,
        accountType: 'Liability',
        status: true
      }, {
        code: 'COMBINATION',
        value: '2000',
        descriptionOne: 'Accounts Receivable',
        segmentNumber: 4,
        accountType: 'Asset',
        status: true
      }, {
        code: 'COMBINATION',
        value: '3000',
        descriptionOne: 'Cash in Bank',
        accountType: 'Asset',
        segmentNumber: 4,
        status: true
      }, {
        code: 'COMBINATION',
        value: '10001',
        descriptionOne: 'AP Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '10002',
        descriptionOne: 'AP Non Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '20001',
        descriptionOne: 'AR Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '20002',
        descriptionOne: 'AR Non Trade',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '30001',
        descriptionOne: 'BDO',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '30002',
        descriptionOne: 'BPI',
        segmentNumber: 5,
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Future Use',
        segmentNumber: 6,
        status: true
      }, {
        code: 'COMBINATION',
        value: '01',
        descriptionOne: 'Future Use',
        segmentNumber: 7,
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Tenants',
        descriptionOne: 'Tenants',
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Card Customers',
        descriptionOne: 'Card Customers',
        status: true
      }, {
        code: 'CUSTOMER_TYPE',
        value: 'Installation Customers',
        descriptionOne: 'Installation Customers',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Trade',
        descriptionOne: 'Trade',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Non-trade',
        descriptionOne: 'Non-trade',
        status: true
      }, {
        code: 'SUPPLIER_TYPE',
        value: 'Services',
        descriptionOne: 'Services',
        status: true
      }, {
        code: 'UOM',
        value: 'PCS',
        descriptionOne: 'Piece/s',
        status: true
      }, {
        code: 'UOM',
        value: 'BOX',
        descriptionOne: 'Box/es',
        status: true
      }, {
        code: 'UOM',
        value: 'UNIT',
        descriptionOne: 'Unit/s',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '15 Net Days',
        descriptionOne: '15 Net Days',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '30 Net Days',
        descriptionOne: '30 Net Days',
        status: true
      }, {
        code: 'PYMNT_TERM',
        value: '45 Net Days',
        descriptionOne: '45 Net Days',
        status: true
      }, {
        code: 'PAYMENT_METHOD',
        value: 'Cash',
        descriptionOne: 'Cash',
        status: true
      }, {
        code: 'PAYMENT_METHOD',
        value: 'Check',
        descriptionOne: 'Check',
        status: true
      }, {
        code: 'CATEGORY',
        value: 'NON_TRADE.OFFICE_TOOLS.BOND_PAPERS',
        descriptionOne: 'Non-trade',
        descriptionTwo: 'Office Tools',
        descriptionThree: 'Bond Papers',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW1',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 1',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW2',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 2',
        status: true
      }, {
        code: 'SUBINV',
        value: 'WH1.SHELF1.ROW3',
        descriptionOne: 'Warehouse 1',
        descriptionTwo: 'Shelf 1',
        descriptionThree: 'Row 3',
        status: true
      }],
      companyProfile: {
        name: 'Poweraire Engineering Services',
        address: '#35 Wilson St. Greenhills, San Juan (near Cardinal Santos)',
        phone: '584-2522; 584-2366; 0922-317-4567',
        tin: '000-000-000-000',
        fax: '584-2032'
      }
    }
  }
};
