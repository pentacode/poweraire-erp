'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Company Schema
 */
var CompanySchema = new Schema({
  name: {
    type: String,
    required: 'Company name is required.'
  },
  address: {
    type: String,
    required: 'Company address is required.'
  },
  phone: {
    type: String,
    required: 'Company phone number is required.'
  },
  tin: { 
    type: String,
    required: 'TIN is required.'
  },
  fax: {
    type: String,
    required: 'Company fax number is required.'
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

CompanySchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate; 
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  next();
});

mongoose.model('Company', CompanySchema);
