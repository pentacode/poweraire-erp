'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Company = mongoose.model('Company'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Company
 */
exports.create = function(req, res) {
  var company = new Company(req.body);
  company.user = req.user;

  company.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(company);
    }
  });
};

/**
 * Show the current Company
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var company = req.company ? req.company.toJSON() : {};
  res.jsonp(company);
};

/**
 * Update a Company
 */
exports.update = function(req, res) {
  var company = req.company;
  company.user = req.user;

  company = _.extend(company , req.body);

  company.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(company);
    }
  });
};

/**
 * List of Companies
 */
exports.list = function(req, res) { 
  Company.find().sort('-created').exec(function(err, companies) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(companies);
    }
  });
};

/**
 * Company middleware
 */
exports.companyByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    Company.findOne(function (err, company) {
      if (err) {
        return next(err);
      } else {
        req.company = company;
        next();
      }
    })
    .populate('createdBy','username')
    .populate('updatedBy','username');
  }else{
    Company.findById(id).populate('createdBy','username')
    .populate('updatedBy','username')
    .exec(function (err, company) {
      if (err) {
        return next(err);
      } else if (!company) {
        return res.status(404).send({
          message: 'No Company with that identifier has been found'
        });
      }
      req.company = company;
      next();
    });
  }

  
};