(function (app) {
  'use strict';

  app.registerModule('companies');
  app.registerModule('companies.admin', ['core.admin']);
})(ApplicationConfiguration);
