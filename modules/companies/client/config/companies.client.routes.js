(function () {
  'use strict';

  angular
    .module('companies')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('companies', {
        // abstract: true,
        url: '/companies',
        controller: 'CompaniesController',
        controllerAs: 'vm',
        templateUrl: 'modules/companies/client/views/form-company.client.view.html',
        resolve: {
          companyResolve: getCompany
        },
        data: {
          roles: ['Company Profile'],
          pageTitle: 'Edit Company {{ companyResolve.name }}'
        }
      });
      // .state('companies.list', {
      //   url: '',
      //   templateUrl: 'modules/companies/client/views/list-companies.client.view.html',
      //   controller: 'CompaniesListController',
      //   controllerAs: 'vm',
      //   data: {
      //     roles: ['Maintenance'],
      //     pageTitle: 'Companies List'
      //   }
      // })
      // .state('companies.create', {
      //   url: '/create',
      //   templateUrl: 'modules/companies/client/views/form-company.client.view.html',
      //   controller: 'CompaniesController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     companyResolve: newCompany
      //   },
      //   data: {
      //     roles: ['Maintenance'],
      //     pageTitle : 'Companies Create'
      //   }
      // })
      // .state('companies.edit', {
      //   url: '/:companyId/edit',
      //   templateUrl: 'modules/companies/client/views/form-company.client.view.html',
      //   controller: 'CompaniesController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     companyResolve: getCompany
      //   },
      //   data: {
      //     roles: ['Maintenance'],
      //     pageTitle: 'Edit Company {{ companyResolve.name }}'
      //   }
      // });
  }

  getCompany.$inject = ['$stateParams', 'CompaniesService'];

  function getCompany($stateParams, CompaniesService) {
    return CompaniesService.get({
      companyId : 0
    }).$promise;
  }

  newCompany.$inject = ['CompaniesService'];

  function newCompany(CompaniesService) {
    return new CompaniesService();
  }
})();
