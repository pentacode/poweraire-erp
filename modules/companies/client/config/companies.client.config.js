(function () {
  'use strict';

  angular
    .module('companies.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Company Profile',
      state: 'companies',
      roles: ['Company Profile']
    });
  }
})();
