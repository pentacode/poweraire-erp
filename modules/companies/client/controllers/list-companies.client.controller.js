(function () {
  'use strict';

  angular
    .module('companies')
    .controller('CompaniesListController', CompaniesListController);

  CompaniesListController.$inject = ['$scope', '$filter','CompaniesService'];

  function CompaniesListController($scope, $filter, CompaniesService) {
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.companies, function(item){
        if ($scope.name && !(new RegExp('^' + $scope.name).test(item.name))) { //prefix search
          return false;
        }
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.name = '';
      $scope.figureOutItemsToDisplay();
    };

    $scope.searchName = function () {
      $scope.ns = [];
      $scope.ns = $filter('filter')($scope.names, function(name){
        if ($scope.name && !(new RegExp($scope.name, 'i').test(name))) {
          return false;
        }
        return true;
      });
    };

    CompaniesService.query(function(data){
      $scope.names = [];
      $scope.companies = data;
      for (var i = 0; i < $scope.companies.length; i++) {
        var company = $scope.companies[i];
        if($scope.names.indexOf(company.name) === -1){
          $scope.names.push(company.name);
        }
      }
      $scope.buildPager();
    });
  }
})();
