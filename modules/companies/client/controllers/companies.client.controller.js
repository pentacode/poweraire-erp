(function () {
  'use strict';

  // Companies controller
  angular
    .module('companies')
    .controller('CompaniesController', CompaniesController);

  CompaniesController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'companyResolve', 'alertify', 'Util','CombinationsService','$modal'];

  function CompaniesController ($scope, $filter, $state, Authentication, company, alertify, Util,CombinationsService,$modal) {
    var vm = this;
    vm.accountCodes = [];
    vm.authentication = Authentication;
    vm.company = company;
    vm.error = null;
    vm.form = {};
    vm.openActionCodeLookup = openActionCodeLookup;
    vm.save = save;

    $scope.$on('$stateChangeStart', function(event, toState) {
      if(vm.form.companyForm.$dirty){
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function() {
          vm.form.companyForm.$dirty = false;
          $state.go(toState);
        });
      }
    });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.company.accountCode = selected.combination._id;
        }, null);

    }

    // Save Company
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.companyForm');
        return false;
      }

      // TODO: move create/update logic to service
      var isCreate = vm.company._id ? false : true;
      if (isCreate) {
        vm.company.$save(successCallback, errorCallback);
      } else {
        vm.company.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Company successfully ';
        vm.error = '';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.companyForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
