'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Transaction = mongoose.model('Transaction'),
  Calendar = mongoose.model('Calendar'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  processHandler = require(path.resolve('./modules/core/server/controllers/process.server.controller')),
  _ = require('lodash'),
  async = require('async');

/**
 * Create a Transaction (use process.server.controller for process flow)
 */
exports.create = function(req, res) {
  var transaction = new Transaction(req.body);
  transaction.user = req.user;

  var query = { module : {$elemMatch: {moduleName: 'Inventory', status: 'Open'}}, fromDate : { $lte : transaction.date }, toDate : { $gte : transaction.date} };
  Calendar.findOne(query).exec(function(err, record){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
	  
	 console.log(calendar);
    if (record.length === 0) {
      return res.status(400).send({
        message: 'Cannot create transaction (No existing period for date: ' + transaction.date + ')'
      });
    } else {
        var calendar = record.module.find(function( obj ) { return obj.moduleName == 'Inventory'; });
        if(calendar.status === 'Closed' || calendar.status === 'Future'){
          res.status(400).send({
            message: 'Transaction Calendar is ' +  calendar.status + ' Cannot create transaction for the specified date, calendar must have OPEN status'
          });
        }else{
          transaction.save(function(err){
            if (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            } else {
              // handle successful transaction creation
              processHandler.handleTransaction(transaction, function(success){
                if (success) {
                  Transaction.populate(transaction, [{
                    path: 'lines',
                    populate: [{
                      path: 'item',
                      model: 'Item'
                    }, {
                      path : 'category',
                      model : 'Reference'
                    }, {
                      path : 'uom',
                      model : 'Reference'
                    },{
                      path : 'debit',
                      model : 'Combination'
                    },{
                      path : 'credit',
                      model : 'Combination'
                    }]
                  }], function(err, transaction) {
                      if (err) {
                        return res.status(400).send({
                          message: errorHandler.getErrorMessage(err)
                        });
                      } else {
                        res.jsonp(transaction);
                      }
                  });
                } else {
                  transaction.remove();
                  return res.status(400).send({
                    message: 'Something went wrong.'
                  });
                }
              });
            }
          });
        }
      }
    });
};

/**
 * Show the current Transaction
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var transaction = req.transaction ? req.transaction.toJSON() : {};

  res.jsonp(transaction);
};

/**
 * Update a Transaction
 * Note: This method will
 * never be used since Transaction
 * cannot be edited
 */
exports.update = function(req, res) {
  var transaction = req.transaction ;

  transaction = _.extend(transaction , req.body);

  transaction.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(transaction);
    }
  });
};

/**
 * List of Transactions
 */
exports.list = function(req, res) { 
  Transaction.find().sort('-created')
  .populate({
    path:'lines',
    populate: [{
      path: 'item',
      model: 'Item'
    },{
      path : 'serials',
      populate : [{
        path : 'subInventory',
        model : 'Reference'
      },{
        path : 'transferSubInventory',
        model : 'Reference'
      }]
    }]
  }).populate({
    path : 'receipt',
    model : 'Receipt',
    populate : {
      path : 'purchaseOrder',
      model : 'PurchaseOrder'
    }
  }).exec(function(err, transactions) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(transactions);
    }
  });
};

/**
 * Transaction middleware
 */
exports.transactionByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Transaction is invalid'
    });
  }

  Transaction.findById(id)
  .populate('createdBy', 'username')
  .populate('updatedBy', 'username')
  .populate({
    path:'lines',
    populate: [{
      path: 'item',
      model: 'Item',
      populate : [{
        path : 'category',
        model : 'Reference'
      },{
        path : 'uom',
        model : 'Reference'
      }]
    },{
      path : 'debit',
      model : 'Combination'
    },{
      path : 'credit',
      model : 'Combination'
    }]
  }).populate({
    path : 'receipt',
    model : 'Receipt',
    populate : {
      path : 'purchaseOrder',
      model : 'PurchaseOrder',
      populate : {
        path : 'supplier',
        model : 'Supplier'
      }
    }
  }).exec(function (err, transaction) {
    if (err) {
      return next(err);
    } else if (!transaction) {
      return res.status(404).send({
        message: 'No Transaction with that identifier has been found'
      });
    }
    req.transaction = transaction;
    next();
  });
};
