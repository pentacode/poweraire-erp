'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  sequenceModel = mongoose.model('Sequence'),
  async = require('async'),
  Schema = mongoose.Schema;

/**
 * Transaction Schema
 */
var TransactionSchema = new Schema({
  number : Number,
  source : {
    type: String,
    enum: ['Inventory', 'PO Receipt']
  },
  action : {
    type : String,
    enum : ['Issuance', 'Receipt', 'Transfer']
  },
  receipt : {
    type : Schema.ObjectId,
    ref : 'Receipt'
  },
  date: Date,
  lines : [{
    item : {
      type : Schema.ObjectId,
      ref : 'Item'
    },
    quantity : Number,
    unitPrice : Number,
    chargeAccount : {
      type : Schema.ObjectId,
      ref : 'Combination'
    },
    serials : [{
      serial: {
        type: String,
        default : 'N/A'
      },
      subInventory: {
        type: Schema.ObjectId,
        ref: 'Reference'
      },
      quantity: {
        type: Number,
        required: 'Quantity is required.',
        min: [1, 'Quantity cannot be less than 1.']
      },
      transferSubInventory : {
        type : Schema.ObjectId,
        ref : 'Reference'
      },
    }],
    debit : {
      type: Schema.ObjectId,
      ref: 'Combination'
    },
    credit : {
      type : Schema.ObjectId,
      ref : 'Combination'
    }
  }],
  created: Date,
  updated : Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  issueTo : {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

TransactionSchema.pre('save', function(next) {
  var transaction = this;
  var currentDate = new Date();
  var user = transaction.user;
  transaction.updated = currentDate;
  transaction.updatedBy = user;
  if (!transaction.created){
    transaction.created = currentDate;
    transaction.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'Transaction' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        return next(err);
      }
      transaction.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('Transaction', TransactionSchema);
