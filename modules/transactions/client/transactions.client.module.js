(function (app) {
  'use strict';

  app.registerModule('transactions');
  app.registerModule('transactions.admin', ['core.admin']);
})(ApplicationConfiguration);
