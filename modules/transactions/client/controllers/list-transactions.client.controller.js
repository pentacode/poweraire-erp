(function () {
	'use strict';

	angular
	.module('transactions')
	.controller('TransactionsListController', TransactionsListController);

	TransactionsListController.$inject = ['$scope', '$ngConfirm', '$http', '$filter', 'TransactionsService', 'ItemsService', 'PurchaseOrdersService', 'CalendarsService', 'alertify'];

	function TransactionsListController($scope, $ngConfirm, $http, $filter, TransactionsService, ItemsService, PurchaseOrdersService, CalendarsService, alertify) {
		TransactionsService.query(function (data) {
			$scope.transactions = data;
			$scope.buildPager();
		});

		CalendarsService.query(function (data) {
			$scope.calendars = data;
			$scope.periods = [];
			for (var i = 0; i < $scope.calendars.length; i++) {
				var calendar = $scope.calendars[i];
				if ($scope.periods.indexOf(calendar.periodName) === -1) {
					angular.forEach(calendar.module, function (modName) {
						if (modName.moduleName == 'Inventory') {
							$scope.periods.push(calendar);
						}
					})
				}
			}
		});

		$scope.postToGL = function () {
			if ($scope.period && $scope.module) {
				var query = {
					module: $scope.module,
				};
				for (var i = 0; i < $scope.calendars.length; i++) {
					var calendar = $scope.calendars[i];
					if (calendar.periodName === $scope.period.periodName) {
						query.period = calendar;
						break;
					}
				}
				$http.post('/api/journal-vouchers/autoCreate', query)
				.then(function () {
					alertify.success('Journal Vouchers posted to GL');
				})
				.catch(function (err) {
					alertify.error(err.message);
				});
			} else {
				alertify.error('Please select a period and module.');
			}
		};

		$scope.module = 'Inventory';

		$scope.items = ItemsService.query({
			status: true
		});
		$scope.pos = PurchaseOrdersService.query();

		$scope.openDate = function ($event, isDateFrom) {
			if (isDateFrom) {
				$scope.dateFromStatus.opened = true;
				return;
			}
			$scope.dateToStatus.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};


		$scope.dateFromStatus = {
			opened: false
		};

		$scope.dateToStatus = {
			opened: false
		};


		$scope.buildPager = function () {
			$scope.pagedItems = [];
			$scope.itemsPerPage = 10;
			$scope.currentPage = 1;
			$scope.figureOutItemsToDisplay();
		};
		$scope.getTotalQuantity = function (lines) {
			var totalQuantity = 0;
			for (var i = 0; i < lines.length; i++) {
				var line = lines[i];
				totalQuantity = totalQuantity + line.quantity;
			}
			return totalQuantity;
		};
		$scope.figureOutItemsToDisplay = function () {
			if (!$scope.dateFrom && $scope.dateTo) {
				alertify.error('You must have a Date From!');
			}

			$scope.filteredItems = $filter('filter')($scope.transactions, function (obj) {
				if ($scope.source && !(new RegExp($scope.source).test(obj.source))) {
					return false;
				}
				if ($scope.transaction && $scope.source === 'PO Receipt' && !(new RegExp($scope.transaction).test(obj.receipt.purchaseOrder.number))) {
					return false;
				}
				if ($scope.transaction && $scope.source === 'Inventory' && !(new RegExp($scope.transaction).test(obj.number))) {
					return false;
				}
				if ($scope.item) {
					for (var i = 0; i < obj.lines.length; i++) {
						var line = obj.lines[i];
						if (line.item.code !== $scope.item) {
							return false;
						}
					}
				}

				if ($scope.action && !(new RegExp($scope.action).test(obj.action))) {
					return false;
				}
				if ($scope.dateFrom && (Date.parse(obj.date) < Date.parse($scope.dateFrom))) {
					return false;
				}
				if ($scope.dateTo && (Date.parse(obj.date) > Date.parse($scope.dateTo))) {
					return false;
				}
				return true;
			});

			$scope.filterLength = $scope.filteredItems.length;
			var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
			var end = begin + $scope.itemsPerPage;
			$scope.pagedItems = $scope.filteredItems.slice(begin, end);
		};

		$scope.pageChanged = function () {
			$scope.figureOutItemsToDisplay();
		};

		$scope.clear = function () {
			$scope.transaction = '';
			$scope.item = '';
			$scope.source = '';
			$scope.action = '';
			$scope.dateFrom = '';
			$scope.dateTo = '';
			$scope.figureOutItemsToDisplay();
		};

		$scope.searchTransaction = function () {
			$scope.trans = [];
			var records = [];
			if ($scope.source && $scope.source === 'Inventory') {
				records = $scope.transactions;
			} else if ($scope.source && $scope.source === 'PO Receipt') {
				records = $scope.pos;
			} else {
				$scope.transaction = '';
				alertify.error('You must select source');
				return;
			}
			for (var i = 0; i < records.length; i++) {
				var record = records[i];
				if ($scope.transaction && new RegExp($scope.transaction).test(record.number)) {
					$scope.trans.push(record.number);
				}
			}
		};

		$scope.searchItem = function () {
			$scope.filtered = $filter('filter')($scope.items, function (obj) {
				if ($scope.item && !(new RegExp($scope.item).test(obj.code))) {
					return false;
				}
				return true;
			});
		};
	}
})();