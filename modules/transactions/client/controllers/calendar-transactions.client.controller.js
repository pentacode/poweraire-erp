(function () {
	'use strict';

	angular
	.module('transactions')
	.controller('InventoryCalendarController', InventoryCalendarController);

	InventoryCalendarController.$inject = ['$scope', '$ngConfirm', '$http', '$filter', 'TransactionsService', 'ItemsService', 'PurchaseOrdersService', 'CalendarsService', 'alertify'];

	function InventoryCalendarController($scope, $ngConfirm, $http, $filter, TransactionsService, ItemsService, PurchaseOrdersService, CalendarsService, alertify) {
		CalendarsService.query(function (data) {
			$scope.calendars = data;
			$scope.periods = [];
			for (var i = 0; i < $scope.calendars.length; i++) {
				var calendar = $scope.calendars[i];
				if ($scope.periods.indexOf(calendar.periodName) === -1) {
					angular.forEach(calendar.module, function (modName) {
						if (modName.moduleName == 'Inventory') {
							$scope.periods.push(calendar);
						}
					})
				}
			}
			$scope.buildPager();
		});

		$scope.module = 'Inventory';
		
		$scope.buildPager = function () {
			$scope.pagedItems = [];
			$scope.itemsPerPage = 20;
			$scope.currentPage = 1;
			$scope.figureOutItemsToDisplay();
		};

		$scope.figureOutItemsToDisplay = function () {
			$scope.filteredItems = $filter('filter')($scope.calendars, function(calendar){
				if ($scope.periodName && !(new RegExp('^' + $scope.periodName, 'i').test(calendar.periodName))) {
					return false;
				}
				if ($scope.periodNumber && ($scope.periodNumber !== calendar.periodNumber)) {
					return false;
				}
				if ($scope.status && ($scope.status !== calendar.status)) {
					return false;
				}
				return true;
			});

			$scope.filterLength = $scope.filteredItems.length;
			var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
			var end = begin + $scope.itemsPerPage;
			$scope.pagedItems = $scope.filteredItems.slice(begin, end);
		};

		// $scope.postToGL = function () {
		// 	if ($scope.period && $scope.module) {
		// 		var query = {
		// 			module: $scope.module,
		// 		};
		// 		for (var i = 0; i < $scope.calendars.length; i++) {
		// 			var calendar = $scope.calendars[i];
		// 			if (calendar.periodName === $scope.period.periodName) {
		// 				query.period = calendar;
		// 				break;
		// 			}
		// 		}
		// 		$http.post('/api/journal-vouchers/autoCreate', query)
		// 		.then(function () {
		// 			alertify.success('Journal Vouchers posted to GL');
		// 		})
		// 		.catch(function (err) {
		// 			alertify.error(err.message);
		// 		});
		// 	} else {
		// 		alertify.error('Please select a period and module.');
		// 	}
		// };

		$scope.moduleStatus = function (moduleArr) {
			var modStatus = '';
			var calendar = moduleArr.find(function( obj ) { return obj.moduleName == 'Inventory'; });
			return calendar.status;
		}

		$scope.calendarStatus = function (moduleArr) {
			var status = 'Future';
			var openFlag = 0;
			var closeFlag = 0;
			angular.forEach(moduleArr, function(module) {
				if (module.status == 'Open'){
					openFlag += 1;
				} else if (module.status == 'Closed'){
					closeFlag += 1;
				} else {
					openFlag += 0;
					closeFlag += 0;
				}
			});

			if (closeFlag != moduleArr.length && openFlag > 0){
				status = 'Open';
			} else if (closeFlag == moduleArr.length) {
				status = 'Closed';
			}

			return status;
		}

		$scope.search = function () {
			$scope.ps = [];
			$scope.ps = $filter('filter')($scope.periods, function(period) {
				if ($scope.periodName && !(new RegExp('^' + $scope.periodName, 'i').test(period))) {
					return false;
				}
				return true;
			});
		};

		$scope.clear = function () {
			$scope.periodName = '';
			$scope.periodNumber = '';
			$scope.status = '';
			$scope.figureOutItemsToDisplay();
		};

		$scope.pageChanged = function () {
			$scope.figureOutItemsToDisplay();
		};

		$scope.openCalendar = function (event, calendar) {
			event.stopPropagation();
			var modInventory = calendar.module.find(o => o.moduleName == $scope.module);
			if (modInventory.moduleName == 'Inventory' && modInventory.status == 'Future') {
				CalendarsService.get({
					calendarId: calendar._id
				}, function (c) {
					c.module.find(x => x.moduleName === 'Inventory').status = 'Open';
					c.$update(function () {
						calendar.module.find(x => x.moduleName === 'Inventory').status = 'Open';
						alertify.success('Period successfully opened');
					}, function () {
						alertify.error('Failed to open period');
					});
				});
			} else {
				alertify.error('Cannot re-open Inventory calendar');
			}
		};

		$scope.confirmDialog = function (event, calendar) {
			event.stopPropagation();
			var stringContent = 'Are you sure you want to close "' + calendar.periodName + '" period?';
			if (calendar.module == 'Inventory') {
				stringContent += " You will not be able to reopen this period.";
			}

			$ngConfirm({
				title: 'Confirmation!',
				content: stringContent,
				columnClass: 'medium',
				scope: $scope,
				buttons: {
					closePeriod: {
						text: 'Close Period',
						btnClass: 'btn-orange',
						action: function (scope, button) {
							$scope.closeCalendar(calendar);
						}
					},
					close: function (scope, button) {
						// closes the modal
					}
				}
			});
		};

		$scope.closeCalendar = function (calendar) {
			CalendarsService.get({
				calendarId: calendar._id
			}, function (c) {
				c.module.find(x => x.moduleName === 'Inventory').status = 'Closed';
				c.$update(function () {
					calendar.module.find(x => x.moduleName === 'Inventory').status = 'Closed';
					alertify.success('Period successfully closed');
				}, function () {
					alertify.error('Failed to close period');
				});
			});
		};
	}
})();