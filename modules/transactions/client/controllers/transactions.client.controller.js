(function () {
  'use strict';

  // Transactions controller
  angular
    .module('transactions')
    .controller('TransactionsController', TransactionsController);
  
  TransactionsController.$inject = ['$scope', '$filter','$state', 'Authentication', 'transactionResolve', 'ItemsService', 'ReferencesService', 'CombinationsService', 'CalendarsService', 'alertify', 'Util', 'OnhandsService', '_', '$modal','CompaniesService', 'Users'];

  function TransactionsController ($scope, $filter, $state, Authentication, transaction, ItemsService, ReferencesService, CombinationsService, CalendarsService, alertify, Util, OnhandsService, _, $modal, CompaniesService, Users) {
    var vm = this;

    vm.items = ItemsService.query({ status : true });
    vm.periods = CalendarsService.query();
    vm.subInventories = [];
    vm.accounts = [];
    
    vm.formatDateTime = Util.formatDateTime;
    vm.authentication = Authentication;
    vm.transaction = transaction;
    if(vm.transaction.source !== 'PO Receipt'){
      vm.transaction.source = 'Inventory';
    }
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    vm.companyProfile = CompaniesService.get({
      companyId : 0
    });

    vm.users = Users.query({
      roles : 'Employee',
      status : true
    });

    vm.openDate = openDate;

    vm.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };


    vm.date = {
      opened: false
    };

    if(_.isEmpty(vm.transaction.date) && _.isEmpty(vm.transaction._id)){
      vm.transaction.date = new Date().setHours(0,0,0,0);
    }

    function openDate($event) {
        vm.date.opened = true;  
    }

    vm.openActionCodeLookup = openActionCodeLookup;
    vm.getPopOverDescription = getPopOverDescription;
    vm.getTotalLineAmout = getTotalLineAmout;
    
    function openActionCodeLookup(index){
      var modalInstance = $modal.open({
        animation : true,
        templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
        controller : 'CombinationLookupController',
        controllerAs: 'vm',
        size : 'lg',
        resolve : {
          combinations : function(){
            return vm.combinations;
          },
          index : index
        }
      });

      modalInstance.result.then(function (selected) {
        vm.transaction.lines[selected.lineIndex].chargeAccount = selected.combination._id;
      }, null);
    }

    function getPopOverDescription(chargeAccount){
      var description = "";
      if(!_.isEmpty(chargeAccount)){
         var combination = _.find(vm.combinations, { '_id' : chargeAccount});
         if(!_.isEmpty(combination)){
            description = combination.accountDescription;
         }
      }
        return description;
    }

    function getTotalLineAmout(line){
      if(line){
          return line.quantity * line.unitPrice;  
      }
      return 0;
    }

    $scope.addLine = function(){
      if(!vm.transaction.lines){
        vm.transaction.lines = [];
      }
      vm.transaction.lines.push({
        item : '',
        quantity : 0,
        unitPrice : 0,
        transferSubInventory : null,
        chargeAccount : vm.companyProfile.accountCode,
        serials:[],
        distributions:[]
      });
    };

    $scope.deleteLine=function(index, id){
      if (!id){
        vm.transaction.lines.splice(index, 1);
      }
    };
    vm.existingSerials = {};
    vm.unitPrices = {};
    vm.currentQuantity = {};
    OnhandsService.query(function(data){
        for (var i = 0; i < data.length; i++) {
          var onhand = data[i];
          for (var j = 0; j < onhand.serials.length; j++) {
            var serial = onhand.serials[j];
            if (!serial.subInventory)
              continue;
            
            var subinv = serial.subInventory; 
            serial.displaySerial = serial.serial + ' ( ' + $scope.getIfhasValue(subinv.descriptionOne) + ' - ' + $scope.getIfhasValue(subinv.descriptionTwo) + ' - ' + $scope.getIfhasValue(subinv.descriptionThree) + ' )';
            vm.currentQuantity[serial.serial+'_'+subinv._id] = serial.quantity;
          }
          vm.existingSerials[onhand.item._id] = filterExistingSerial(onhand.serials);
          vm.unitPrices[onhand.item._id] = onhand.unitPrice;
          vm.currentQuantity[onhand.item._id] = onhand.quantity ? onhand.quantity : 0;
        }
        console.log(vm.existingSerials);
    });


    vm.dropdownSerials = function(item){
      return vm.existingSerials[item._id];
    }

    $scope.onSerialSelect = function($model, $item, $label){
      $model.subInventory = $item.subInventory._id;
      $model.serial = $item.serial;
    };

    ReferencesService.query({ code : 'SUBINV', status : true }, function(data){
      for (var i = 0; i < data.length; i++) {
        var subinv = data[i];
        subinv.value = $scope.getIfhasValue(subinv.descriptionOne) + ' - ' + $scope.getIfhasValue(subinv.descriptionTwo) + ' - ' + $scope.getIfhasValue(subinv.descriptionThree);
        vm.subInventories.push(subinv);
      }
    });
    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.transactionForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.transactionForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accounts.push(combination);
      });
    });

    function filterExistingSerial(serials){
      return serials.filter(function(serial){
        return serial.quantity > 0;
      });
    }

    // Remove existing Transaction
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.transaction.$remove($state.go('transactions.list'));
      }
    }

    // Save Transaction
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.transactionForm');
        vm.error = 'Error in form values';
        alertify.error(vm.error);
        return false;
      }

       if (!vm.transaction.lines || vm.transaction.lines.length ===0){
        vm.error = 'Transaction Line is required';
        alertify.error(vm.error);
        return false;
      }else{
        var itemList = [];
        for (var i = 0; i < vm.transaction.lines.length; i++) {
          var line = vm.transaction.lines[i];
          var lineNo = i + 1;
          
          var itemid = line.item._id;        
          if (itemList.includes(itemid)){
            vm.error = 'Duplicate Item is not allowed';
            alertify.error(vm.error);
            return false;
          }else{
            itemList.push(itemid);
          }
          console.log(line.quantity, vm.currentQuantity[line.item._id] < line.quantity);
          if (!line.item){
            vm.error = 'Line ' + lineNo + ': Item Line can\'t be blank';
            alertify.error(vm.error);
            return false;
          } else if (line.quantity === 0){
            vm.error = 'Line ' + lineNo + ': Quantity can\'t be zero (0)';
            alertify.error(vm.error);
            return false;
          } else if(vm.currentQuantity[line.item._id] < line.quantity && vm.transaction.action !== 'Receipt') {
            vm.error = 'Line ' + lineNo + ': Unable to issue/transfer due to insufficient number of item/s on-hand.';
            alertify.error(vm.error);
            return false;
          }else if (!line.chargeAccount && vm.transaction.action !== 'Transfer'){
            vm.error = 'Line ' + lineNo + ': Charge Account can\'t be blank';
            alertify.error(vm.error);
            return false;
          } else if (line.serials.length === 0){
            vm.error = 'Line ' + lineNo + ': Serials are required';
            alertify.error(vm.error);
            return false;
          } else if (line.serials.length > 0){
            var total = 0;
            var mapSerial = {};
            for (var x = 0; x < line.serials.length; x++) {
              var serial = line.serials[x];
              console.log(serial);
              if(!serial.serial){
                serial.serial = 'N/A';
              } 
              if(!serial.subInventory){
                vm.error = 'Line ' + lineNo + ': Serial sub-inventory is required';  
                alertify.error(vm.error);
                return false; 
              }else if(serial.quantity < 0){
                vm.error = 'Line ' + lineNo + ': Serial quantity is required';  
                alertify.error(vm.error);
                return false; 
              }else if(['Issuance','Transfer'].indexOf(vm.transaction.action) > -1 && !isValidExistingSerial(itemid,serial, false)){
                vm.error = 'Line ' + lineNo + ': Must choose existing serial';  
                alertify.error(vm.error);
                return false; 
              } else if (vm.currentQuantity[serial.serial+'_'+serial.subInventory] < serial.quantity && vm.transaction.action !== 'Receipt'){
                vm.error = 'Line ' + lineNo + ': Unable to issue/transfer due to insufficient number of item/s on-hand';  
                alertify.error(vm.error);
                return false; 
              }
              if(vm.transaction.action === 'Transfer'){
                if(!serial.transferSubInventory){
                  vm.error = 'Line ' + lineNo + ': Please input transfer sub-inventory.';
                  alertify.error(vm.error);
                  return false;   
                }else if(serial.transferSubInventory === serial.subInventory){
                  vm.error = 'Line ' + lineNo + ': Unable to transfer to same sub-inventory.';  
                  alertify.error(vm.error);
                  return false;   
                }
              }
              total += serial.quantity; 
              if(mapSerial[serial.serial + '-' + serial.subInventory] === undefined){
                mapSerial[serial.serial + '-' + serial.subInventory] = x;
              } else if(mapSerial[serial.serial + '-' + serial.subInventory] !== x) {
                vm.error = 'Line ' + lineNo + ': Duplicate serial and sub-inventory detected.';  
                alertify.error(vm.error);
                return false;  
              }
            }            
            if (total !== line.quantity){
              vm.error = 'Line ' + lineNo + ': Serials total should be same with Quantity';
              alertify.error(vm.error);
              return false;
            }
          }
          // set debit / credit
          if(vm.transaction.action === 'Receipt'){
            line.debit = line.item.accountCode;
            line.credit = line.chargeAccount;
          }else if (vm.transaction.action === 'Issuance'){
            line.debit = line.chargeAccount;
            line.credit = line.item.accountCode;
          }
        }
      }

      // TODO: move create/update logic to service
      var isCreate = vm.transaction._id ? false : true;
      if (vm.transaction._id) {
        vm.transaction.$update(successCallback, errorCallback);
      } else {
        vm.transaction.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Transaction successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.transactionForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }

      function isValidExistingSerial(itemid, serial, isForTransfer){
        var existingSerials = vm.existingSerials[itemid];
        var valid = [];
        if(existingSerials){
          for (var i = 0; i < existingSerials.length; i++) {
            var existingSerial = existingSerials[i];
            if(existingSerial.serial === serial.serial && 
              (!isForTransfer && serial.subInventory === existingSerial.subInventory._id) ||
              (isForTransfer && serial.transferSubInventory === existingSerial.subInventory._id)){
              valid.push(serial);
            }
          }
          return valid.length > 0;  
        }
        return true;
      }
    }

    $scope.getIfhasValue = function(arg){
      return arg ? arg : 'N/A';
    };

    $scope.selectItem=function(obj){
      for (var i = 0; i < vm.items.length; i++) {
        var item = vm.items[i];
        if (item._id === obj.item._id){
          obj.item.category = item.category;
          obj.item.uom = item.uom;
          obj.item.subInventory=item.subInventory;
          obj.item.accountCode = item.accountCode;
          obj.unitPrice = vm.unitPrices[item._id];
        }
      }
    };

    $scope.deleteSerial = function(index, id){
      if (!id){
        vm.transaction.lines[vm.currentIndex].serials.splice(index, 1);
      }      
    };

    $scope.addSerial= function(obj){
      obj.push({
        quantity: 0
      });
    };

    $scope.setSelectedLine=function(obj,index){
      vm.currentIndex = index;
    };

    $scope.deleteDistribution=function(index){
      vm.transaction.lines[vm.currentIndex].distributions.splice(index, 1);
    };

    $scope.addDistribution= function(obj){
      obj.push({
        debit: '',
        credit: ''
      });
    };
  }
})();
