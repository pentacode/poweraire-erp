(function () {
  'use strict';

  angular
    .module('onhands.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'inventory', {
      title: 'Transactions',
      state: 'transactions.list',
      roles: ['Transactions']
    });
    Menus.addSubMenuItem('topbar', 'inventory', {
      title: 'Inventory Calendar',
      state: 'transactions.calendar',
      roles: ['Transactions']
    })
  }
})();