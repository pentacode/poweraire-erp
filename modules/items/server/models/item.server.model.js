'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Item Schema
 */
var ItemSchema = new Schema({
  code: {
    type: String,
    unique: true,
    required: 'Item code is required'
  },
  description: {
    type: String,
    default: ''
  },
  category: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  purchasedPrice: {
    type: Number,
    min: [0, 'Item purchased price cannot be negative'],
    required: 'Item purchased price is required'
  },
  sellingPrice: {
    type: Number,
    min: [0, 'Item selling price cannot be negative'],
    required: 'Item selling price is required'
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  uom: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  subInventory : {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

ItemSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate; 
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  if (this.status) {
    this.disabled = null;
  } else if (!this.disabled) {
    this.disabled = currentDate;
  }
  next();
});

mongoose.model('Item', ItemSchema);
