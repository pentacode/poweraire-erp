(function () {
  'use strict';

  // Items controller
  angular
    .module('items')
    .controller('ItemsController', ItemsController);

  ItemsController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'ReferencesService', 'CombinationsService', 'itemResolve','alertify','Util','$modal'];

  function ItemsController ($scope, $filter, $state, Authentication, ReferencesService, CombinationsService, item, alertify, Util,$modal) {
    var vm = this;

    $scope.getIfhasValue = function(arg){
      return arg ? arg : 'N/A';
    };

    vm.authentication = Authentication;
    vm.categories = ReferencesService.query({ code : 'CATEGORY', status : true });
    vm.uoms = ReferencesService.query({ code : 'UOM', status : true });
    vm.subInventories = [];
    ReferencesService.query({ code : 'SUBINV', status : true }, function(data){
      for (var i = 0; i < data.length; i++) {
        var subinv = data[i];
        subinv.value = $scope.getIfhasValue(subinv.descriptionOne) + ' - ' + $scope.getIfhasValue(subinv.descriptionTwo) + ' - ' + $scope.getIfhasValue(subinv.descriptionThree);
        vm.subInventories.push(subinv);
      }
    });
    
    vm.accountCodes = [];
    vm.item = item;
    vm.error = null;
    vm.form = {};
    vm.save = save;
    vm.openActionCodeLookup = openActionCodeLookup;

    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.itemForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.itemForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.item.accountCode = selected.combination._id;
        }, null);

    }

    // Save Item
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.itemForm');
        return false;
      }

      var isCreate = vm.item._id ? false : true;
      if (isCreate) {
        vm.item.$save(successCallback, errorCallback);
      } else {
        vm.item.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Item successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.itemForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
