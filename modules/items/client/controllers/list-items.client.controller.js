(function () {
  'use strict';

  angular
    .module('items')
    .controller('ItemsListController', ItemsListController);

  ItemsListController.$inject = ['$scope', '$filter', 'ItemsService'];

  function ItemsListController($scope, $filter, ItemsService) {
    ItemsService.query(function (data) {
      $scope.items = data;
      $scope.codes = [];
      $scope.descriptions = [];
      $scope.serials = [];
      for (var i = 0; i < $scope.items.length; i++) {
        var item = $scope.items[i];
        if ($scope.codes.indexOf(item.code) === -1) {
          $scope.codes.push(item.code);
        }
        if ($scope.descriptions.indexOf(item.description) === -1) {
          $scope.descriptions.push(item.description);
        }
        var combination = item.accountCode;
        if (!combination)
          continue;
        combination.accountDescription = 'N/A';
        if (combination.company) {
          combination.accountDescription = combination.company.descriptionOne;
        }
        if (combination.location) {
          combination.accountDescription += '-' + combination.location.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.serviceType) {
          combination.accountDescription += '-' + combination.serviceType.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.majorAccount) {
          combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.minorAccount) {
          combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedOne) {
          combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedTwo) {
          combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
      }
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.items, function(item){
        if ($scope.code && !(new RegExp('^' + $scope.code).test(item.code))) { //prefix search
          return false;
        }
        if ($scope.description && !(new RegExp($scope.description).test(item.description))) {
          return false;
        }
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.code = '';
      $scope.description = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.searchCode = function () {
      $scope.cs = [];
      $scope.cs = $filter('filter')($scope.codes, function(code){
        if ($scope.code && !(new RegExp('^' + $scope.code, 'i').test(code))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchDesc = function () {
      $scope.ds = [];
      $scope.ds = $filter('filter')($scope.descriptions, function(description){
        if ($scope.description && !(new RegExp($scope.description, 'i').test(description))) {
          return false;
        }
        return true;
      });
    };
  }
})();
