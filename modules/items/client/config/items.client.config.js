(function () {
  'use strict';

  angular
    .module('items.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Items',
      state: 'items.list',
      roles: ['Items']
    });
  }
})();
