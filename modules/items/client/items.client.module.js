(function (app) {
  'use strict';

  app.registerModule('items');
  app.registerModule('items.admin', ['core.admin']);
})(ApplicationConfiguration);
