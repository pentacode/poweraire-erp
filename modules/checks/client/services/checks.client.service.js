//Checks service used to communicate Checks REST endpoints
(function () {
  'use strict';

  angular
    .module('checks')
    .factory('ChecksService', ChecksService);

  ChecksService.$inject = ['$resource'];

  function ChecksService($resource) {
    return $resource('api/checks/:checkId', {
      checkId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
