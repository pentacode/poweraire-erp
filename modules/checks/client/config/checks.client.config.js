(function () {
  'use strict';

  angular
    .module('checks.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Checks',
      state: 'checks.list',
      roles: ['Checks']
    });
  }
})();
