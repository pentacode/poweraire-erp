(function () {
  'use strict';

  angular
    .module('checks')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('checks', {
        abstract: true,
        url: '/checks',
        template: '<ui-view/>'
      })
      .state('checks.list', {
        url: '',
        templateUrl: 'modules/checks/client/views/list-checks.client.view.html',
        controller: 'ChecksListController',
        controllerAs: 'vm',
        data: {
          roles: ['Checks'],
          pageTitle: 'Checks List'
        }
      })
      .state('checks.create', {
        url: '/create',
        templateUrl: 'modules/checks/client/views/form-check.client.view.html',
        controller: 'ChecksController',
        controllerAs: 'vm',
        resolve: {
          checkResolve: newCheck
        },
        data: {
          roles: ['Checks'],
          pageTitle : 'Checks Create'
        }
      })
      .state('checks.edit', {
        url: '/:checkId/edit',
        templateUrl: 'modules/checks/client/views/form-check.client.view.html',
        controller: 'ChecksController',
        controllerAs: 'vm',
        resolve: {
          checkResolve: getCheck
        },
        data: {
          roles: ['Checks'],
          pageTitle: 'Edit Check {{ checkResolve.name }}'
        }
      });
  }

  getCheck.$inject = ['$stateParams', 'ChecksService'];

  function getCheck($stateParams, ChecksService) {
    return ChecksService.get({
      checkId: $stateParams.checkId
    }).$promise;
  }

  newCheck.$inject = ['ChecksService'];

  function newCheck(ChecksService) {
    return new ChecksService();
  }
})();
