(function () {
  'use strict';

  // Checks controller
  angular
    .module('checks')
    .controller('ChecksController', ChecksController);

  ChecksController.$inject = ['$scope', '$state','$filter', 'Authentication', 'checkResolve', 'BanksService','alertify', 'Util'];

  function ChecksController ($scope, $state, $filter, Authentication, check, BanksService, alertify, Util) {
    var vm = this;
    
    vm.authentication = Authentication;
    vm.check = check;
    vm.error = null;
    vm.form = {};

    vm.save = save;
    vm.banks = [];
    BanksService.query({ status : true }, function(data){
      for (var i = 0; i < data.length; i++) {
        var bank = data[i];
        bank.displayName = 'N/A';
        if (bank.code) {
          bank.displayName = bank.code;
        }
        if (bank.branch) {
          bank.displayName += ' - ' + bank.branch;
        } else {
          bank.displayName += ' - N/A';
        }
        if (bank.accountNumber) {
          bank.displayName += ' - ' + bank.accountNumber;
        } else {
          bank.displayName += ' - N/A';
        }
        vm.banks.push(bank);
      }
    });

    $scope.$on('$stateChangeStart', function(event, toState) {
      if (vm.form.checkForm.$dirty) {
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
          vm.form.checkForm.$dirty = false;
          $state.go(toState);
        });
      }
    });

    // Save Check
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.checkForm');
        return false;
      }

      var isCreate = vm.check._id ? false : true;
      if (isCreate) {
        vm.check.$save(successCallback, errorCallback);
      } else {
        vm.check.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Check successfully ';
        vm.error = '';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.checkForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
