(function () {
  'use strict';

  angular
    .module('checks')
    .controller('ChecksListController', ChecksListController);

  ChecksListController.$inject = ['$scope','$filter','ChecksService', 'BanksService', '_'];

  function ChecksListController($scope, $filter, ChecksService, BanksService, _ ) {
    $scope._ = _;

    var vm = this;
    vm.pageChanged = pageChanged;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.clear = clear;

    vm.checks = ChecksService.query(function(){
      buildPager();
    });

    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;

      figureOutItemsToDisplay();
    }
    
    function figureOutItemsToDisplay() {

      vm.filtered = $filter('filter')(vm.checks, function(check){
        if(vm.bankName && !(new RegExp('^' + vm.bankName).test(check.bank.name)) ){
          return false;
        }
         if(vm.bankBranch && !(new RegExp('^' + vm.bankBranch).test(check.bank.branch)) ){
          return false;
        }
         if(vm.bankAccount && !(new RegExp('^' + vm.bankAccount).test(check.bank.accountNumber)) ){
          return false;
        }
         if(vm.checkName && !(new RegExp('^' + vm.checkName).test(check.name)) ){
          return false;
        }
        return true;
      });

      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filtered.slice(begin, end);
    }
    
    function pageChanged() {
      figureOutItemsToDisplay();
    }
    
    function clear() {
      vm.bankName = '';
      vm.bankBranch = '';
      vm.bankAccount = '';
      vm.checkName = '';
      figureOutItemsToDisplay();
    }
    
  }
})();
