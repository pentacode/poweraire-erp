(function (app) {
  'use strict';

  app.registerModule('checks');
  app.registerModule('checks.admin', ['core.admin']);
})(ApplicationConfiguration);
