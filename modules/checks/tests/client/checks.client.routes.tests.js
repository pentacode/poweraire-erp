(function () {
  'use strict';

  describe('Checks Route Tests', function () {
    // Initialize global variables
    var $scope,
      ChecksService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _ChecksService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      ChecksService = _ChecksService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('checks');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/checks');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          ChecksController,
          mockCheck;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('checks.view');
          $templateCache.put('modules/checks/client/views/view-check.client.view.html', '');

          // create mock Check
          mockCheck = new ChecksService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Check Name'
          });

          //Initialize Controller
          ChecksController = $controller('ChecksController as vm', {
            $scope: $scope,
            checkResolve: mockCheck
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:checkId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.checkResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            checkId: 1
          })).toEqual('/checks/1');
        }));

        it('should attach an Check to the controller scope', function () {
          expect($scope.vm.check._id).toBe(mockCheck._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/checks/client/views/view-check.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          ChecksController,
          mockCheck;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('checks.create');
          $templateCache.put('modules/checks/client/views/form-check.client.view.html', '');

          // create mock Check
          mockCheck = new ChecksService();

          //Initialize Controller
          ChecksController = $controller('ChecksController as vm', {
            $scope: $scope,
            checkResolve: mockCheck
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.checkResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/checks/create');
        }));

        it('should attach an Check to the controller scope', function () {
          expect($scope.vm.check._id).toBe(mockCheck._id);
          expect($scope.vm.check._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/checks/client/views/form-check.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          ChecksController,
          mockCheck;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('checks.edit');
          $templateCache.put('modules/checks/client/views/form-check.client.view.html', '');

          // create mock Check
          mockCheck = new ChecksService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Check Name'
          });

          //Initialize Controller
          ChecksController = $controller('ChecksController as vm', {
            $scope: $scope,
            checkResolve: mockCheck
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:checkId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.checkResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            checkId: 1
          })).toEqual('/checks/1/edit');
        }));

        it('should attach an Check to the controller scope', function () {
          expect($scope.vm.check._id).toBe(mockCheck._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/checks/client/views/form-check.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
