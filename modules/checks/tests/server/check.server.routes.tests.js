'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Check = mongoose.model('Check'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, check;

/**
 * Check routes tests
 */
describe('Check CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Check
    user.save(function () {
      check = {
        name: 'Check name'
      };

      done();
    });
  });

  it('should be able to save a Check if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Check
        agent.post('/api/checks')
          .send(check)
          .expect(200)
          .end(function (checkSaveErr, checkSaveRes) {
            // Handle Check save error
            if (checkSaveErr) {
              return done(checkSaveErr);
            }

            // Get a list of Checks
            agent.get('/api/checks')
              .end(function (checksGetErr, checksGetRes) {
                // Handle Check save error
                if (checksGetErr) {
                  return done(checksGetErr);
                }

                // Get Checks list
                var checks = checksGetRes.body;

                // Set assertions
                (checks[0].user._id).should.equal(userId);
                (checks[0].name).should.match('Check name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Check if not logged in', function (done) {
    agent.post('/api/checks')
      .send(check)
      .expect(403)
      .end(function (checkSaveErr, checkSaveRes) {
        // Call the assertion callback
        done(checkSaveErr);
      });
  });

  it('should not be able to save an Check if no name is provided', function (done) {
    // Invalidate name field
    check.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Check
        agent.post('/api/checks')
          .send(check)
          .expect(400)
          .end(function (checkSaveErr, checkSaveRes) {
            // Set message assertion
            (checkSaveRes.body.message).should.match('Please fill Check name');

            // Handle Check save error
            done(checkSaveErr);
          });
      });
  });

  it('should be able to update an Check if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Check
        agent.post('/api/checks')
          .send(check)
          .expect(200)
          .end(function (checkSaveErr, checkSaveRes) {
            // Handle Check save error
            if (checkSaveErr) {
              return done(checkSaveErr);
            }

            // Update Check name
            check.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Check
            agent.put('/api/checks/' + checkSaveRes.body._id)
              .send(check)
              .expect(200)
              .end(function (checkUpdateErr, checkUpdateRes) {
                // Handle Check update error
                if (checkUpdateErr) {
                  return done(checkUpdateErr);
                }

                // Set assertions
                (checkUpdateRes.body._id).should.equal(checkSaveRes.body._id);
                (checkUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Checks if not signed in', function (done) {
    // Create new Check model instance
    var checkObj = new Check(check);

    // Save the check
    checkObj.save(function () {
      // Request Checks
      request(app).get('/api/checks')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Check if not signed in', function (done) {
    // Create new Check model instance
    var checkObj = new Check(check);

    // Save the Check
    checkObj.save(function () {
      request(app).get('/api/checks/' + checkObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', check.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Check with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/checks/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Check is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Check which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Check
    request(app).get('/api/checks/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Check with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Check if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Check
        agent.post('/api/checks')
          .send(check)
          .expect(200)
          .end(function (checkSaveErr, checkSaveRes) {
            // Handle Check save error
            if (checkSaveErr) {
              return done(checkSaveErr);
            }

            // Delete an existing Check
            agent.delete('/api/checks/' + checkSaveRes.body._id)
              .send(check)
              .expect(200)
              .end(function (checkDeleteErr, checkDeleteRes) {
                // Handle check error error
                if (checkDeleteErr) {
                  return done(checkDeleteErr);
                }

                // Set assertions
                (checkDeleteRes.body._id).should.equal(checkSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Check if not signed in', function (done) {
    // Set Check user
    check.user = user;

    // Create new Check model instance
    var checkObj = new Check(check);

    // Save the Check
    checkObj.save(function () {
      // Try deleting Check
      request(app).delete('/api/checks/' + checkObj._id)
        .expect(403)
        .end(function (checkDeleteErr, checkDeleteRes) {
          // Set message assertion
          (checkDeleteRes.body.message).should.match('User is not authorized');

          // Handle Check error error
          done(checkDeleteErr);
        });

    });
  });

  it('should be able to get a single Check that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Check
          agent.post('/api/checks')
            .send(check)
            .expect(200)
            .end(function (checkSaveErr, checkSaveRes) {
              // Handle Check save error
              if (checkSaveErr) {
                return done(checkSaveErr);
              }

              // Set assertions on new Check
              (checkSaveRes.body.name).should.equal(check.name);
              should.exist(checkSaveRes.body.user);
              should.equal(checkSaveRes.body.user._id, orphanId);

              // force the Check to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Check
                    agent.get('/api/checks/' + checkSaveRes.body._id)
                      .expect(200)
                      .end(function (checkInfoErr, checkInfoRes) {
                        // Handle Check error
                        if (checkInfoErr) {
                          return done(checkInfoErr);
                        }

                        // Set assertions
                        (checkInfoRes.body._id).should.equal(checkSaveRes.body._id);
                        (checkInfoRes.body.name).should.equal(check.name);
                        should.equal(checkInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Check.remove().exec(done);
    });
  });
});
