'use strict';

/**
 * Module dependencies
 */
var checksPolicy = require('../policies/checks.server.policy'),
  checks = require('../controllers/checks.server.controller');

module.exports = function(app) {
  // Checks Routes
  app.route('/api/checks').all(checksPolicy.isAllowed)
    .get(checks.list)
    .post(checks.create);

  app.route('/api/checks/:checkId').all(checksPolicy.isAllowed)
    .get(checks.read)
    .put(checks.update);

  // Finish by binding the Check middleware
  app.param('checkId', checks.checkByID);
};
