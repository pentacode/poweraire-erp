'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Check Schema
 */
var CheckSchema = new Schema({
  bank : {
    type : Schema.ObjectId,
    ref : 'Bank'
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Check name',
    trim: true
  },
  documentNumberFrom: Number,
  documentNumberTo : Number,
  currentDocNum : Number,
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  created: Date,
  updated: Date
});

CheckSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate; 
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  if (this.status) {
    this.disabled = null;
  } else if (!this.disabled) {
    this.disabled = currentDate;
  }
  next();
});

mongoose.model('Check', CheckSchema);