'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Check = mongoose.model('Check'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Check
 */
exports.create = function(req, res) {
  var check = new Check(req.body);
  check.user = req.user;

  check.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(check);
    }
  });
};

/**
 * Show the current Check
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var check = req.check ? req.check.toJSON() : {};

  res.jsonp(check);
};

/**
 * Update a Check
 */
exports.update = function(req, res) {
  var check = req.check;
  check.user = req.user;
  
  check = _.extend(check , req.body);

  check.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(check);
    }
  });
};

/**
 * List of Checks
 */
exports.list = function(req, res) { 
  Check.find().populate('bank','name branch accountNumber').sort('-created').exec(function(err, checks) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(checks);
    }
  });
};

/**
 * Check middleware
 */
exports.checkByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Check is invalid'
    });
  }

  Check.findById(id).populate('createdBy','username').populate('updatedBy','username').exec(function (err, check) {
    if (err) {
      return next(err);
    } else if (!check) {
      return res.status(404).send({
        message: 'No Check with that identifier has been found'
      });
    }
    req.check = check;
    next();
  });
};
