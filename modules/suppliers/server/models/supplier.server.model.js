'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  sequenceModel = mongoose.model('Sequence');

/**
 * Supplier Schema
 */
var SupplierSchema = new Schema({
  number: Number,
  name: {
    type: String,
    unique: true,
    required: 'Supplier name is required'
  },
  addressOne: {
    type: String,
    required: 'Supplier address is required'
  },
  addressTwo: {
    type: String,
    required: 'Supplier address is required'
  },
  contactPerson: {
    type: String,
    required: 'Contact person is required'
  },
  contactNumber: {
    type: String,
    required: 'Contact number is required',
    maxLength: [11, 'Contact number exceeds maximum allowed length (11)']
  },
  paymentTerms: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  supplierType : {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  currency : {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  paymentMethod : {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  vat : {
    type: Schema.ObjectId,
    ref: 'Tax'
  },
  withholdingTax : {
    type: Schema.ObjectId,
    ref: 'Tax' 
  },
  countryCode : {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  tin: {
    type: String,
    required: 'TIN is required'
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

SupplierSchema.pre('save', function(next) {
  var currentDate = new Date();
  var supplier = this;
  var user = supplier.user;
  supplier.updated = currentDate; 
  supplier.updatedBy = user;
  if (supplier.status) {
    supplier.disabled = null;
  } else if (!supplier.disabled) {
    supplier.disabled = currentDate;
  }
  if(!supplier.created){
    supplier.created = currentDate;
    supplier.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'Supplier' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        return next(err);
      }
      supplier.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('Supplier', SupplierSchema);
