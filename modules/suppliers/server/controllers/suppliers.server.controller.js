'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Supplier = mongoose.model('Supplier'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Supplier
 */
exports.create = function(req, res) {
  var supplier = new Supplier(req.body);
  supplier.user = req.user;

  supplier.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(supplier);
    }
  });
};

/**
 * Show the current Supplier
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var supplier = req.supplier ? req.supplier.toJSON() : {};

  res.jsonp(supplier);
};

/**
 * Update a Supplier
 */
exports.update = function(req, res) {
  var supplier = req.supplier;
  supplier.user = req.user;

  supplier = _.extend(supplier , req.body);

  supplier.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(supplier);
    }
  });
};

/**
 * List of Suppliers
 */
exports.list = function(req, res) {
  var query = {};
  if (req.query.status !== undefined) {
    query.status = req.query.status;
  }
  if (req.query.name) {
    query.name = new RegExp(req.query.name, 'i');
  }
  if (req.query.number) {
    query.number = new RegExp('^' + req.query.number);
  }
  if (req.query.paymentTerms) {
    query.paymentTerms = new RegExp(req.query.paymentTerms);
  }
  Supplier.find(query)
  .sort('number')
  .populate('paymentTerms')
  .populate({
    path : 'accountCode',
    model : 'Combination',
    populate : {
      path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
      select : 'descriptionOne',
      model : 'Reference'
    }
  })
  .populate('supplierType')
  .populate('currency')
  .populate('paymentMethod')
  .populate('vat')
  .populate('withholdingTax')
  .populate('countryCode')
  .exec(function(err, suppliers) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(suppliers);
    }
  });
};

/**
 * Supplier middleware
 */
exports.supplierByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Supplier is invalid'
    });
  }

  Supplier.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, supplier) {
    if (err) {
      return next(err);
    } else if (!supplier) {
      return res.status(404).send({
        message: 'No Supplier with that identifier has been found'
      });
    }
    req.supplier = supplier;
    next();
  });
};
