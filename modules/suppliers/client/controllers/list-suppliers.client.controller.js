(function () {
  'use strict';

  angular
    .module('suppliers')
    .controller('SuppliersListController', SuppliersListController);

  SuppliersListController.$inject = ['$scope', '$filter','SuppliersService'];

  function SuppliersListController($scope, $filter, SuppliersService) {
    SuppliersService.query(function (data) {
      $scope.suppliers = data;
      $scope.numbers = [];
      $scope.names = [];
      $scope.paymentTermsList = [];
      for (var i = 0; i < $scope.suppliers.length; i++) {
        var supplier = $scope.suppliers[i];
        if ($scope.numbers.indexOf(supplier.number) === -1) {
          $scope.numbers.push(supplier.number);
        }
        if ($scope.names.indexOf(supplier.name) === -1) {
          $scope.names.push(supplier.name);
        }
        if (supplier.paymentTerms && $scope.paymentTermsList.indexOf(supplier.paymentTerms.value) === -1) {
          $scope.paymentTermsList.push(supplier.paymentTerms.value);
        }
        var combination = supplier.accountCode;
        if (!combination)
          continue;
        combination.accountDescription = 'N/A';
        if (combination.company) {
          combination.accountDescription = combination.company.descriptionOne;
        }
        if (combination.location) {
          combination.accountDescription += '-' + combination.location.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.serviceType) {
          combination.accountDescription += '-' + combination.serviceType.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.majorAccount) {
          combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.minorAccount) {
          combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedOne) {
          combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedTwo) {
          combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
      }
      $scope.buildPager();
    });
      
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.suppliers, function(supplier){
        if ($scope.number && !(new RegExp('^' + $scope.number).test(supplier.number))) {
          return false;
        }
        if ($scope.name && !(new RegExp($scope.name, 'i').test(supplier.name))) {
          return false;
        }
        if ($scope.paymentTerms && !(new RegExp($scope.paymentTerms).test(supplier.paymentTerms.value))) {
          return false;
        } 
        return true;
      });        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.number = '';
      $scope.name = '';
      $scope.paymentTerms = '';
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.searchNumber = function () {
      $scope.cs = [];
      $scope.cs = $filter('filter')($scope.numbers, function(number){
        if ($scope.number && !(new RegExp('^' + $scope.number, 'i').test(number))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchName = function () {
      $scope.ns = [];
      $scope.ns = $filter('filter')($scope.names, function(name){
        if ($scope.name && !(new RegExp($scope.name, 'i').test(name))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchPayment = function (){
      $scope.pts = [];
      $scope.pts = $filter('filter')($scope.paymentTermsList, function(paymentTerms){
        if ($scope.paymentTerms && !(new RegExp($scope.paymentTerms, 'i').test(paymentTerms))) {
          return false;
        }
        return true;
      });
    };
  }
})();