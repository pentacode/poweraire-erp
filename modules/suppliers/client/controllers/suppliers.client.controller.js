(function () {
  'use strict';

  // Suppliers controller
  angular
    .module('suppliers')
    .controller('SuppliersController', SuppliersController);

  SuppliersController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'TaxesService', 'ReferencesService', 'CombinationsService', 'supplierResolve','alertify','Util','$modal'];

  function SuppliersController ($scope, $filter, $state, Authentication, TaxesService, ReferencesService, CombinationsService, supplier, alertify, Util, $modal) {
    var vm = this;

    vm.authentication = Authentication;
    vm.paymentTerms = ReferencesService.query({ code : 'PYMNT_TERM', status : true });
    vm.accountCodes = [];
    vm.supplier = supplier;
    vm.error = null;
    vm.form = {};
    vm.save = save;
    vm.currencies = ReferencesService.query({ code : 'CURRENCY', status : true });
    vm.paymentMethods = ReferencesService.query({ code : 'PAYMENT_METHOD', status : true });
    vm.vats = TaxesService.query({ type : 'VAT', status : true });
    vm.countryCodes = ReferencesService.query({ code : 'COUNTRY', status : true });
    vm.supplierTypes = ReferencesService.query({ code : 'SUPPLIER_TYPE', status : true });
    vm.withholdingTaxes = TaxesService.query({ type : 'WTAX', status : true });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    vm.openActionCodeLookup = openActionCodeLookup;

    
    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.supplier.accountCode = selected.combination._id;
        }, null);

    }

    $scope.$on('$stateChangeStart', function( event, toState ) {
        if(vm.form.supplierForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.supplierForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    // Save Supplier
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.supplierForm');
        return false;
      }

      var isCreate = vm.supplier._id ? false : true;
      if (isCreate) {
        vm.supplier.$save(successCallback, errorCallback);
      } else {
        vm.supplier.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Supplier successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.supplierForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
