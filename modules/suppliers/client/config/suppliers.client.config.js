(function () {
  'use strict';

  angular
    .module('suppliers.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Suppliers',
      state: 'suppliers.list',
      roles: ['Suppliers']
    });
  }
})();
