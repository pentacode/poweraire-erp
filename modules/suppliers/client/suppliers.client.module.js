(function (app) {
  'use strict';

  app.registerModule('suppliers');
  app.registerModule('suppliers.admin', ['core.admin']);
})(ApplicationConfiguration);
