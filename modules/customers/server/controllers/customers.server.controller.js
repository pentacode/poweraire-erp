'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Customer = mongoose.model('Customer'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Customer
 */
exports.create = function(req, res) {
  var customer = new Customer(req.body);
  customer.user = req.user;

  customer.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customer);
    }
  });
};

/**
 * Show the current Customer
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var customer = req.customer ? req.customer.toJSON() : {};

  res.jsonp(customer);
};

/**
 * Update a Customer
 */
exports.update = function(req, res) {
  var customer = req.customer;
  customer.user = req.user;

  customer = _.extend(customer , req.body);

  customer.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customer);
    }
  });
};

/**
 * List of Customers
 */
exports.list = function(req, res) {
  var query = {};
  if (req.query.status !== undefined) {
    query.status = req.query.status;
  }
  if (req.query.name) {
    query.name = new RegExp(req.query.name, 'i');
  }
  if (req.query.number) {
    query.number = new RegExp('^' + req.query.number);
  }
  if (req.query.paymentTerms) {
    query.paymentTerms = new RegExp(req.query.paymentTerms);
  }
  Customer.find(query)
  .sort('number')
  .populate('paymentTerms')
  .populate({
    path : 'accountCode',
    model : 'Combination',
    populate : {
      path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
      select : 'descriptionOne',
      model : 'Reference'           
    }
  })
  .populate('customerType')
  .populate('currency')
  .populate('paymentMethod')
  .populate('vat')
  .populate('withholdingTax')
  .populate('countryCode')
  .exec(function(err, customers) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(customers);
    }
  });
};

/**
 * Customer middleware
 */
exports.customerByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Customer is invalid'
    });
  }

  Customer.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, customer) {
    if (err) {
      return next(err);
    } else if (!customer) {
      return res.status(404).send({
        message: 'No Customer with that identifier has been found'
      });
    }
    req.customer = customer;
    next();
  });
};
