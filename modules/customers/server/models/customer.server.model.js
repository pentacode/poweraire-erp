'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  sequenceModel = mongoose.model('Sequence');

/**
 * Customer Schema
 */
var CustomerSchema = new Schema({
  number: Number,
  name: {
    type: String,
    unique: true,
    required: 'Customer name is required'
  },
  addressOne: {
    type: String,
    required: 'Customer address is required'
  },
  addressTwo: {
    type: String,
    required: 'Customer address is required'
  },
  contactPerson: {
    type: String,
    required: 'Contact person is required'
  },
  contactNumber: {
    type: String,
    required: 'Contact number is required',
    maxLength: [11, 'Contact number exceeds maximum allowed length (11)']
  },
  paymentTerms: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  tin: {
    type: String,
    required: 'TIN is required'
  },
  customerType: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  currency: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  paymentMethod: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  vat: {
    type: Schema.ObjectId,
    ref: 'Tax'
  },
  withholdingTax : {
    type: Schema.ObjectId,
    ref: 'Tax' 
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  countryCode: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

CustomerSchema.pre('save', function(next) {
  var customer = this;
  var currentDate = new Date();
  var user = customer.user;
  customer.updated = currentDate;
  customer.updatedBy = user;
  if (customer.status) {
    customer.disabled = null;
  } else if (!customer.disabled) {
    customer.disabled = currentDate;
  }
  if (!customer.created) {
    customer.created = currentDate;
    customer.createdBy = user;
    sequenceModel.findOneAndUpdate({
      model: 'Customer'
    }, {
      $inc: {
        value: 1
      }
    }, function(err, sequence) {
      if (err) {
        return next(err);
      }
      customer.number = sequence.value;
      next();
    });
  } else {
    next();
  }

});

mongoose.model('Customer', CustomerSchema);