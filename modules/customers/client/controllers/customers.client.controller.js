(function () {
  'use strict';

  // Customers controller
  angular
    .module('customers')
    .controller('CustomersController', CustomersController);

  CustomersController.$inject = ['$scope', '$filter', '$state', 'Authentication','TaxesService', 'ReferencesService', 'CombinationsService', 'customerResolve','alertify','Util','$modal'];

  function CustomersController ($scope, $filter, $state, Authentication,TaxesService, ReferencesService, CombinationsService, customer, alertify, Util, $modal) {
    var vm = this;

    vm.authentication = Authentication;
    vm.paymentTerms = ReferencesService.query({ code : 'PYMNT_TERM', status : true });
    vm.accountCodes = [];
    vm.customer = customer;
    vm.error = null;
    vm.form = {};
    vm.save = save;
    vm.customerTypes = ReferencesService.query({ code : 'CUSTOMER_TYPE', status : true });
    vm.currencies = ReferencesService.query({ code : 'CURRENCY', status : true });
    vm.paymentMethods = ReferencesService.query({ code : 'PAYMENT_METHOD', status : true });
    vm.vats = TaxesService.query({ type : 'VAT', status : true });
    vm.withholdingTaxes = TaxesService.query({ type : 'WTAX', status : true });
    vm.countryCodes = ReferencesService.query({ code : 'COUNTRY', status : true });
    vm.openActionCodeLookup = openActionCodeLookup;
    
    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.customerForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.customerForm.$dirty = false;
            $state.go(toState);
          });
        }
    });
    
   vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.customer.accountCode = selected.combination._id;
        }, null);

    }

    // Save Customer
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.customerForm');
        return false;
      }
      
      var isCreate = vm.customer._id ? false : true;
      if (isCreate) {
        vm.customer.$save(successCallback, errorCallback);
      } else {
        vm.customer.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Customer successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.customerForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
