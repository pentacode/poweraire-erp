(function () {
  'use strict';

  angular
    .module('customers')
    .controller('CustomersListController', CustomersListController);

  CustomersListController.$inject = ['$scope', '$filter', 'CustomersService'];

  function CustomersListController($scope, $filter, CustomersService) {
    CustomersService.query(function (data) {
      $scope.customers = data;
      $scope.numbers = [];
      $scope.names = [];
      $scope.paymentTermsList = [];
      for (var i = 0; i < $scope.customers.length; i++) {
        var customer = $scope.customers[i];
        if ($scope.numbers.indexOf(customer.number) === -1) {
          $scope.numbers.push(customer.number);
        }

        if ($scope.names.indexOf(customer.name) === -1) {
          $scope.names.push(customer.name);
        }
        if (customer.paymentTerms && $scope.paymentTermsList.indexOf(customer.paymentTerms.value)) {
          $scope.paymentTermsList.push(customer.paymentTerms.value);
        }
        var combination = customer.accountCode;
        if (!combination)
          continue;
        combination.accountDescription = 'N/A';
        if (combination.company) {
          combination.accountDescription = combination.company.descriptionOne;
        }
        if (combination.location) {
          combination.accountDescription += '-' + combination.location.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.serviceType) {
          combination.accountDescription += '-' + combination.serviceType.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.majorAccount) {
          combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.minorAccount) {
          combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedOne) {
          combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedTwo) {
          combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
      }
      $scope.buildPager();
    });
      
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.customers, function(customer){
        if ($scope.number && !(new RegExp('^' + $scope.number).test(customer.number))) {
          return false;
        }
        if ($scope.name && !(new RegExp($scope.name, 'i').test(customer.name))) {
          return false;
        }
        if ($scope.paymentTerms && !(new RegExp($scope.paymentTerms).test(customer.paymentTerms.value))) {
          return false;
        } 
        return true;
      });        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.number = '';
      $scope.name = '';
      $scope.paymentTerms = '';
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.searchNumber = function () {
      $scope.cs = $filter('filter')($scope.numbers, function(number) {
        if ($scope.number && !(new RegExp('^' + $scope.number).test(number))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchName = function () {
      $scope.ns = [];
      $scope.ns = $filter('filter')($scope.names, function(name){
        if ($scope.name && !(new RegExp($scope.name, 'i').test(name))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchPayment = function (){
      $scope.pts = [];
      $scope.pts = $filter('filter')($scope.paymentTermsList, function(paymentTerms){
        if ($scope.paymentTerms && !(new RegExp($scope.paymentTerms, 'i').test(paymentTerms))) {
          return false;
        }
        return true;
      });
    };
  }
})();
