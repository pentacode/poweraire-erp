(function (app) {
  'use strict';

  app.registerModule('customers');
  app.registerModule('customers.admin', ['core.admin']);
})(ApplicationConfiguration);
