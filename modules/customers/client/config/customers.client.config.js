(function () {
  'use strict';

  angular
    .module('customers.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Customers',
      state: 'customers.list',
      roles: ['Customers']
    });
  }
})();
