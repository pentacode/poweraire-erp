(function () {
  'use strict';

  describe('References Route Tests', function () {
    // Initialize global variables
    var $scope,
      ReferencesService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _ReferencesService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      ReferencesService = _ReferencesService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('references');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/references');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          ReferencesController,
          mockReference;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('references.view');
          $templateCache.put('modules/references/client/views/view-reference.client.view.html', '');

          // create mock Reference
          mockReference = new ReferencesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Reference Name'
          });

          //Initialize Controller
          ReferencesController = $controller('ReferencesController as vm', {
            $scope: $scope,
            referenceResolve: mockReference
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:referenceId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.referenceResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            referenceId: 1
          })).toEqual('/references/1');
        }));

        it('should attach an Reference to the controller scope', function () {
          expect($scope.vm.reference._id).toBe(mockReference._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/references/client/views/view-reference.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          ReferencesController,
          mockReference;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('references.create');
          $templateCache.put('modules/references/client/views/form-reference.client.view.html', '');

          // create mock Reference
          mockReference = new ReferencesService();

          //Initialize Controller
          ReferencesController = $controller('ReferencesController as vm', {
            $scope: $scope,
            referenceResolve: mockReference
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.referenceResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/references/create');
        }));

        it('should attach an Reference to the controller scope', function () {
          expect($scope.vm.reference._id).toBe(mockReference._id);
          expect($scope.vm.reference._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/references/client/views/form-reference.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          ReferencesController,
          mockReference;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('references.edit');
          $templateCache.put('modules/references/client/views/form-reference.client.view.html', '');

          // create mock Reference
          mockReference = new ReferencesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Reference Name'
          });

          //Initialize Controller
          ReferencesController = $controller('ReferencesController as vm', {
            $scope: $scope,
            referenceResolve: mockReference
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:referenceId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.referenceResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            referenceId: 1
          })).toEqual('/references/1/edit');
        }));

        it('should attach an Reference to the controller scope', function () {
          expect($scope.vm.reference._id).toBe(mockReference._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/references/client/views/form-reference.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
