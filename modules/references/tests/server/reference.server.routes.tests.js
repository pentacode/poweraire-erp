'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Reference = mongoose.model('Reference'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, reference;

/**
 * Reference routes tests
 */
describe('Reference CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Reference
    user.save(function () {
      reference = {
        name: 'Reference name'
      };

      done();
    });
  });

  it('should be able to save a Reference if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Reference
        agent.post('/api/references')
          .send(reference)
          .expect(200)
          .end(function (referenceSaveErr, referenceSaveRes) {
            // Handle Reference save error
            if (referenceSaveErr) {
              return done(referenceSaveErr);
            }

            // Get a list of References
            agent.get('/api/references')
              .end(function (referencesGetErr, referencesGetRes) {
                // Handle Reference save error
                if (referencesGetErr) {
                  return done(referencesGetErr);
                }

                // Get References list
                var references = referencesGetRes.body;

                // Set assertions
                (references[0].user._id).should.equal(userId);
                (references[0].name).should.match('Reference name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Reference if not logged in', function (done) {
    agent.post('/api/references')
      .send(reference)
      .expect(403)
      .end(function (referenceSaveErr, referenceSaveRes) {
        // Call the assertion callback
        done(referenceSaveErr);
      });
  });

  it('should not be able to save an Reference if no name is provided', function (done) {
    // Invalidate name field
    reference.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Reference
        agent.post('/api/references')
          .send(reference)
          .expect(400)
          .end(function (referenceSaveErr, referenceSaveRes) {
            // Set message assertion
            (referenceSaveRes.body.message).should.match('Please fill Reference name');

            // Handle Reference save error
            done(referenceSaveErr);
          });
      });
  });

  it('should be able to update an Reference if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Reference
        agent.post('/api/references')
          .send(reference)
          .expect(200)
          .end(function (referenceSaveErr, referenceSaveRes) {
            // Handle Reference save error
            if (referenceSaveErr) {
              return done(referenceSaveErr);
            }

            // Update Reference name
            reference.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Reference
            agent.put('/api/references/' + referenceSaveRes.body._id)
              .send(reference)
              .expect(200)
              .end(function (referenceUpdateErr, referenceUpdateRes) {
                // Handle Reference update error
                if (referenceUpdateErr) {
                  return done(referenceUpdateErr);
                }

                // Set assertions
                (referenceUpdateRes.body._id).should.equal(referenceSaveRes.body._id);
                (referenceUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of References if not signed in', function (done) {
    // Create new Reference model instance
    var referenceObj = new Reference(reference);

    // Save the reference
    referenceObj.save(function () {
      // Request References
      request(app).get('/api/references')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Reference if not signed in', function (done) {
    // Create new Reference model instance
    var referenceObj = new Reference(reference);

    // Save the Reference
    referenceObj.save(function () {
      request(app).get('/api/references/' + referenceObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', reference.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Reference with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/references/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Reference is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Reference which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Reference
    request(app).get('/api/references/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Reference with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Reference if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Reference
        agent.post('/api/references')
          .send(reference)
          .expect(200)
          .end(function (referenceSaveErr, referenceSaveRes) {
            // Handle Reference save error
            if (referenceSaveErr) {
              return done(referenceSaveErr);
            }

            // Delete an existing Reference
            agent.delete('/api/references/' + referenceSaveRes.body._id)
              .send(reference)
              .expect(200)
              .end(function (referenceDeleteErr, referenceDeleteRes) {
                // Handle reference error error
                if (referenceDeleteErr) {
                  return done(referenceDeleteErr);
                }

                // Set assertions
                (referenceDeleteRes.body._id).should.equal(referenceSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Reference if not signed in', function (done) {
    // Set Reference user
    reference.user = user;

    // Create new Reference model instance
    var referenceObj = new Reference(reference);

    // Save the Reference
    referenceObj.save(function () {
      // Try deleting Reference
      request(app).delete('/api/references/' + referenceObj._id)
        .expect(403)
        .end(function (referenceDeleteErr, referenceDeleteRes) {
          // Set message assertion
          (referenceDeleteRes.body.message).should.match('User is not authorized');

          // Handle Reference error error
          done(referenceDeleteErr);
        });

    });
  });

  it('should be able to get a single Reference that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Reference
          agent.post('/api/references')
            .send(reference)
            .expect(200)
            .end(function (referenceSaveErr, referenceSaveRes) {
              // Handle Reference save error
              if (referenceSaveErr) {
                return done(referenceSaveErr);
              }

              // Set assertions on new Reference
              (referenceSaveRes.body.name).should.equal(reference.name);
              should.exist(referenceSaveRes.body.user);
              should.equal(referenceSaveRes.body.user._id, orphanId);

              // force the Reference to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Reference
                    agent.get('/api/references/' + referenceSaveRes.body._id)
                      .expect(200)
                      .end(function (referenceInfoErr, referenceInfoRes) {
                        // Handle Reference error
                        if (referenceInfoErr) {
                          return done(referenceInfoErr);
                        }

                        // Set assertions
                        (referenceInfoRes.body._id).should.equal(referenceSaveRes.body._id);
                        (referenceInfoRes.body.name).should.equal(reference.name);
                        should.equal(referenceInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Reference.remove().exec(done);
    });
  });
});
