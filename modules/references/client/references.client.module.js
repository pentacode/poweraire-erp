(function (app) {
  'use strict';

  app.registerModule('references');
  app.registerModule('references.admin', ['core.admin']);
})(ApplicationConfiguration);
