(function() {
  'use strict';

  // References controller
  angular
    .module('references')
    .controller('ReferencesController', ReferencesController);

  ReferencesController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'referenceResolve', 'ReferencesService','alertify','Util'];

  function ReferencesController($scope, $filter, $state, Authentication, reference, ReferencesService, alertify, Util) {
    var vm = this;

    vm.authentication = Authentication;
    vm.reference = reference;
    vm.error = null;
    vm.form = {};
    vm.isCreate = vm.reference._id ? false : true;
    vm.max = 0;

    vm.save = save;
    vm.valueFieldBlur = valueFieldBlur;
    vm.segmentFieldBlur = segmentFieldBlur;

    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.referenceForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.referenceForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    function getReferenceByQuery(value, segment) {
      return ReferencesService.query({
        value: value,
        segmentNumber: segment
      }).$promise;
    }

    function valueFieldBlur(){
      if (vm.reference.segmentNumber){
        valueCodeMaxChecking(vm.reference.segmentNumber, vm.reference.value);
      }
      
      if (vm.reference.code === 'COMBINATION' && (vm.reference.segmentNumber)) {
        vm.form.referenceForm.value.$setValidity('value_code_duplicate', true);        
      }
      
      $scope.$broadcast('show-errors-check-validity', 'vm.form.referenceForm');
    }

    function segmentFieldBlur(){
      if (vm.reference.value) {
        valueCodeMaxChecking(vm.reference.segmentNumber, vm.reference.value);
        vm.form.referenceForm.value.$setValidity('value_code_duplicate', true);
        $scope.$broadcast('show-errors-check-validity', 'vm.form.referenceForm');
      }
    }
    
    function valueCodeMaxChecking(segmentNumber, value){
      var listMax = [2,3,3,4,4,2,2];
      vm.max = listMax[segmentNumber - 1];
      if (value.length > vm.max){
        vm.form.referenceForm.value.$setValidity('value_code_max', false);
      }else{
        vm.form.referenceForm.value.$setValidity('value_code_max', true);
      }
    }
    
    var successCallback = function successCallback(res) {
      vm.success = 'Reference successfully ';
      if (vm.isCreate) {
        vm.success = vm.success + 'created';
      } else {
        vm.success = vm.success + 'updated';
      }
      vm.form.referenceForm.$dirty = false;
      alertify.success(vm.success);
    };

    var errorCallback = function errorCallback(res) {
      vm.error = res.data.message;
      alertify.error(vm.error);
    };


    // Save Reference
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.referenceForm');
        return false;
      }
      var isCodeCombination = false;
      if (vm.reference.code === 'COMBINATION') {
        isCodeCombination = true;
      }
      if (isCodeCombination) {
        saveReferenceCodeCombination();
        return;
      }
      upsert(vm.isCreate);
    }

    function saveReferenceCodeCombination() {
      getReferenceByQuery(vm.reference.value, vm.reference.segmentNumber)
        .then(function(data) {
          if (data.length > 0) {
            if((vm.reference._id) && vm.reference._id === data[0]._id){
              upsert(false);
            } else {
              vm.form.referenceForm.value.$setValidity('value_code_duplicate', false);
              $scope.$broadcast('show-errors-check-validity', 'vm.form.referenceForm');
            }
          } else {
            if(vm.reference._id){
              upsert(false);
              return;
            }
            upsert(vm.isCreate);
          }
        });
    }

    function upsert(isCreate) {
      if (isCreate) {
        vm.reference.$save(successCallback, errorCallback);
      } else {
        vm.reference.$update(successCallback, errorCallback);
      }
    }
  }
})();