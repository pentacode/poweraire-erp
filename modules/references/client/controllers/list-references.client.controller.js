(function() {
  'use strict';

  angular
    .module('references')
    .controller('ReferencesListController', ReferencesListController);

  ReferencesListController.$inject = ['$scope', '$filter', 'ReferencesService', '_'];

  function ReferencesListController($scope, $filter, ReferencesService, _) {

    $scope._ = _;
    var vm = this;
    vm.pageChanged = pageChanged;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.clear = clear;
    vm.clearSegmentNumber = clearSegmentNumber;
    vm.sortSegmentNumber = sortSegmentNumber;
    vm.sortReverse = false;
    vm.sortType = 'createdBy';

    vm.references = ReferencesService.query(function() {
      buildPager();
    });

    function clearSegmentNumber(){
      if(vm.referenceCode !== 'COMBINATION'){
        vm.segmentNumber = '';
      }
    }
    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;

      figureOutItemsToDisplay();
    }

    function sortSegmentNumber(){
       vm.references = $filter('orderBy')(vm.references, vm.sortType, vm.sortReverse);
       figureOutItemsToDisplay();
    }

    function figureOutItemsToDisplay() {

      vm.filtered = $filter('filter')(vm.references, function(reference) {

        if (vm.referenceCode && !(new RegExp('^' + vm.referenceCode, 'i').test(reference.code))) {
          return false;
        }

        if(vm.valueCode && !(new RegExp('^' + vm.valueCode).test(reference.value)) ){
          return false;
        }

        if (vm.valueDescription && !(new RegExp(vm.valueDescription, 'i').test(reference.descriptionOne)) && !(new RegExp(vm.valueDescription, 'i').test(reference.descriptionTwo)) && !(new RegExp(vm.valueDescription, 'i').test(reference.descriptionThree))) {
          return false;
        }

        if(vm.segmentNumber && !(new RegExp('^' + vm.segmentNumber).test(reference.segmentNumber)) ){
          return false;
        }

        return true;
      });

      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filtered.slice(begin, end);
    }

    function pageChanged() {
      figureOutItemsToDisplay();
    }

    function clear() {
      vm.referenceCode = '';
      vm.valueCode = '';
      vm.valueDescription = '';
      figureOutItemsToDisplay();
    }

  }
})();