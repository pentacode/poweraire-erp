//References service used to communicate References REST endpoints
(function () {
  'use strict';

  angular
    .module('references')
    .factory('ReferencesService', ReferencesService);

  ReferencesService.$inject = ['$resource'];

  function ReferencesService($resource) {
    return $resource('api/references/:referenceId', {
      referenceId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
