(function () {
  'use strict';

  angular
    .module('references')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('references', {
        abstract: true,
        url: '/references',
        template: '<ui-view/>'
      })
      .state('references.list', {
        url: '',
        templateUrl: 'modules/references/client/views/list-references.client.view.html',
        controller: 'ReferencesListController',
        controllerAs: 'vm',
        data: {
          roles: ['References'],
          pageTitle: 'References List'
        }
      })
      .state('references.create', {
        url: '/create',
        templateUrl: 'modules/references/client/views/form-reference.client.view.html',
        controller: 'ReferencesController',
        controllerAs: 'vm',
        resolve: {
          referenceResolve: newReference
        },
        data: {
          roles: ['References'],
          pageTitle : 'References Create'
        }
      })
      .state('references.edit', {
        url: '/:referenceId/edit',
        templateUrl: 'modules/references/client/views/form-reference.client.view.html',
        controller: 'ReferencesController',
        controllerAs: 'vm',
        resolve: {
          referenceResolve: getReference
        },
        data: {
          roles: ['References'],
          pageTitle: 'Edit Reference {{ referenceResolve.name }}'
        }
      });
  }

  getReference.$inject = ['$stateParams', 'ReferencesService'];

  function getReference($stateParams, ReferencesService) {
    return ReferencesService.get({
      referenceId: $stateParams.referenceId
    }).$promise;
  }

  newReference.$inject = ['ReferencesService'];

  function newReference(ReferencesService) {
    return new ReferencesService();
  }
})();
