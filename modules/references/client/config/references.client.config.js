(function () {
  'use strict';

  angular
    .module('references.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'References',
      state: 'references.list',
      roles: ['References']
    });
  }
})();
