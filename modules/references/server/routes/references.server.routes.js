'use strict';

/**
 * Module dependencies
 */
var referencesPolicy = require('../policies/references.server.policy'),
  references = require('../controllers/references.server.controller');

module.exports = function(app) {
  // References Routes
  app.route('/api/references').all(referencesPolicy.isAllowed)
    .get(references.list)
    .post(references.create);

  app.route('/api/references/:referenceId').all(referencesPolicy.isAllowed)
    .get(references.read)
    .put(references.update);

  // Finish by binding the Reference middleware
  app.param('referenceId', references.referenceByID);
};
