'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Reference Schema
 */
var ReferenceSchema = new Schema({
  code: {
    type: String,
    required: 'Reference code is required.'
  },
  value: {
    type: String,
    required: 'Value code is required.'
  },
  descriptionOne: {
    type: String,
    default: ''
  },
  descriptionTwo: {
    type: String,
    default: ''
  },
  descriptionThree: {
    type: String,
    default: ''
  },
  segmentNumber: {
    type: Number
  },
  accountType: {
    type: String,
    enum: ['Asset', 'Expense', 'Liability', 'Ownership', 'Revenue', '']
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

ReferenceSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate;
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  if (this.status) {
    this.disabled = null;
  } else if (!this.disabled) {
    this.disabled = currentDate;
  }
  if (this.segmentNumber !== 4) {
    this.accountType = '';
  }
  next();
});

mongoose.model('Reference', ReferenceSchema);
