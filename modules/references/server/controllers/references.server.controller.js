'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Reference = mongoose.model('Reference'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Reference
 */
exports.create = function(req, res) {
  var reference = new Reference(req.body);
  reference.user = req.user;

  reference.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(reference);
    }
  });
};

/**
 * Show the current Reference
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var reference = req.reference ? req.reference.toJSON() : {};

  res.jsonp(reference);
};

/**
 * Update a Reference
 */
exports.update = function(req, res) {
  var reference = req.reference;
  reference.user = req.user;

  reference = _.extend(reference , req.body);

  reference.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(reference);
    }
  });
};

/**
 * List of References
 */
exports.list = function(req, res) { 
  var query = {};
  if (req.query.status !== undefined) {
    query.status = req.query.status;
  }
  if (req.query.code) {
    query.code = new RegExp('^' + req.query.code, 'i');
  }
  if (req.query.value) {
    query.value = req.query.value;
  }
  if (req.query.description) {
    query.descriptionOne = new RegExp(req.query.description, 'i');
    query.descriptionTwo = new RegExp(req.query.description, 'i');
    query.descriptionThree = new RegExp(req.query.description, 'i');
  }
  if (req.query.segmentNumber !== undefined) {
    query.segmentNumber = req.query.segmentNumber;
  }
  if (req.query.distinct){
    Reference.find(query).distinct('code', function(err, references){
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.jsonp(references);
      } 
    });
  }else{
    Reference.find(query).sort({ code : 1, value : 1 }).exec(function(err, references) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.jsonp(references);
      }
    });
  }
};

/**
 * Reference middleware
 */
exports.referenceByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Reference is invalid'
    });
  }

  Reference.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, reference) {
    if (err) {
      return next(err);
    } else if (!reference) {
      return res.status(404).send({
        message: 'No Reference with that identifier has been found'
      });
    }
    req.reference = reference;
    next();
  });
};
