'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  PurchaseOrder = mongoose.model('PurchaseOrder');

/**
 * Globals
 */
var user, purchaseOrder;

/**
 * Unit tests
 */
describe('Purchase order Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() { 
      purchaseOrder = new PurchaseOrder({
        name: 'Purchase order Name',
        user: user
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
      return purchaseOrder.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) { 
      purchaseOrder.name = '';

      return purchaseOrder.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) { 
    PurchaseOrder.remove().exec(function(){
      User.remove().exec(function(){
        done();  
      });
    });
  });
});
