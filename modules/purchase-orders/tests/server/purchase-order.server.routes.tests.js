'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  PurchaseOrder = mongoose.model('PurchaseOrder'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, purchaseOrder;

/**
 * Purchase order routes tests
 */
describe('Purchase order CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Purchase order
    user.save(function () {
      purchaseOrder = {
        name: 'Purchase order name'
      };

      done();
    });
  });

  it('should be able to save a Purchase order if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Purchase order
        agent.post('/api/purchaseOrders')
          .send(purchaseOrder)
          .expect(200)
          .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
            // Handle Purchase order save error
            if (purchaseOrderSaveErr) {
              return done(purchaseOrderSaveErr);
            }

            // Get a list of Purchase orders
            agent.get('/api/purchaseOrders')
              .end(function (purchaseOrdersGetErr, purchaseOrdersGetRes) {
                // Handle Purchase order save error
                if (purchaseOrdersGetErr) {
                  return done(purchaseOrdersGetErr);
                }

                // Get Purchase orders list
                var purchaseOrders = purchaseOrdersGetRes.body;

                // Set assertions
                (purchaseOrders[0].user._id).should.equal(userId);
                (purchaseOrders[0].name).should.match('Purchase order name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Purchase order if not logged in', function (done) {
    agent.post('/api/purchaseOrders')
      .send(purchaseOrder)
      .expect(403)
      .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
        // Call the assertion callback
        done(purchaseOrderSaveErr);
      });
  });

  it('should not be able to save an Purchase order if no name is provided', function (done) {
    // Invalidate name field
    purchaseOrder.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Purchase order
        agent.post('/api/purchaseOrders')
          .send(purchaseOrder)
          .expect(400)
          .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
            // Set message assertion
            (purchaseOrderSaveRes.body.message).should.match('Please fill Purchase order name');

            // Handle Purchase order save error
            done(purchaseOrderSaveErr);
          });
      });
  });

  it('should be able to update an Purchase order if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Purchase order
        agent.post('/api/purchaseOrders')
          .send(purchaseOrder)
          .expect(200)
          .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
            // Handle Purchase order save error
            if (purchaseOrderSaveErr) {
              return done(purchaseOrderSaveErr);
            }

            // Update Purchase order name
            purchaseOrder.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Purchase order
            agent.put('/api/purchaseOrders/' + purchaseOrderSaveRes.body._id)
              .send(purchaseOrder)
              .expect(200)
              .end(function (purchaseOrderUpdateErr, purchaseOrderUpdateRes) {
                // Handle Purchase order update error
                if (purchaseOrderUpdateErr) {
                  return done(purchaseOrderUpdateErr);
                }

                // Set assertions
                (purchaseOrderUpdateRes.body._id).should.equal(purchaseOrderSaveRes.body._id);
                (purchaseOrderUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Purchase orders if not signed in', function (done) {
    // Create new Purchase order model instance
    var purchaseOrderObj = new PurchaseOrder(purchaseOrder);

    // Save the purchaseOrder
    purchaseOrderObj.save(function () {
      // Request Purchase orders
      request(app).get('/api/purchaseOrders')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Purchase order if not signed in', function (done) {
    // Create new Purchase order model instance
    var purchaseOrderObj = new PurchaseOrder(purchaseOrder);

    // Save the Purchase order
    purchaseOrderObj.save(function () {
      request(app).get('/api/purchaseOrders/' + purchaseOrderObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', purchaseOrder.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Purchase order with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/purchaseOrders/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Purchase order is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Purchase order which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Purchase order
    request(app).get('/api/purchaseOrders/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Purchase order with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Purchase order if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Purchase order
        agent.post('/api/purchaseOrders')
          .send(purchaseOrder)
          .expect(200)
          .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
            // Handle Purchase order save error
            if (purchaseOrderSaveErr) {
              return done(purchaseOrderSaveErr);
            }

            // Delete an existing Purchase order
            agent.delete('/api/purchaseOrders/' + purchaseOrderSaveRes.body._id)
              .send(purchaseOrder)
              .expect(200)
              .end(function (purchaseOrderDeleteErr, purchaseOrderDeleteRes) {
                // Handle purchaseOrder error error
                if (purchaseOrderDeleteErr) {
                  return done(purchaseOrderDeleteErr);
                }

                // Set assertions
                (purchaseOrderDeleteRes.body._id).should.equal(purchaseOrderSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Purchase order if not signed in', function (done) {
    // Set Purchase order user
    purchaseOrder.user = user;

    // Create new Purchase order model instance
    var purchaseOrderObj = new PurchaseOrder(purchaseOrder);

    // Save the Purchase order
    purchaseOrderObj.save(function () {
      // Try deleting Purchase order
      request(app).delete('/api/purchaseOrders/' + purchaseOrderObj._id)
        .expect(403)
        .end(function (purchaseOrderDeleteErr, purchaseOrderDeleteRes) {
          // Set message assertion
          (purchaseOrderDeleteRes.body.message).should.match('User is not authorized');

          // Handle Purchase order error error
          done(purchaseOrderDeleteErr);
        });

    });
  });

  it('should be able to get a single Purchase order that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Purchase order
          agent.post('/api/purchaseOrders')
            .send(purchaseOrder)
            .expect(200)
            .end(function (purchaseOrderSaveErr, purchaseOrderSaveRes) {
              // Handle Purchase order save error
              if (purchaseOrderSaveErr) {
                return done(purchaseOrderSaveErr);
              }

              // Set assertions on new Purchase order
              (purchaseOrderSaveRes.body.name).should.equal(purchaseOrder.name);
              should.exist(purchaseOrderSaveRes.body.user);
              should.equal(purchaseOrderSaveRes.body.user._id, orphanId);

              // force the Purchase order to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Purchase order
                    agent.get('/api/purchaseOrders/' + purchaseOrderSaveRes.body._id)
                      .expect(200)
                      .end(function (purchaseOrderInfoErr, purchaseOrderInfoRes) {
                        // Handle Purchase order error
                        if (purchaseOrderInfoErr) {
                          return done(purchaseOrderInfoErr);
                        }

                        // Set assertions
                        (purchaseOrderInfoRes.body._id).should.equal(purchaseOrderSaveRes.body._id);
                        (purchaseOrderInfoRes.body.name).should.equal(purchaseOrder.name);
                        should.equal(purchaseOrderInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      PurchaseOrder.remove().exec(done);
    });
  });
});
