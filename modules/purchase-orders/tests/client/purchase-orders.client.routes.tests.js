(function () {
  'use strict';

  describe('Purchase orders Route Tests', function () {
    // Initialize global variables
    var $scope,
      PurchaseOrdersService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _PurchaseOrdersService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      PurchaseOrdersService = _PurchaseOrdersService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('purchase-orders');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/purchase-orders');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          PurchaseOrdersController,
          mockPurchaseOrder;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('purchase-orders.view');
          $templateCache.put('modules/purchase-orders/client/views/view-purchase-order.client.view.html', '');

          // create mock Purchase order
          mockPurchaseOrder = new PurchaseOrdersService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Purchase order Name'
          });

          //Initialize Controller
          PurchaseOrdersController = $controller('PurchaseOrdersController as vm', {
            $scope: $scope,
            purchaseOrderResolve: mockPurchaseOrder
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:purchaseOrderId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.purchaseOrderResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            purchaseOrderId: 1
          })).toEqual('/purchase-orders/1');
        }));

        it('should attach an Purchase order to the controller scope', function () {
          expect($scope.vm.purchaseOrder._id).toBe(mockPurchaseOrder._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/purchase-orders/client/views/view-purchase-order.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          PurchaseOrdersController,
          mockPurchaseOrder;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('purchase-orders.create');
          $templateCache.put('modules/purchase-orders/client/views/form-purchase-order.client.view.html', '');

          // create mock Purchase order
          mockPurchaseOrder = new PurchaseOrdersService();

          //Initialize Controller
          PurchaseOrdersController = $controller('PurchaseOrdersController as vm', {
            $scope: $scope,
            purchaseOrderResolve: mockPurchaseOrder
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.purchaseOrderResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/purchase-orders/create');
        }));

        it('should attach an Purchase order to the controller scope', function () {
          expect($scope.vm.purchaseOrder._id).toBe(mockPurchaseOrder._id);
          expect($scope.vm.purchaseOrder._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/purchase-orders/client/views/form-purchase-order.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          PurchaseOrdersController,
          mockPurchaseOrder;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('purchase-orders.edit');
          $templateCache.put('modules/purchase-orders/client/views/form-purchase-order.client.view.html', '');

          // create mock Purchase order
          mockPurchaseOrder = new PurchaseOrdersService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Purchase order Name'
          });

          //Initialize Controller
          PurchaseOrdersController = $controller('PurchaseOrdersController as vm', {
            $scope: $scope,
            purchaseOrderResolve: mockPurchaseOrder
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:purchaseOrderId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.purchaseOrderResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            purchaseOrderId: 1
          })).toEqual('/purchase-orders/1/edit');
        }));

        it('should attach an Purchase order to the controller scope', function () {
          expect($scope.vm.purchaseOrder._id).toBe(mockPurchaseOrder._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/purchase-orders/client/views/form-purchaseOrder.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
