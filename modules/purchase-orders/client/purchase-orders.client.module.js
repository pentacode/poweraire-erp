(function (app) {
  'use strict';

  app.registerModule('purchase-orders');
  app.registerModule('purchase-orders.admin', ['core.admin']);
})(ApplicationConfiguration);
