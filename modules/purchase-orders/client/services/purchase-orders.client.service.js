//Purchase orders service used to communicate Purchase orders REST endpoints
(function () {
  'use strict';

  angular
    .module('purchase-orders')
    .factory('PurchaseOrdersService', PurchaseOrdersService);

  PurchaseOrdersService.$inject = ['$resource'];

  function PurchaseOrdersService($resource) {
    return $resource('api/purchase-orders/:purchaseOrderId', {
      purchaseOrderId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
