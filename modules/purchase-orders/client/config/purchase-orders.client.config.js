(function () {
  'use strict';

  angular
    .module('purchase-orders.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'purchasing', {
      title: 'Purchase Orders',
      state: 'purchase-orders.list',
      roles: ['Purchase Orders']
    });

    Menus.addSubMenuItem('topbar', 'purchasing', {
      title: 'Purchasing Calendar',
      state: 'purchase-orders.calendar',
      roles: ['Purchase Orders']
    });
  }
})();
