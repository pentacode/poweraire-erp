(function () {
  'use strict';

  angular
    .module('purchase-orders')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('purchase-orders', {
        abstract: true,
        url: '',
        template: '<ui-view/>'
      })
      .state('purchase-orders.list', {
        url: '/purchase-orders',
        templateUrl: 'modules/purchase-orders/client/views/list-purchase-orders.client.view.html',
        controller: 'PurchaseOrdersListController',
        controllerAs: 'vm',
        data: {
          roles: ['Purchase Orders'],
          pageTitle: 'Purchase orders List'
        }
      })
      .state('purchase-orders.calendar', {
        url: '/calendar',
        templateUrl: 'modules/purchase-orders/client/views/calendar-purchase-orders.client.view.html',
        controller: 'PurchasingCalendarController',
        controllerAs: 'vm',
        data: {
          roles: ['Purchase Orders'],
          pageTitle: 'Purchasing Calendar'
        }
      })
      .state('purchase-orders.create', {
        url: '/purchase-orders/create',
        templateUrl: 'modules/purchase-orders/client/views/form-purchase-order.client.view.html',
        controller: 'PurchaseOrdersController',
        controllerAs: 'vm',
        resolve: {
          purchase_orderResolve: newPurchaseOrder
        },
        data: {
          roles: ['Purchase Orders'],
          pageTitle : 'Purchase orders Create'
        }
      })
      .state('purchase-orders.edit', {
        url: '/purchase-orders/:purchaseOrderId/edit',
        templateUrl: 'modules/purchase-orders/client/views/form-purchase-order.client.view.html',
        controller: 'PurchaseOrdersController',
        controllerAs: 'vm',
        resolve: {
          purchase_orderResolve: getPurchaseOrder
        },
        data: {
          roles: ['Purchase Orders'],
          pageTitle: 'Edit Purchase order {{ purchase-orderResolve.name }}'
        }
      })
      .state('purchase-orders.receipts', {
        url: '/receipts/create/:purchaseOrderId',
        templateUrl: 'modules/receipts/client/views/form-receipt.client.view.html',
        controller: 'ReceiptsController',
        controllerAs: 'vm',
        resolve: {
          receiptResolve: newReceipt,
          purchase_orderResolve: getPurchaseOrder,
          triggeredFrom: getPageFrom
        },
        data: {
          roles: ['Receipts'],
          pageTitle : 'Receipts Create'
        }
      })
      .state('purchase-orders.editReceipts', {
        url: '/receipts/:receiptId/edit/:purchaseOrderId',
        templateUrl: 'modules/receipts/client/views/form-receipt.client.view.html',
        controller: 'ReceiptsController',
        controllerAs: 'vm',
        resolve: {
          receiptResolve: getReceipt,
          purchase_orderResolve: getPurchaseOrder,
          triggeredFrom: getPageFrom
        },
        data: {
          roles: ['Receipts'],
          pageTitle: 'Edit Receipt {{ receiptResolve.name }}'
        }
      })
    ;
  }
  
  function getPageFrom(){
    return 'purchaseOrder';
  }

  getPurchaseOrder.$inject = ['$stateParams', 'PurchaseOrdersService'];

  function getPurchaseOrder($stateParams, PurchaseOrdersService) {
    return PurchaseOrdersService.get({
      purchaseOrderId: $stateParams.purchaseOrderId
    }).$promise;
  }
    
  getReceipt.$inject = ['$stateParams', 'ReceiptsService'];

  function getReceipt($stateParams, ReceiptsService) {
    return ReceiptsService.get({
      receiptId: $stateParams.receiptId
    }).$promise;
  }

  newPurchaseOrder.$inject = ['PurchaseOrdersService'];
  newReceipt.$inject = ['ReceiptsService'];

  function newPurchaseOrder(PurchaseOrdersService) {
    return new PurchaseOrdersService();
  }

  function newReceipt(ReceiptsService) {
    return new ReceiptsService();
  }
})();
