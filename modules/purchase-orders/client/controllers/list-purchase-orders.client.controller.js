(function () {
	'use strict';

	angular
		.module('purchase-orders')
		.controller('PurchaseOrdersListController', PurchaseOrdersListController);

	PurchaseOrdersListController.$inject = ['$scope', '$ngConfirm', '$http', '$filter', 'SuppliersService', 'PurchaseOrdersService', 'alertify', 'CalendarsService'];

	function PurchaseOrdersListController($scope, $ngConfirm, $http, $filter, SuppliersService, PurchaseOrdersService, alertify, CalendarsService) {

		PurchaseOrdersService.query(function (data) {
			$scope.object = data;
			$scope.numbers = [];
			$scope.suppliers = [];
			$scope.createdList = [];
			for (var i = 0; i < $scope.object.length; i++) {
				var po = $scope.object[i];
				if ($scope.numbers.indexOf(po.number) === -1) {
					$scope.numbers.push(po.number);
				}
				if (po.supplier && $scope.suppliers.indexOf(po.supplier.name) === -1) {
					$scope.suppliers.push(po.supplier.name);
				}
				if (po.createdBy && $scope.createdList.indexOf(po.createdBy.username) === -1) {
					$scope.createdList.push(po.createdBy.username);
				}
			}

			$scope.buildPager();
		});

		$scope.module = 'Purchasing';

		$scope.postToGL = function () {
			if ($scope.period && $scope.module) {
				var query = {
					module: $scope.module,
				};
				for (var i = 0; i < $scope.calendars.length; i++) {
					var calendar = $scope.calendars[i];
					if (calendar.periodName === $scope.period.periodName) {
						query.period = calendar;
						break;
					}
				}
				$http.post('/api/journal-vouchers/autoCreate', query)
					.then(function () {
						alertify.success('Purchase Orders posted to GL');
					})
					.catch(function (err) {
						alertify.error(err.message);
					});
			} else {
				alertify.error('Please select a period and module.');
			}
		};


		CalendarsService.query(function (data) {
			$scope.calendars = data;
			$scope.periods = [];
			for (var i = 0; i < $scope.calendars.length; i++) {
				var calendar = $scope.calendars[i];
				if ($scope.periods.indexOf(calendar.periodName) === -1) {
					angular.forEach(calendar.module, function (modName) {
						if (modName.moduleName == 'Purchasing') {
							$scope.periods.push(calendar);
						}
					})
				}
			}
		});

		$scope.buildPager = function () {
			$scope.pagedItems = [];
			$scope.itemsPerPage = 10;
			$scope.currentPage = 1;
			$scope.figureOutItemsToDisplay();
		};

		$scope.openDate = function ($event, isDateFrom) {
			if (isDateFrom) {
				$scope.dateFromStatus.opened = true;
				return;
			}
			$scope.dateToStatus.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};


		$scope.dateFromStatus = {
			opened: false
		};

		$scope.dateToStatus = {
			opened: false
		};

		$scope.figureOutItemsToDisplay = function () {

			$scope.filteredItems = $filter('filter')($scope.object, function (obj) {
				if ($scope.number && !(new RegExp($scope.number).test(obj.number))) {
					return false;
				}
				if ($scope.supplier && !(new RegExp($scope.supplier).test(obj.supplier.name))) {
					return false;
				}
				if ($scope.created && !(new RegExp($scope.created).test(obj.createdBy.username))) {
					return false;
				}
				if ($scope.dateFrom && (Date.parse(obj.date) < Date.parse($scope.dateFrom))) {
					return false;
				}
				if ($scope.dateTo && (Date.parse(obj.date) > Date.parse($scope.dateTo))) {
					return false;
				}
				if ($scope.status && !(new RegExp($scope.status).test(obj.status))) {
					return false;
				}
				return true;
			});

			$scope.filterLength = $scope.filteredItems.length;
			var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
			var end = begin + $scope.itemsPerPage;
			$scope.pagedItems = $scope.filteredItems.slice(begin, end);
		};

		$scope.pageChanged = function () {
			$scope.figureOutItemsToDisplay();
		};

		$scope.clear = function () {
			$scope.number = '';
			$scope.supplier = '';
			$scope.created = '';
			$scope.dateFrom = '';
			$scope.dateTo = '';
			$scope.status = '';
			$scope.figureOutItemsToDisplay();
		};

		$scope.searchNumber = function () {
			$scope.sc = [];
			$scope.sc = $filter('filter')($scope.numbers, function (number) {
				if ($scope.number && !(new RegExp('^' + $scope.number).test(number))) {
					return false;
				}
				return true;
			});
		};

		$scope.searchName = function () {
			$scope.sn = [];
			$scope.sn = $filter('filter')($scope.suppliers, function (supplier) {
				if ($scope.supplier && !(new RegExp($scope.supplier, 'i').test(supplier))) {
					return false;
				}
				return true;
			});
		};

		$scope.searchUser = function () {
			$scope.user = [];
			$scope.user = $filter('filter')($scope.createdList, function (created) {
				if ($scope.created && !(new RegExp($scope.created, 'i').test(created))) {
					return false;
				}
				return true;
			});
		};

		$scope.getTotalPrice = function (lines) {
			var total = 0;
			for (var i = 0; i < lines.length; i++) {
				var amt = lines[i].purchasedPrice * lines[i].ordered;
				total += amt;
			}
			return total;
		};
	}
})();