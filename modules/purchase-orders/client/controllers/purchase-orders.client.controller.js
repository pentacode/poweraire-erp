(function () {
  'use strict';

  // Purchase orders controller
  angular
    .module('purchase-orders')
    .controller('PurchaseOrdersController', PurchaseOrdersController);

  PurchaseOrdersController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'SuppliersService', 'ItemsService', 'ReceiptsService', 'ReferencesService', 'purchase_orderResolve','alertify','Util'];

  function PurchaseOrdersController ($scope, $filter, $state, Authentication, SuppliersService, ItemsService, ReceiptsService, ReferencesService, purchaseOrder,alertify, Util) {
    var vm = this;

    vm.authentication = Authentication;
    vm.purchaseOrder = purchaseOrder;
    vm.supplier = SuppliersService.query({ status : true });
    vm.items = ItemsService.query();
    vm.uoms = ReferencesService.query({ code : 'UOM', status : true });
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.addLine = addLine;
    vm.deleteLine = deleteLine;
    vm.selectItem = selectItem;
    vm.cancelPo = cancelPo;
    vm.computeTotal = computeTotal;
    vm.number = 0;

    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.purchaseOrderForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.purchaseOrderForm.$dirty = false;
            $state.go(toState);
          });
        }
    });
      
    if (purchaseOrder._id){
      vm.receipts = ReceiptsService.query({ purchaseOrder : purchaseOrder._id });
    }else{
      vm.receipts = [];
      vm.items = ItemsService.query({ status : true });
    }
      
    if (!purchaseOrder.lines)
      vm.purchaseOrder.lines = [];
      
    function addLine(obj){
      obj.push({
        number: ++vm.number,
        item: '',
        uom: '',
        ordered: 0,
        received: 0,
        cancelled: 0,
        purchasedPrice: 0.00,
        status: 'Open',
        cancellationDate: ''
      });
    }

    vm.purchaseOpenDate = openDatePO;

    vm.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    vm.poDate = {
      opened: false
    };

    if(_.isEmpty(vm.purchaseOrder.date) && _.isEmpty(vm.purchaseOrder._id)){
      vm.purchaseOrder.date = new Date().setHours(0,0,0,0);

    } else {
      vm.purchaseOrder.date = new Date(vm.purchaseOrder.date).setHours(0,0,0,0);
    }

    function openDatePO($event) {
        vm.poDate.opened = true;  
    }

    function cancelPo(obj){      
      alertify.confirm('Are you sure you want to close whole Purchase Order?', function () {
        obj.status = 'Closed';
        savePurchaseOrder(obj, false);
      }, function() {
        
      });
      
    }
      
    function deleteLine(index){
      vm.purchaseOrder.lines.splice(index, 1);
    }

    function selectItem(obj){
      for (var i = 0; i < vm.items.length; i++) {
        var item = vm.items[i];
        if (item._id === obj.item._id){
          obj.purchasedPrice = item.purchasedPrice;
          obj.uom = item.uom;
        }
      }
    }
    
    function computeTotal(lines){
      var total = 0;
      for (var i = 0; i < lines.length; i++) {
        var amt = lines[i].purchasedPrice * lines[i].ordered;
        total += amt;
      }
      return total;
    }
      
    // Remove existing Purchase order
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.purchaseOrder.$remove($state.go('purchase-orders.list'));
      }
    }

    // Save Purchase order
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.purchaseOrderForm');
        vm.error = 'Error in form values';
        alertify.error(vm.error);
        return false;
      }
      if (vm.purchaseOrder.lines.length===0){
        vm.error = 'Item Line is required';
        alertify.error(vm.error);
        return false;
      }
      var itemList = [];
      for (var i = 0; i < vm.purchaseOrder.lines.length; i++) {
        var cnt = i + 1;
        var lines = vm.purchaseOrder.lines[i];
        var itemid = lines.item._id;
        
        if (itemList.includes(itemid)){
          vm.error = 'Duplicate Item is not allowed';
          alertify.error(vm.error);
          return false;
        }else{
          itemList.push(itemid);
        }
        
        var total = lines.ordered - lines.received;
        if (lines.cancelled !== 0 && lines.cancelled > total){
          vm.error = 'Line ' + cnt + ': Cancelled + Received should not be greater than Quantity';
          alertify.error(vm.error);
          return false;
        }
      }
      
      var isCreate = vm.purchaseOrder._id ? false : true;
      savePurchaseOrder(vm.purchaseOrder, isCreate);
    }

    function savePurchaseOrder(purchaseOrder, isCreate){
      if (isCreate) {
        purchaseOrder.$save(successCallback, errorCallback);
      } else {
        purchaseOrder.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.error = null;
        vm.success = 'Purchase Order successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.purchaseOrderForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        if(res.data){
          vm.error = res.data.message;
        } else {
          vm.error = "Error in creating PO"
        }
        alertify.error(vm.error);
      }
    }
    
  }
})();
