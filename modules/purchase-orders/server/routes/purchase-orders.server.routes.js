'use strict';

/**
 * Module dependencies
 */
var purchaseOrdersPolicy = require('../policies/purchase-orders.server.policy'),
  purchaseOrders = require('../controllers/purchase-orders.server.controller');

module.exports = function(app) {
  // Purchase orders Routes
  app.route('/api/purchase-orders').all(purchaseOrdersPolicy.isAllowed)
    .get(purchaseOrders.list)
    .post(purchaseOrders.create);

  app.route('/api/purchase-orders/:purchaseOrderId').all(purchaseOrdersPolicy.isAllowed)
    .get(purchaseOrders.read)
    .put(purchaseOrders.update);

  // Finish by binding the Purchase order middleware
  app.param('purchaseOrderId', purchaseOrders.purchaseOrderByID);
};
