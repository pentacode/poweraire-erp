'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  sequenceModel = mongoose.model('Sequence'),
  Schema = mongoose.Schema;

/**
 * Purchase order Schema
 */
var PurchaseOrderSchema = new Schema({
  number: Number,
  supplier: {
    type: Schema.ObjectId,
    ref: 'Supplier'
  },
  comments: {
    type: String,
    default: ''
  },
  date: Date,
  lines: [{
    number: {
      type: Number,
      min: [1, 'Line number cannot be less than 1.'],
      required: 'Line number is required.'
    },
    item: {
      type: Schema.ObjectId,
      ref: 'Item'
    },
    uom: {
      type: Schema.ObjectId,
      ref: 'Reference'
    },
    ordered: {
      type: Number,
      min: [1, 'Quantity ordered cannot be less than 1.'],
      required: 'Quantiy ordered is required.'
    },
    received: {
      type: Number,
      default: 0
    },
    cancelled: {
      type: Number,
      default: 0
    },
    purchasedPrice: {
      type: Number,
      min: [0, 'Item purchased price cannot be negative.']
    },
    status: {
      type: String,
      enum: ['Open', 'Partially Received', 'Completed', 'Cancelled']
    },
    cancellationDate: Date
  }],
  status: {
    type: String,
    enum: ['Open', 'Processing', 'Completed', 'Closed']
  },
  closedDate: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

PurchaseOrderSchema.pre('save', function(next) {
  var po = this;
  var currentDate = new Date();
  var user = po.user;
  po.updated = currentDate;
  if (user) {
    po.updatedBy = user;
  }
  // update line status
  var isComplete = true;
  for (var i = 0; i < po.lines.length; i++) {
    var line = po.lines[i];
    if (line.ordered === line.cancelled) {
      line.status = 'Cancelled';
    } else if (line.ordered === (line.received + line.cancelled)) {
      line.status = 'Completed';
    } else if (line.received > 0) {
      line.status = 'Partially Received';
    } else {
      line.status = 'Open';
    }
    if (po.status === 'Closed') {
      if (line.status === 'Partially Received') {
        line.cancelled = line.ordered - line.received;
        line.status = 'Completed';
      } else if (line.status === 'Open') {
        line.cancelled = line.ordered;
        line.status = 'Cancelled';
      }
    }
    if (line.status === 'Open' || line.status === 'Partially Received') {
      isComplete = false;
    }
    if (line.cancelled > 0) {
      line.cancellationDate = line.cancellationDate ? line.cancellationDate : currentDate;
    } else {
      line.cancellationDate = null;
    }
  }
  if (isComplete && po.status !== 'Closed') {
    po.status = 'Completed';
  }
  // update closed date
  if (po.status !== 'Closed') {
    po.closedDate = null;
  } else if (!po.closedDate) {
    po.closedDate = currentDate;
  }
  if (!po.created){
    po.created = currentDate;
    po.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'Purchase Order' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        return next(err);
      }
      po.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('PurchaseOrder', PurchaseOrderSchema);
