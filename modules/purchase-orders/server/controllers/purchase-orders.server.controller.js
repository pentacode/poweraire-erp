'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  PurchaseOrder = mongoose.model('PurchaseOrder'),
  async = require('async'),
  Calendar = mongoose.model('Calendar'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Purchase order
 */
exports.create = function(req, res) {
  var purchaseOrder = new PurchaseOrder(req.body);
  purchaseOrder.user = req.user;
  purchaseOrder.status = 'Open';
  var query = {module : {$elemMatch: {moduleName: 'Purchasing', status: 'Open'}}}
  Calendar.findOne(query).exec(function(err, record){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    if (record.length === 0) {
      return res.status(400).send({
        message: 'Cannot create PO (No existing period for date: ' + purchaseOrder.date + ')'
      });
    } else {
      var calendar = record.module.find(function( obj ) { return obj.moduleName == 'Purchasing'; });
      console.log(calendar);
      if (calendar.status === 'Closed' || calendar.status === 'Future') {
        return res.status(400).send({
          message: 'Purchasing Calendar has status of ' +  calendar.status + '.\nCannot create PO for the specified date, calendar must have OPEN status'
        });
      } else {
        purchaseOrder.save(function(err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            PurchaseOrder.populate(purchaseOrder, [{ 
              path:'lines',
              populate: [{
                path: 'item',
                model: 'Item',
                populate : {
                  path: 'uom',
                  model: 'Reference'
                }
              }, {
                path: 'uom',
                model: 'Reference'
              }]
            }, {
              path: 'supplier',
              model: 'Supplier'
            }], function(err, purchaseOrder) {
              if (err) {
                return res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              } else {
                res.jsonp(purchaseOrder);
              }
            });
          }
        });
      }
    }
  })
};

/**
 * Show the current Purchase order
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var purchaseOrder = req.purchaseOrder ? req.purchaseOrder.toJSON() : {};

  res.jsonp(purchaseOrder);
};

/**
 * Update a Purchase order
 */
exports.update = function(req, res) {
  var purchaseOrder = req.purchaseOrder;
  purchaseOrder.user = req.user;

  purchaseOrder = _.extend(purchaseOrder , req.body);
  
  purchaseOrder.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      PurchaseOrder.populate(purchaseOrder, {
        path:'lines',
        populate: [{ 
          path: 'item',
          model: 'Item',
          populate : {
            path: 'uom',
            model : 'Reference'
          }
        }, {
          path: 'uom',
          model: 'Reference'
        }]
      }, function(err, purchaseOrder) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.jsonp(purchaseOrder);
        }
      });
    }
  });
};

/**
 * List of Purchase orders
 */
exports.list = function(req, res) {
  var query = {};
  if (req.query.status) {
    query.status = { $in : req.query.status };
  }
  PurchaseOrder.find(query)
    .populate({
      path:'lines',
      populate: [{ 
        path: 'item',
        model: 'Item',
        populate : {
          path: 'uom',
          model : 'Reference'
        }
      }, {
        path: 'uom',
        model: 'Reference'
      }]
    }).populate('supplier').populate('createdBy').exec(function(err, purchaseOrders) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(purchaseOrders);
    }
  });
};

/**
 * Purchase order middleware
 */
exports.purchaseOrderByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Purchase order is invalid'
    });
  }
  PurchaseOrder.findById(id)
  .populate({
    path : 'lines',
    populate : [{
      path : 'item',
      model : 'Item',
    }, {
      path : 'uom',
      model : 'Reference'
    }]
  }).populate({
    path : 'supplier',
    populate : {
      path : 'vat',
      model : 'Tax',
    }
  }).populate('supplier').populate('createdBy').populate('updatedBy').exec(function (err, purchaseOrder) {
    if (err) {
      return next(err);
    } else if (!purchaseOrder) {
      return res.status(404).send({
        message: 'No Purchase order with that identifier has been found'
      });
    }
    req.purchaseOrder = purchaseOrder;
    next();
  });
};