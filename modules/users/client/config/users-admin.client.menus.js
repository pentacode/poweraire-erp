'use strict';

// Configuring the Articles module
angular.module('users.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Users',
      state: 'admin.users',
      roles: ['Users']
    });
  }
]);
