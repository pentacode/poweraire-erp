'use strict';

angular.module('users.admin').controller('UserListController', ['$scope', '$filter', 'Admin',
  function ($scope, $filter, Admin) {
    $scope.type = '';
    $scope.status = '';
    
    Admin.query(function (data) {
      $scope.users = data;
      $scope.usernames = [];
      $scope.firstNames = [];
      $scope.lastNames = [];
      for (var i = 0; i < $scope.users.length; i++) {
        var user = $scope.users[i];
        if ($scope.usernames.indexOf(user.username) === -1) {
          $scope.usernames.push(user.username);
        }
        if ($scope.firstNames.indexOf(user.firstName) === -1) {
          $scope.firstNames.push(user.firstName);
        }
        if ($scope.lastNames.indexOf(user.lastName) === -1) {
          $scope.lastNames.push(user.lastName);
        }
      }
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, function(user){
        if ($scope.username && !(new RegExp($scope.username, 'i').test(user.username))) {
          return false;
        }
        if ($scope.lastName && !(new RegExp($scope.lastName, 'i').test(user.lastName))) {
          return false;
        }
        if ($scope.firstName && !(new RegExp($scope.firstName, 'i').test(user.firstName))) {
          return false;
        }
        if ($scope.status && ($scope.status === 'true') !== user.status) {
          return false;
        }
        if ($scope.type && user.roles.indexOf($scope.type) === -1) {
          return false;
        }
        return true;
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.clear = function() {
      $scope.username = '';
      $scope.lastName = '';
      $scope.firstName = '';
      $scope.status = '';
      $scope.type = '';
      $scope.figureOutItemsToDisplay();
    };

    $scope.searchUsername = function () {
      $scope.uns = [];
      $scope.uns = $filter('filter')($scope.usernames, function(username){
        if ($scope.username && !(new RegExp('^' + $scope.username, 'i').test(username))) {
          return false;
        }
        return true;
      });
    };

    $scope.searchLastname = function () {
      $scope.lns = [];
      $scope.lns = $filter('filter')($scope.lastNames, function(lastName){
        if ($scope.lastName && !(new RegExp($scope.lastName, 'i').test(lastName))) {
          return false;
        }
        return true;
      });
    };

    $scope.searchFirstname = function () {
      $scope.fns = [];
      $scope.fns = $filter('filter')($scope.firstNames, function(firstName){
        if ($scope.firstName && !(new RegExp($scope.firstName, 'i').test(firstName))) {
          return false;
        }
        return true;
      });
    };

    $scope.getUserType = function(roles) {
      for (var i = 0; i < roles.length; i++) {
        if (roles[i] === 'Admin') {
          return 'Admin';
        } else if (roles[i] === 'Staff') {
          return 'Staff';
        } else if (roles[i] === 'Employee') {
          return 'Employee';
        }
      }
    }
  }
]);
