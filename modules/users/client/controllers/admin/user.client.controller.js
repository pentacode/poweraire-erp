'use strict';

angular.module('users.admin').controller('UserController', ['$scope', '$filter', '$state', 'Authentication', 'userResolve', 'alertify', 'Util',
  function ($scope, $filter, $state, Authentication, userResolve, alertify, Util) {
    $scope.authentication = Authentication;
    $scope.user = userResolve;
    $scope.type = 'Employee';
    $scope.selectedCategories = [];
    $scope.selectedModules = [];
    $scope.categories = [{
      category : 'Maintenance',
      modules : ['Bank Accounts', 'Calendar', 'Checks', 'Combinations', 'Company Profile', 'Customers', 'Items', 'References', 'Suppliers', 'Taxes', 'Users']
    }, {
      category : 'Inventory',
      modules : ['On-hand Quantity', 'Transactions']
    }, {
      category : 'Purchasing',
      modules : ['Purchase Orders', 'Receipts']
    }];

    $scope.$watch('user.roles', function(){
      if (!$scope.user.roles) {
        return;
      }
      for (var i = 0; i < $scope.categories.length; i++) {
        var category = $scope.categories[i];
        var matchedModules = [];
        for(var j = 0; j < category.modules.length; j++){
          var module = category.modules[j];
          if($scope.user.roles.indexOf(module) > -1 && $scope.selectedModules.indexOf(module) < 0){
            $scope.selectedModules.push(module);
            matchedModules.push(module);
          }
        }
        if(((matchedModules.length === category.modules.length && category.modules.length > 0) || $scope.user.roles.indexOf(category.category) > -1) && $scope.selectedCategories.indexOf(category.category) < 0) {
          $scope.selectedCategories.push(category.category);
        }
      }

      var roles = ['Admin','Staff','Employee'];
      roles.forEach(function(role) {
        if($scope.user.roles.indexOf(role) > -1){
          $scope.type = role;
          return;
        }
      });
    });

    $scope.$on('$stateChangeStart', function(event, toState) {
      if($scope.userForm.$dirty){
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
          $scope.userForm.$dirty = false;
          $state.go(toState);
        });
      }
    });

    function getCategoryModules(selectedCategory){
      var modules = [];
      for (var i = 0; i < $scope.categories.length; i++) {
        var category = $scope.categories[i];
        if(selectedCategory === category.category){
          for (var j = 0; j < category.modules.length; j++) {
            var module = category.modules[j];
            modules.push(module);
          }
          break;
        }
      }
      return modules;
    }

    function clearUserData(){
      $scope.user.password = '';
      $scope.user.username = '';
    }

    function getModuleCategory(selectedModule){
      for (var i = 0; i < $scope.categories.length; i++) {
        var category = $scope.categories[i];
        if(category.modules.length > 0 && category.modules.indexOf(selectedModule) > -1){
          return category.category;
        }
      }
      return '';
    }

    $scope.toggle = function(selected, isCategory) {
      if(isCategory){
        var modules = getCategoryModules(selected);
        if($scope.selectedCategories.indexOf(selected) > -1) { // category deselect
          modules.forEach(function(element){
            $scope.selectedModules.splice($scope.selectedModules.indexOf(element), 1);
          });
          $scope.selectedCategories.splice($scope.selectedCategories.indexOf(selected), 1);
        } else {
          modules.forEach(function(element){
            if($scope.selectedModules.indexOf(element) < 0){
              $scope.selectedModules.push(element);
            }
          });
          $scope.selectedCategories.push(selected);
        }
      } else {
        var category = getModuleCategory(selected);
        var modules = getCategoryModules(category);
        if($scope.selectedModules.indexOf(selected) > -1) { // module deselect
          if($scope.selectedCategories.indexOf(category) > -1){
            $scope.selectedCategories.splice($scope.selectedCategories.indexOf(category), 1);
          }
          $scope.selectedModules.splice($scope.selectedModules.indexOf(selected), 1);
        } else {
          $scope.selectedModules.push(selected);
          var isCategoryChecked = modules.every(function(element){
            return $scope.selectedModules.indexOf(element) > - 1;
          });
          if(isCategoryChecked) {
            $scope.selectedCategories.push(category);
          }
        }
      }
    };

    $scope.save = function(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');
        return false;
      }

      var user = $scope.user;
      user.roles = [ $scope.type ];
      user.roles = user.roles.concat($scope.selectedModules);

      var isCreate = user._id ? false : true;
      if (isCreate) {
        $scope.user.$save(successCallback, errorCallback);
      } else {
        $scope.user.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        $scope.success = 'User successfully ';
        $scope.user.password = '';
        if (isCreate) {
          $scope.success = $scope.success + 'created';
        } else {
          $scope.success = $scope.success + 'updated';
        }
        $scope.userForm.$dirty = false;
        alertify.success($scope.success);
      }

      function errorCallback(res) {
        $scope.error = res.data.message;
        alertify.error($scope.error);
      }
    };
  }
]);
