'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  sequenceModel = mongoose.model('Sequence'),
  Schema = mongoose.Schema;

/**
 * Receipt Schema
 */
var ReceiptSchema = new Schema({
  number: Number,
  purchaseOrder: {
    type: Schema.ObjectId,
    ref: 'PurchaseOrder'
  },
  date: Date,
  comments: {
    type: String,
    default: ''
  },
  lines: [{
    number: {
      type: Number,
      min: [1, 'Line number cannot be less than 1.'],
      required: 'Line number is required.'
    },
    quantity: {
      type: Number,
      min: [1, 'Quantity cannot be less than 1.'],
      required: 'Quantity is required.'
    },
    chargeAccount: {
      type: Schema.ObjectId,
      ref: 'Combination'
    },
    serials: [{
      serial: {
        type: String,
        default : 'N/A'
      },
      subInventory: {
        type: Schema.ObjectId,
        ref: 'Reference'
      },
      quantity: {
        type: Number,
        min: [1, 'Quantity cannot be less than 1.'],
        required: 'Quantity is required.'
      }
    }]
  }],
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

ReceiptSchema.pre('save', function(next) {
  var rr = this;
  var currentDate = new Date();
  var user = rr.user;
  rr.updated = currentDate; 
  rr.updatedBy = user;
  if(!rr.created){
    rr.created = currentDate;
    rr.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'Receipt' }, { $inc: { value : 1 } }, function(error, sequence) {
      if (error) {
        return next(error);
      }
      rr.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('Receipt', ReceiptSchema);
