'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Receipt = mongoose.model('Receipt'),
  Calendar = mongoose.model('Calendar'),
  async = require('async'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  processHandler = require(path.resolve('./modules/core/server/controllers/process.server.controller')),
  _ = require('lodash');

/**
 * Create a receipt (use process.server.controller for process flow)
 */
exports.create = function(req, res) {
  var receipt = new Receipt(req.body);
  receipt.user = req.user;

  var query = { module : {$elemMatch: {moduleName: 'Inventory', status: 'Open'}}, fromDate : { $lte : receipt.date }, toDate : { $gte : receipt.date} };
  console.log(receipt);
  
  Calendar.findOne(query).exec(function(err, record){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    if (record.length === 0) {
      return res.status(400).send({
        message: 'Cannot create receipt (No existing period for date: ' + receipt.date + ')'
      });
    } else {
      var calendar = record.module.find(function( obj ) { return obj.moduleName == 'Inventory'; });
      console.log(calendar);
        if(calendar.status === 'Closed' || calendar.status === 'Future'){
          res.status(400).send({
            message: 'Receipt Calendar is ' +  calendar.status + ' Cannot create transaction for the specified date, calendar must have OPEN status'
          });
        }else{
          receipt.save(function(err){
            if (err) {
                return res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              } else {
                // handle successful receipt creation
                processHandler.handleReceipt(receipt, function(success){
                  if (success) {
                    res.jsonp(receipt);
                  } else {
                    receipt.remove();
                    return res.status(400).send({
                      message: 'Something went wrong.'
                  });
                }
              });
            }
          });          
        }
      }
  });
};

/**
 * Show the current Receipt
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var receipt = req.receipt ? req.receipt.toJSON() : {};

  res.jsonp(receipt);
};

/**
 * Update a Receipt
 * Note: This method will 
 * never be used since Receipt 
 * cannot be edited
 */
exports.update = function(req, res) {
  var receipt = req.receipt;
  receipt.user = req.user;

  receipt = _.extend(receipt , req.body);

  receipt.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(receipt);
    }
  });
};

/**
 * List of Receipts
 */
exports.list = function(req, res) {
  var query = {};
  if (req.query.purchaseOrder) {
    query.purchaseOrder = new mongoose.mongo.ObjectId(req.query.purchaseOrder);
  }
  Receipt.find(query).sort('-created')
  .populate({
    path : 'purchaseOrder',
    model : 'PurchaseOrder',
    populate : {
      path: 'supplier',
      select : 'name',
      model : 'Supplier'           
    }
  }).populate('createdBy').populate('updatedBy').exec(function(err, receipts) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(receipts);
    }
  });
};

/**
 * Receipt middleware
 */
exports.receiptByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Receipt is invalid'
    });
  }
  Receipt.findById(id).populate({
    path : 'purchaseOrder',
    model : 'PurchaseOrder',
    populate : [{
      path: 'supplier',
      select : 'name',
      model : 'Supplier'
    }, {
      path: 'line',
      populate: {
        path: 'item',
        model: 'Item'
      }
    }]
  }).populate('createdBy').populate('updatedBy').exec(function (err, receipt) {
    if (err) {
      return next(err);
    } else if (!receipt) {
      return res.status(404).send({
        message: 'No Receipt with that identifier has been found'
      });
    }
    req.receipt = receipt;
    next();
  });
};
