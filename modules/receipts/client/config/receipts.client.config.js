(function () {
  'use strict';

  angular
    .module('receipts.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'purchasing', {
      title: 'Receipts',
      state: 'receipts.list',
      roles: ['Receipts']
    });
  }
})();
