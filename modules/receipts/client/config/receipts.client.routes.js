(function () {
  'use strict';

  angular
    .module('receipts')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('receipts', {
        abstract: true,
        url: '',
        template: '<ui-view/>'
      })
      .state('receipts.list', {
        url: '/receipts',
        templateUrl: 'modules/receipts/client/views/list-receipts.client.view.html',
        controller: 'ReceiptsListController',
        controllerAs: 'vm',
        data: {
          roles: ['Receipts'],
          pageTitle: 'Receipts List'
        }
      })
      .state('receipts.create', {
        url: '/receipts/create',
        templateUrl: 'modules/receipts/client/views/form-receipt.client.view.html',
        controller: 'ReceiptsController',
        controllerAs: 'vm',
        resolve: {
          receiptResolve: newReceipt,
          purchase_orderResolve: getNewPurchaseOrder,
          triggeredFrom: getPageFrom
        },
        data: {
          roles: ['Receipts'],
          pageTitle : 'Receipts Create'
        }
      })
      .state('receipts.edit', {
        url: '/receipts/:receiptId/edit/:purchaseOrderId',
        templateUrl: 'modules/receipts/client/views/form-receipt.client.view.html',
        controller: 'ReceiptsController',
        controllerAs: 'vm',
        resolve: {
          receiptResolve: getReceipt,
          purchase_orderResolve: getPurchaseOrder,
          triggeredFrom: getPageFrom
        },
        data: {
          roles: ['Receipts'],
          pageTitle: 'Edit Receipt {{ receiptResolve.name }}'
        }
      })
      .state('receipts.purchaseOrder', {
        url: '/purchase-orders/:purchaseOrderId/edit',
        templateUrl: 'modules/purchase-orders/client/views/form-purchase-order.client.view.html',
        controller: 'PurchaseOrdersController',
        controllerAs: 'vm',
        resolve: {
          purchase_orderResolve: getPurchaseOrder
        },
        data: {
          roles: ['Purchase Orders'],
          pageTitle: 'Edit Purchase order {{ purchase-orderResolve.name }}'
        }
      })
    ;
  }
    
  function getPageFrom(){
    return 'receipts';
  }
  
  getPurchaseOrder.$inject = ['$stateParams', 'PurchaseOrdersService'];

  function getPurchaseOrder($stateParams, PurchaseOrdersService) {
    return PurchaseOrdersService.get({
      purchaseOrderId: $stateParams.purchaseOrderId
    }).$promise;
  }
  
  getNewPurchaseOrder.$inject = ['$stateParams', 'PurchaseOrdersService'];
  
  function getNewPurchaseOrder($stateParams, PurchaseOrdersService){
    return PurchaseOrdersService.query({ status : [ 'Processing', 'Open' ] });
  }

  getReceipt.$inject = ['$stateParams', 'ReceiptsService'];

  function getReceipt($stateParams, ReceiptsService) {
    return ReceiptsService.get({
      receiptId: $stateParams.receiptId
    }).$promise;
  }

  newReceipt.$inject = ['ReceiptsService'];

  function newReceipt(ReceiptsService) {
    return new ReceiptsService();
  }
})();
