(function () {
  'use strict';

  // Receipts controller
  angular
    .module('receipts')
    .controller('ReceiptsController', ReceiptsController);

  ReceiptsController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'PurchaseOrdersService', 'CombinationsService', 'receiptResolve', 'purchase_orderResolve', 'triggeredFrom', 'alertify', '_', 'ReferencesService', 'OnhandsService','Util','$modal', 'CompaniesService'];

  function ReceiptsController ($scope, $filter, $state, Authentication, PurchaseOrdersService, CombinationsService, receipt, purchaseOrder, fromUI, alertify, _, ReferencesService, OnhandsService, Util,$modal,CompaniesService) {
    var vm = this;
    vm.authentication = Authentication;
    vm.receipt = receipt;
    vm.purchaseOrder = purchaseOrder;
    vm.chargeAccount = [];
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.addLine = addLine;
    vm.showSerials = showSerials;
    vm.addSerial = addSerial;
    vm.deleteLine = deleteLine;
    vm.deleteSerial = deleteSerial;
    vm.getAmountPerLine = getAmountPerLine;
    vm.computeTotal= computeTotal;
    vm.getPoLine = getPoLine;
    vm.currentPoLine = {};
    vm.searchPONumber = searchPONumber;
    vm.selectPONumber = selectPONumber;
    vm.resetReceipt = resetReceipt;
    vm.statusChecking = statusChecking;
    vm.subInventories = [];
    vm.poLine = [];
    vm.existingSerials = [];
    vm.ponumber = "";
    vm.fromPurchaseUI = fromUI === "purchaseOrder" ? true : false ;
    vm.openActionCodeLookup = openActionCodeLookup;
    vm.getPopOverDescription = getPopOverDescription; 

    vm.companyProfile = CompaniesService.get({
      companyId : 0
    });

    vm.openDate = openDate;

    vm.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };


    vm.date = {
      opened: false
    };

    if(_.isEmpty(vm.receipt.date) && _.isEmpty(vm.receipt._id)){
      vm.receipt.date = new Date().setHours(0,0,0,0);
    }

    function openDate($event) {
        vm.date.opened = true;  
    };

    function getPopOverDescription(chargeAccount){
      var description = "";
      if(!_.isEmpty(chargeAccount)){
         var combination = _.find(vm.combinations, { '_id' : chargeAccount});
         if(!_.isEmpty(combination)){
            description = combination.accountDescription;
         }
      }
        return description;
    }

    
    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.receipt.lines[selected.lineIndex].chargeAccount = selected.combination._id;
        }, null);

    }
    
    if (!angular.isArray(purchaseOrder)){
      vm.receipt.purchaseOrder = vm.purchaseOrder;
      vm.ponumber = vm.purchaseOrder.number;
      getPOLines(vm.purchaseOrder);
    }

    function getPOLines(purchaseOrder){
      if (vm.receipt._id)
        return;
      
      vm.poLine = [];
      OnhandsService.query(function(data){
        for(var i = 0; i < purchaseOrder.lines.length; i++){
          var line = purchaseOrder.lines[i];
          if (!statusChecking(line.status)){
            vm.receipt.lines.push({
              number: line.number,
              quantity: 0,
              chargeAccount: vm.companyProfile.accountCode,
              serials: []
            });
          }else{
            vm.poLine.push(line);
          }
          
          for (var j = 0; j < data.length; j++) {
            var onhand = data[j];
            if(onhand.item._id === line.item._id){
              angular.extend(vm.existingSerials, onhand.serials);
            }
          }
        }
        for(var k = 0; k < vm.existingSerials.length; k++){
          var existing = vm.existingSerials[k];
          var subinv = existing.subInventory;
          existing.displaySerial = existing.serial + ' ( ' + $scope.getIfhasValue(subinv.descriptionOne) + ' - ' + $scope.getIfhasValue(subinv.descriptionTwo) + ' - ' + $scope.getIfhasValue(subinv.descriptionThree) + ' )';
        }
      });
    }
    
    function resetReceipt(){
      vm.receipt = {};
      vm.ponumber = "";
    }
    
    function selectPONumber(){
      if (typeof(vm.ponumber) === "object"){
        vm.receipt.purchaseOrder = vm.ponumber;
        getPOLines(vm.ponumber);
        vm.receipt.lines = [];
      }
    }
    
    function searchPONumber(){
      var poList = vm.purchaseOrder;      
      vm.po = $filter('filter')(poList, function(po){
        if (vm.ponumber && !(new RegExp('^' + vm.ponumber).test(po.number))) {
          return false;
        }
        return true;
      });
    }
    
    function getPoLine(receiptNumber){
      var poObject = vm.receipt.purchaseOrder;
      for (var j = 0; j < poObject.lines.length; j++) {
        var poLine = poObject.lines[j];
          if (receiptNumber === poLine.number) {
            vm.currentPoLine = poLine;
          }
      }
    }
    
    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.receiptForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.receiptForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    ReferencesService.query({ code : 'SUBINV', status : true }, function(data){
      for (var i = 0; i < data.length; i++) {
        var subinv = data[i];
        subinv.value = $scope.getIfhasValue(subinv.descriptionOne) + ' - ' + $scope.getIfhasValue(subinv.descriptionTwo) + ' - ' + $scope.getIfhasValue(subinv.descriptionThree);
        vm.subInventories.push(subinv);
      }
    });

    $scope.getIfhasValue = function(arg){
      return arg ? arg : 'N/A';
    };


    $scope.onSerialSelect = function($model, $item, $label){
      $model.subInventory = $item.subInventory._id;
      $model.serial = $item.serial;
    };

    function computeTotal(lines, poLine){
      var total = 0;
      for (var i = 0; i < receipt.lines.length; i++) {
        var line = receipt.lines[i];
        for (var j = 0; j < receipt.purchaseOrder.lines.length; j++){
          var poLine = receipt.purchaseOrder.lines[j];
          if (line.number === poLine.number){
            total += (poLine.purchasedPrice * line.quantity);
          }
        }
      }
      return total;
    }
    
    function getAmountPerLine(line, poLine){
      if (poLine){
        return poLine.purchasedPrice * line.quantity;  
      }
      return 0; 
    }
    
    if (!receipt.lines)
      vm.receipt.lines = [];
      
    function addLine(obj){
      if (vm.poLine.length === 0){
        vm.error = 'Select Purchase Order First';
        alertify.error(vm.error);
        return;
      }
      
      obj.push({
        poLine: '',
        quantity: 0,
        chargeAccount: '',
        serials: []
      });
    }
      
    function addSerial(obj){
      obj.push({
        quantity: 0
      });
    }
      
    function showSerials(obj, index){
      vm.currentIndex = index;
      if (!vm.receipt.lines[index].serials){
        vm.receipt.lines[index].serials.push({
          serial: '',
          quantity: 0
        });
      }
    }
    
    function statusChecking(status){
      if (status === 'Cancelled' || status === 'Completed'){
        return true;
      }
      return false;
    }
      
    function deleteLine(index, id, line){
      if (!id && (!statusChecking(line.status))){
        vm.receipt.lines.splice(index, 1);
      }
    }
      
    function deleteSerial(index, id){
      if (!id){
        vm.receipt.lines[vm.currentIndex].serials.splice(index, 1);
      }
    }
      
    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.chargeAccount.push(combination);
      });
    });
      
    // Remove existing Receipt
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.receipt.$remove($state.go('receipt.list'));
      }
    }

    // Save Receipt
    function save(isValid) {    
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.receiptForm');
        vm.error = 'Error in form values';
        alertify.error(vm.error);
        return false;
      }
      if (vm.receipt.lines.length ===0){
        vm.error = 'Purchase Order Line is required';
        alertify.error(vm.error);
        return false;
      }else{
        var itemList = [];
        for (var i = 0; i < vm.receipt.lines.length; i++) {
          var line = vm.receipt.lines[i];
          var lineNo = i + 1;
          
          var poLine = {};
          for (var j = 0; j< vm.receipt.purchaseOrder.lines.length; j++){
            var poLineObj = receipt.purchaseOrder.lines[j];
            if (poLineObj.number === line.number){
              poLine = poLineObj;
            }
          }
          
          var itemid = poLine._id;        
          if (itemList.includes(itemid)){
            vm.error = 'Duplicate Item is not allowed';
            alertify.error(vm.error);
            return false;
          }else{
            itemList.push(itemid);
          }
          
          if (line.quantity === 0){
            vm.error = 'Line ' + lineNo + ': Quantity can\'t be zero (0)';
            alertify.error(vm.error);
            return false;
          } else if (line.quantity > (poLine.ordered - (poLine.cancelled + poLine.received))){
            vm.error = 'Line ' + lineNo + ': Quantity greater than the purchase order details';
            alertify.error(vm.error);
            return false;
          } else if (!line.chargeAccount){
            vm.error = 'Line ' + lineNo + ': Charge Account can\'t be blank';
            alertify.error(vm.error);
            return false;
          } else if (line.serials.length === 0){
            vm.error = 'Line ' + lineNo + ': Serials are required';
            alertify.error(vm.error);
            return false;
          } else if (line.serials.length > 0){
            var total = 0;
            var mapSerial = {};
            for (var x = 0; x < line.serials.length; x++) {
              total += line.serials[x].quantity;
              var serial = line.serials[x];
              if(mapSerial[serial.serial + '-' + serial.subInventory] === undefined){
                mapSerial[serial.serial + '-' + serial.subInventory] = x;
              } else if(mapSerial[serial.serial + '-' + serial.subInventory] !== x) {
                vm.error = 'Line ' + lineNo + ': You have input duplicate serial and sub-inventory.';  
                alertify.error(vm.error);
                return false;  
              }
            }            
            if (total !== line.quantity){
              vm.error = 'Line ' + lineNo + ': Serials total should be same with Quantity';
              alertify.error(vm.error);
              return false;
            }

          }
        }
      }
      
      // TODO: move create/update logic to service
      if (vm.receipt._id) {
        vm.receipt.$update(successCallback, errorCallback);
      } else {
        vm.receipt.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.form.receiptForm.$dirty = false;
        alertify.success('Receipt successfully updated');
        $state.go('receipts.edit', {
          purchaseOrderId: vm.receipt.purchaseOrder._id,
          receiptId: vm.receipt._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
