(function () {
  'use strict';

  angular
    .module('receipts')
    .controller('ReceiptsListController', ReceiptsListController);

  ReceiptsListController.$inject = ['$scope', '$filter', 'ReceiptsService'];

  function ReceiptsListController($scope, $filter, ReceiptsService, alertify) {
    ReceiptsService.query(function (data) {
      $scope.object = data;
      $scope.numbers = [];
      $scope.rnumbers = [];
      $scope.createdList = [];
      $scope.suppliers = [];
      for (var i = 0; i < $scope.object.length; i++) {
        var po = $scope.object[i];
        if (po.purchaseOrder && $scope.numbers.indexOf(po.purchaseOrder.number) === -1) {
          $scope.numbers.push(po.purchaseOrder.number);
        }
        if (po.purchaseOrder.supplier && $scope.suppliers.indexOf(po.purchaseOrder.supplier.name) === -1) {
          $scope.suppliers.push(po.purchaseOrder.supplier.name);
        }
        if ($scope.rnumbers.indexOf(po.number) === -1) {
          $scope.rnumbers.push(po.number);
        }
        if (po.createdBy && $scope.createdList.indexOf(po.createdBy.username) === -1)
          $scope.createdList.push(po.createdBy.username);
      }
      $scope.buildPager();
    });
      
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.openDate = function($event, isDateFrom) {
      if(isDateFrom){
        $scope.dateFromStatus.opened = true;  
        return;
      }
        $scope.dateToStatus.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };


    $scope.dateFromStatus = {
      opened: false
    };

    $scope.dateToStatus = {
      opened: false
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.object, function(obj){
        if($scope.number && !(new RegExp($scope.number).test(obj.purchaseOrder.number))) { 
          return false;
        }
        if($scope.rnumber && !(new RegExp($scope.rnumber).test(obj.number))) { 
          return false;
        }
        if ($scope.supplier && !(new RegExp($scope.supplier).test(obj.purchaseOrder.supplier.name))) {
          return false;
        }
        if($scope.created && !(new RegExp($scope.created).test(obj.createdBy.username))) {
          return false;
        }
        if($scope.dateFrom && (Date.parse(obj.date) < Date.parse($scope.dateFrom))) {
          return false;
        } 
        if($scope.dateTo && (Date.parse(obj.date) > Date.parse($scope.dateTo))) {
          return false;
        }
        return true;
      });
  
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.number = '';
      $scope.rnumber = '';
      $scope.created = '';
      $scope.supplier = '';
      $scope.dateFrom = '';
      $scope.dateTo = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.searchNumber = function (){
      $scope.sc = [];
      $scope.sc = $filter('filter')($scope.numbers, function(number){
        if ($scope.number && !(new RegExp('^' + $scope.number).test(number))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchName = function() {
      $scope.sn = [];
      $scope.sn = $filter('filter')($scope.suppliers, function(supplier) {
        if ($scope.supplier && !(new RegExp($scope.supplier, 'i').test(supplier))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchRNumber = function (){
      $scope.sr = [];
      $scope.sr = $filter('filter')($scope.rnumbers, function(number){
        if ($scope.rnumber && !(new RegExp('^' + $scope.rnumber).test(number))) {
          return false;
        }
        return true;
      });
    };
      
    $scope.searchUser = function(){
      $scope.user = [];
      $scope.user = $filter('filter')($scope.createdList, function(created){
        if ($scope.created && !(new RegExp($scope.created, 'i').test(created))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.getTotalAmount = function(receipt){
      var total = 0;
      for (var i = 0; i < receipt.lines.length; i++) {
        var line = receipt.lines[i];
        for (var j = 0; j< receipt.purchaseOrder.lines.length; j++){
          var poLine = receipt.purchaseOrder.lines[j];
          if (poLine.number === line.number){
            total += (line.quantity * poLine.purchasedPrice);
          }
        }
      }
      return total;
    };
  }
})();

