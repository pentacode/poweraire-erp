(function (app) {
  'use strict';

  app.registerModule('receipts');
  app.registerModule('receipts.admin', ['core.admin']);
})(ApplicationConfiguration);
