'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Sequence Schema
 */
var SequenceSchema = new Schema({
  model: {
    type: String
  },
  value: {
    type: Number,
    default: 1
  }
});

mongoose.model('Sequence', SequenceSchema);