'use strict';
var mongoose = require('mongoose'),
PurchaseOrder = mongoose.model('PurchaseOrder'),
Transaction = mongoose.model('Transaction'),
Invoice = mongoose.model('Invoice'),
Onhand = mongoose.model('Onhand'),
Combination = mongoose.model('Combination'),
BatchJV = mongoose.model('JournalVoucherBatchSchema'),
JournalVoucher = mongoose.model('JournalVoucher'),
Calendar = mongoose.model('Calendar'),
moment = require('moment'),
async = require('async'),
_ = require('lodash'),
winston = require('winston');

var logger = new (winston.Logger)({
  transports: [
  new (winston.transports.File)({
    filename : 'error.log',
    json : false,
    timestamp : function() {
      return new Date().toGMTString();
    },
    formatter : function(options) {
      return '[' + options.timestamp() + '] ' + options.message;
    }
  })
  ]
});

var updatePurchaseOrder = function(receipt) {
  return new Promise(function(resolve, reject) {
    // update purchase order line of each receipt line
    // then update purchase order of receipt
    PurchaseOrder.findById(receipt.purchaseOrder).populate({
      path : 'lines',
      populate : [{
        path : 'item',
        model : 'Item',
      }, {
        path : 'uom',
        model : 'Reference'
      }]
    }).populate('supplier').exec(function(err, purchaseOrder){
      for (var i = 0; i < receipt.lines.length; i++) {
        var receiptLine = receipt.lines[i];
        for (var j = 0; j < purchaseOrder.lines.length; j++) {
          var poLine = purchaseOrder.lines[j];
          if (receiptLine.number === poLine.number) {
            poLine.received += receiptLine.quantity;
            break;
          }
        }
      }
      if (purchaseOrder.status === 'Open') {
        purchaseOrder.status = 'Processing';
      }
      purchaseOrder.save(function(err){
        if (err) {
          reject(err);
        } else {
          receipt.purchaseOrder = purchaseOrder;
          resolve(receipt);
        }
      });
    });
  });
};

exports.updateJournalVouchers = function(value, callback){
  if (!value || value.length === 0) {
    return callback();
  }
  var journalVoucherList = value.journalVoucherList,
  userId = value.userId;
  var bulkOp = JournalVoucher.collection.initializeUnorderedBulkOp();
  journalVoucherList.forEach(function (obj) {
    var _id = obj._id;
    delete obj._id;

    var ObjectId = mongoose.Types.ObjectId;
    var query = { _id: new ObjectId(_id) };
    var updateFields = {};
    var newLines = fixVoucherLines(obj.lines);

    updateFields.lines = newLines;
    updateFields.updated = Date;
    updateFields.reverseJv = obj.reverseJv;
    updateFields.status = obj.status;
    updateFields.updatedBy = new ObjectId(userId);

    bulkOp.find(query).upsert().update({ $set: updateFields });
  });
  bulkOp.execute(function(err, success){
    if(!err){
      return callback(true);
    }else{
      return callback();
    }
  })
};

exports.batchPostJournalVouchers = function(value, callback){
  var fromDate = value.fromDate;
  var toDate = value.toDate;
  var query = { status : false , fromDate : { $lte : fromDate }, toDate : { $gte :toDate } };
  var bulkOp = JournalVoucher.collection.initializeOrderedBulkOp();
  bulkOp.find(query).update({ $set: { status: true } });
  bulkOp.execute(function(err, success){
    if(!err){
      return callback(true);
    }else{
      return callback();
    }
  })
}

var fixVoucherLines = function(lines){
  var ObjectId = mongoose.Types.ObjectId;
  _.forEach(lines, function(line, index){
    line._id = new ObjectId(line._id);
    line.accountCode = new ObjectId(line.accountCode);
  })
  return lines;
}

var recordTransaction = function(receipt) {
  return new Promise(function(resolve, reject) {
    var transaction = new Transaction();
    transaction.user = receipt.createdBy;
    transaction.date = receipt.date;
    transaction.source = 'PO Receipt';
    transaction.action = 'Receipt';
    transaction.receipt = receipt;
    transaction.lines = [];
    for (var i = 0; i < receipt.lines.length; i++) {
      var receiptLine = receipt.lines[i];
      var transactionSerials = [];
      for (var j = 0; j < receiptLine.serials.length; j++) {
        var receiptSerial = receiptLine.serials[j];
        var transactionSerial = {
          serial : receiptSerial.serial,
          subInventory : receiptSerial.subInventory,
          quantity : receiptSerial.quantity
        };
        transactionSerials.push(transactionSerial);
      }
      var poLine = null;
      for (var k = 0; k < receipt.purchaseOrder.lines.length; k++) {
        poLine = receipt.purchaseOrder.lines[k];
        if (poLine.number === receiptLine.number) {
          break;
        }
      }
      var transactionLine = {
        item : poLine.item,
        quantity : receiptLine.quantity,
        unitPrice : poLine.purchasedPrice,
        chargeAccount : receiptLine.chargeAccount,
        serials : transactionSerials,
        debit : poLine.item.accountCode,
        credit : receiptLine.chargeAccount
      };
      transaction.lines.push(transactionLine);
    }
    transaction.save(function(err) {
      if (err) {
        reject(err);
      } else {
        resolve(transaction);
      }
    });
  });
};

var recordBatchJournalVoucher = function(batchObject) {
  return new Promise(function(resolve, reject) {
    var batchJournalVouchers = new BatchJV();
    var batchRecords = batchObject[0];
    var allRecords = [];
    allRecords.push(batchObject[1]);
    batchJournalVouchers.source = 'Inventory';
    batchJournalVouchers.batchName = 'INV';
    batchJournalVouchers.period = batchRecords._id;
    batchJournalVouchers.batchDate = new Date();
    batchJournalVouchers.reverseJv = false;
    batchJournalVouchers.totalDebit = 0;
    batchJournalVouchers.totalCredit = 0;
    batchJournalVouchers.save(function(err) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        allRecords.push(batchJournalVouchers);
        resolve(allRecords);
      }
    });
  });
}

var recordJournalVoucher_Inventory = function(recordedTransactions) {
  console.log(recordedTransactions);
  var transactions = recordedTransactions[0];
  var batchRecord = recordedTransactions[1];
  return new Promise(function(resolve, reject) {
    async.each(transactions, function(transaction, next){
      var journalVoucher = new JournalVoucher();
      var transactionName = '';
      journalVoucher.user = transaction.createdBy;
      journalVoucher.date = new Date(transaction.date);
      journalVoucher.transaction = transaction.action;
      journalVoucher.source = 'Inventory';
      journalVoucher.manual = false;
      journalVoucher.status = true;
      journalVoucher.reverseJv = false;
      journalVoucher.period = batchRecord.period;
      journalVoucher.batchId = batchRecord._id;
      journalVoucher.glDate = new Date();
      if (journalVoucher.transaction === 'Receipt'){
        transactionName = 'REC';
      } else {
        transactionName = 'ISSUE';
      }
      journalVoucher.name = 'INV_' + transactionName + '_' + transaction.number + '_' + moment(journalVoucher.date).format("DD-MMM-YYYY") + '_';
      journalVoucher.description = 'Post to GL for module: Inventory - ' + journalVoucher.transaction + ' for ' + moment(journalVoucher.date).format("DD-MMM-YYYY");
      journalVoucher.lines = [];

      if(transaction.receipt !== undefined){
        journalVoucher.name = journalVoucher.name + transaction.receipt.purchaseOrder.supplier.name;
      } else {
        journalVoucher.name = journalVoucher.name + 'N/A';
      }
      for (var i = 0; i < transaction.lines.length; i++) {
        var transactionLine = transaction.lines[i];
        var lineIndex = 0;
        var debitLineProcessed = false;
        var creditLineProcessed = false;

        var amount = transactionLine.quantity * transactionLine.unitPrice;
        for (var k = 0; k < journalVoucher.lines.length; k++){
          var journalVoucherLine = journalVoucher.lines[k];

          if (journalVoucherLine.accountCode.equals(transactionLine.debit)) {
            debitLineProcessed = true;
            journalVoucherLine.debit += amount;
          }

          if(journalVoucherLine.accountCode.equals(transactionLine.credit)) {
            creditLineProcessed = true;
            journalVoucherLine.credit += amount;
          }

          if (debitLineProcessed && creditLineProcessed){
            break;
          }
        }

        if (!debitLineProcessed) {
          var journalVoucherLine = {
            number : lineIndex,
            description : transactionLine.debit.company.descriptionOne + '-' + transactionLine.debit.location.descriptionOne + '-' + transactionLine.debit.serviceType.descriptionOne,
            accountCode : transactionLine.debit,
            debit : amount,
            credit : 0
          };
          journalVoucher.lines.push(journalVoucherLine);
        }

        if (!creditLineProcessed) {
          var journalVoucherLine = {
            number : lineIndex+1,
            description : transactionLine.credit.company.descriptionOne + '-' + transactionLine.credit.location.descriptionOne + '-' + transactionLine.credit.serviceType.descriptionOne,
            accountCode : transactionLine.credit,
            credit : amount,
            debit : 0
          };
          journalVoucher.lines.push(journalVoucherLine);
        }

        lineIndex += 1;
      }
      journalVoucher.save(function(err) {
        if (err) {
          next(err);
        } else {
          next();
        }
      });
    }, function(err){
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
};

var recordJournalVoucher_Payables = function(recordPayables) {
  var payables = recordPayables[0];
  console.log(payables);
  return new Promise(function(resolve, reject) {
    async.each(payables, function(payable, next){
      var journalVoucher = new JournalVoucher();
      journalVoucher.user = payable.createdBy;
      journalVoucher.date = new Date(payable.date);
      journalVoucher.source = 'Accounts Payable';
      journalVoucher.manual = false;
      journalVoucher.status = true;
      journalVoucher.reverseJv = false;
      journalVoucher.glDate = new Date();
      journalVoucher.name = 'AP_' + payable.status + payable.number + '_' + moment(journalVoucher.date).format("DD-MMM-YYYY") + '_';
      journalVoucher.description = 'Post to GL for module: Accounts Payable - ' + ' for ' + moment(journalVoucher.date).format("DD-MMM-YYYY");
      journalVoucher.lines = [];

      if(purchOrder.receipt !== undefined){
        journalVoucher.name = journalVoucher.name + payable.supplier.name;
      } else {
        journalVoucher.name = journalVoucher.name + 'N/A';
      }
      console.log('======================JOURNAL VOUCHER======================');
      console.log(journalVoucher);
      for (var i = 0; i < payable.lines.length; i++) {
        var payableLine = payable.lines[i];
        var lineIndex = 0;
        var itemLineProcessed = false;
        var supplierLineProcessed = false;
        var wtaxLineProcessed = false;
        var vatLineProcessed = false;

        for (var k = 0; k < journalVoucher.lines.length; k++){
          var journalVoucherLine = journalVoucher.lines[k];

          if (journalVoucherLine.accountCode.equals(payableLine.item.accountCode)) {
            itemLineProcessed = true;
            journalVoucherLine.debit += amount;
          }

          if (journalVoucherLine.accountCode.equals(payableLine.withholdingTax.accountCode)) {
            wtaxLineProcessed = true;
            journalVoucherLine.debit += amount;
          }

          if(journalVoucherLine.accountCode.equals(payableLine.credit)) {
            creditLineProcessed = true;
            journalVoucherLine.credit += amount;
          }

          if (debitLineProcessed && creditLineProcessed){
            break;
          }
        }

        if (!debitLineProcessed) {
          var journalVoucherLine = {
            number : lineIndex,
            description : payableLine.debit.company.descriptionOne + '-' + payableLine.debit.location.descriptionOne + '-' + payableLine.debit.serviceType.descriptionOne,
            accountCode : payableLine.debit,
            debit : amount,
            credit : 0
          };
          journalVoucher.lines.push(journalVoucherLine);
        }

        if (!creditLineProcessed) {
          var journalVoucherLine = {
            number : lineIndex+1,
            description : payableLine.credit.company.descriptionOne + '-' + payableLine.credit.location.descriptionOne + '-' + payableLine.credit.serviceType.descriptionOne,
            accountCode : payableLine.credit,
            credit : amount,
            debit : 0
          };
          journalVoucher.lines.push(journalVoucherLine);
        }

        lineIndex += 1;
      }
    journalVoucher.save(function(err) {
      if (err) {
        next(err);
      } else {
        next();
      }
    });
  }, function(err){
    if (err) {
      reject(err);
    } else {
      resolve(true);
    }
  });
  });
};

var updateOnhand = function(transaction) {
  return new Promise(function(resolve, reject) {
    var action = transaction.action;
    async.each(transaction.lines, function(line, next){
      Onhand.findOne({ 'item' : line.item }).exec(function(err, onhand){
        if (err) {
          next(err);
        } else {
          var transactionDtl = {
            transactionNo : transaction.number,
            source : transaction.source,
            date : transaction.date,
            action : transaction.action,
            quantity : line.quantity
          };
          if(transaction.receipt){
            transactionDtl.purchaseNo = transaction.receipt.purchaseOrder.number;
          }
          onhand.transactions.push(transactionDtl);
          if(action === 'Receipt'){
            if (onhand.quantity && onhand.quantity > 0){
              onhand.unitPrice = ((onhand.quantity * onhand.unitPrice) + (line.quantity * line.unitPrice)) / (onhand.quantity + line.quantity);
              onhand.quantity += line.quantity;
            } else {
              onhand.unitPrice = line.unitPrice;
              onhand.quantity = line.quantity;
            }
          }else if(action === 'Issuance'){
            onhand.quantity -= line.quantity;
          }
          for (var i = 0; i < line.serials.length; i++) {
            var processed = false;
            var toMove = false;
            var transactionSerial = line.serials[i].serial;
            var transactionSubInv = line.serials[i].subInventory;
            var onhandSerial, onhandSubinv;

            // get the onhand record that matches the serial & sub-inventory of transaction line
            for (var j = 0; j < onhand.serials.length; j++) {
              onhandSerial = onhand.serials[j].serial;
              onhandSubinv = onhand.serials[j].subInventory;
              if (onhandSerial === transactionSerial && onhandSubinv.equals(transactionSubInv)) {
                if (action === 'Issuance') {
                  processed = true;
                  onhand.serials[j].quantity = onhand.serials[j].quantity - line.serials[i].quantity;
                } else if (action === 'Receipt') {
                  processed = true;
                  onhand.serials[j].quantity = onhand.serials[j].quantity + line.serials[i].quantity;
                } else if (action === 'Transfer') {
                  toMove = true;
                  onhand.serials[j].quantity = onhand.serials[j].quantity - line.serials[i].quantity;
                }
                break;
              }
            }
            var transferSubInventory = line.serials[i].transferSubInventory;
            if (toMove) {
              // get the onhand record that matches the serial & transfer sub-inventory of transaction line
              for (var k = 0; k < onhand.serials.length; k++) {
                onhandSerial = onhand.serials[k].serial;
                onhandSubinv = onhand.serials[k].subInventory;
                if (onhandSerial === transactionSerial && onhandSubinv.equals(transferSubInventory)) {
                  processed = true;
                  onhand.serials[k].quantity = onhand.serials[k].quantity + line.serials[i].quantity;
                  break;
                }
              }
            }
            if (!processed) {
              var serial = {
                serial : line.serials[i].serial,
                quantity : line.serials[i].quantity,
                subInventory : (toMove) ? line.serials[i].transferSubInventory : line.serials[i].subInventory
              };
              onhand.serials.push(serial);
            }
          }
          onhand.save(function(err){
            if (err) {
              next(err);
            } else {
              next();
            }
          });
        }
      });
    }, function(err){
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

exports.handleReceipt = function(receipt, callback) {
  var purchaseOrderUpdated = false;
  var transactionRecorded = false;
  var originalPo = null;
  var transaction = null;
  new Promise(function(resolve, reject) {
    PurchaseOrder.findById(receipt.purchaseOrder).exec(function(err, purchaseOrder){
      originalPo = purchaseOrder;
    });
    resolve(receipt);
  })
  .then(function(receipt) {
    return updatePurchaseOrder(receipt);
  })
  .then(function(receipt) {
    purchaseOrderUpdated = true;
    return recordTransaction(receipt);
  })
  .then(function(txn) {
    transactionRecorded = true;
    transaction = txn;
    return updateOnhand(txn);
  })
  .then(function() {
    callback(true);
  })
  .catch(function(err) {
    if (purchaseOrderUpdated) {
      PurchaseOrder.findById(receipt.purchaseOrder).exec(function(err, purchaseOrder){
        purchaseOrder = _.extend(purchaseOrder, originalPo);
        purchaseOrder.save();
      });
    }
    if (transactionRecorded) {
      transaction.remove();
    }
    logger.error('Receipt: ' + receipt + ' - ' + err);
    callback(false);
  });
};

exports.handleTransaction = function(transaction, callback) {
  new Promise(function(resolve, reject) {
    resolve(transaction);
  })
  .then(function(transaction) {
    return updateOnhand(transaction);
  })
  .then(function(){
    callback(true);
  })
  .catch(function(err) {
    logger.error('Transaction: ' + transaction + ' - ' + err);
    callback(false);
  });
};

exports.postToGL = function(query, callback){
  if(query.module == 'Inventory'){
    postInventory(query, callback)
  } else if (query.module == 'Accounts Payable'){
    postAccountsPayable(query, callback)
  }
};

var postInventory = function(query, callback) {
  var startDate = new Date(query.period.fromDate);
  var endDate = new Date(query.period.toDate);
  new Promise(function(resolve, reject) {
    Transaction.find({
      date : {
        '$gte' : startDate,
        '$lte' : endDate
      },
      action : {
        $in : ['Receipt', 'Issuance']
      }
    }).populate({
      path : 'receipt',
      model : 'Receipt',
      populate : {
        path : 'purchaseOrder',
        model : 'PurchaseOrder',
        populate : {
          path : 'supplier',
          model : 'Supplier'
        }
      }
    }).populate({
      path : 'lines.debit',
      model : 'Combination',
      populate : {
        path : 'company location serviceType',
        model : 'Reference'
      }
    }).populate({
      path : 'lines.credit',
      model : 'Combination',
      populate : {
        path : 'company location serviceType',
        model : 'Reference'
      }
    }).exec(function(err, transactions){
      if (err) return reject(err);
      var returnValues = [query.period];
      returnValues.push(transactions);
      resolve(returnValues);
    });
  }).then(function(records){
    return recordBatchJournalVoucher(records);
  }).then(function(object){
    return recordJournalVoucher_Inventory(object);
  }).then(function(){
    callback(true);
  }).catch(function(err){
    logger.error('General Ledger: ' + query.module + '(' + query.startDate + ', ' + query.endDate + ') - ' + err);
    callback(false);
  });
};

var postAccountsPayable = function(query, callback) {
  var startDate = new Date(query.period.fromDate);
  var endDate = new Date(query.period.toDate);
  new Promise(function(resolve, reject) {
    PurchaseOrder.find({
      date : {
        '$gte' : startDate,
        '$lte' : endDate
      }
    }).populate({
      path : 'paymentTerms',
      model : 'Reference'
    }).populate({
      path : 'purchaseOrder',
      model : 'PurchaseOrder'
    }).populate({
      path : 'referenceInvoice',
      model : 'Invoice'
    }).populate({
      path : 'currency',
      model : 'Reference'
    }).populate({
      path : 'supplier',
      model : 'Supplier'
    }).populate({
      path : 'lines.item',
      model : 'Item'
    }).populate({
      path : 'lines.uom',
      model : 'Reference'
    }).populate({
      path : 'lines.withholdingTax',
      model : 'Tax'
    }).populate({
      path : 'lines.vat',
      model : 'Tax'
    }).populate({
      path : 'lines.chargeAccount',
      model : 'Combination'
    }).exec(function(err, payables){
      if (err) return reject(err);
      var returnValues = [query.period];
      returnValues.push(payables);
      resolve(returnValues);
    });
  }).then(function(object){
    return recordJournalVoucher_Payables(object);
  }).then(function(){
    callback(true);
  }).catch(function(err){
    logger.error('General Ledger: ' + query.module + '(' + query.startDate + ', ' + query.endDate + ') - ' + err);
    callback(false);
  });
}

exports.batchPostJV = function(query, callback) {
  async.autoInject({
    journalVouchers: function(next) {
      JournalVoucher.find(query).exec(next);
    },
    batches: function(journalVouchers, next) {
      var jvIds = [];
      _.each(journalVouchers, function(jvRecord) {
        if (jvRecord.status === false) {
          jvIds.push(jvRecord._id);
        }
      });
      JournalVoucher.update({ _id: { $in: jvIds } }, { status: true }, { multi: true }, next);
    }
  }, function(err, result) {
    if (err) {
      return callback(err);
    }
    var batches = _.get(result, 'batches') || [];
    callback(null, batches);
  });
}

var createNegativeLines = function(lines){
  _.forEach(lines, function(line){
    line.debit = line.debit == 0? 0 : -(line.debit);
    line.credit = line.credit == 0? 0 : -(line.credit);
  });
  return lines;
}

var generateNewBatch = function(batch){
  var newBatch = new BatchJV();
  newBatch.batchName = batch.batchName + '-Reversed';
  newBatch.totalCredit = 0;
  newBatch.totalDebit = 0;
  newBatch.reverseJv = true;
  newBatch.period = batch.period;
  newBatch.batchDate = batch.batchDate;
  newBatch.source = batch.source;
  return newBatch;
}

var generateNewJv = function(journalVoucher){
  var newJv = new JournalVoucher();
  newJv.description = journalVoucher.description;
  newJv.name = journalVoucher.name;
  newJv.status = journalVoucher.status;
  newJv.reverseJv = journalVoucher.reverseJv;
  newJv.source = journalVoucher.source;
  newJv.period = journalVoucher.period;
  newJv.glDate = journalVoucher.glDate;
  newJv.manual = journalVoucher.manual;
  newJv.transaction = journalVoucher.transaction;
  return newJv;
}

exports.reverseBatch = function(batchId, callback) {
  async.autoInject({
    batch: function(next){
      BatchJV.findById(batchId).exec(function(err, batch){
        if (err) return callback(err);
        if(batch){
          BatchJV.update({ _id: batchId }, { reverseJv: true }, { multi:false }, function(err, result){
            if(err){
              return callback(err);
            }else{
              var newBatch = generateNewBatch(batch);
              newBatch.save(next);
            }
          });
        }
      });
    },
    journalVouchers: function(batch, next) {
      var query = { batchId : batchId };
      JournalVoucher.find(query).exec(function(err, journalVouchers){
        if(err) return callback(err);

        async.each(journalVouchers, function(journalVoucher, callback){
          var newJv = generateNewJv(journalVoucher);
          var negativeLines = createNegativeLines(journalVoucher.lines);
          newJv.batchId = batch[0]._id;
          newJv.lines = negativeLines;
          newJv.save(function(err){
            if(err){
              callback(err);
            }else{
              callback();
            }
          })
        }, next);
      });
    }
  }, function(err, result) {
    if (err) {
      return callback(err);
    }
    console.log(result);
    var batch = _.get(result, 'batch') || [];
    callback(null, batch);
  });
}
