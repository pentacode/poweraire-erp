(function(){
	'use strict';
	angular
    .module('core')
     .directive('typeaheadClickOpen', function($parse, $timeout) {
	       return {
		        require: 'ngModel',
		        link: function (scope, element, attr, ngModel) {

		          const ARROW_KEYS = [37,38,39,40];

		          function manipulateViewValue(e) {

		            if( ARROW_KEYS.indexOf(e.keyCode) >= 0 )
		              return;

		            var viewValue = ngModel.$viewValue;

		            if (ngModel.$viewValue == ' ') {
		              ngModel.$setViewValue(null);
		            }

		            ngModel.$setViewValue(' ');

		            ngModel.$setViewValue(viewValue || ' ');
		          }

		          element.bind('click keyup', manipulateViewValue);

		          scope.$emptyOrMatch = function (actual, expected) {
		            if (expected == ' ') {
		              return true;
		            }
		            return actual ? actual.toString().toLowerCase().indexOf(expected.toLowerCase()) > -1 : false;
		          };
		        }
		};
  })


})()