(function () {
  'use strict';

  angular
    .module('core')
    .directive('statusOption', statusOption);

  statusOption.$inject = ['$compile'];
  
  function statusOption($compile) {
      return {
        priority: 1001, 
        terminal: true, 
        compile: function (element, attrs) {
            element.attr("ng-options", "status.key as status.value for status in statuses");
            element.removeAttr('status-option'); 
            var fn = $compile(element);
            return function (scope) {
                scope.statuses = [{ key : true, value : 'Active'}, {key : false, value : 'Inactive'}];
                fn(scope);
            };
        }
    };
  }
})();
