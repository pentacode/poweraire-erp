'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication',
  function ($scope, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    
    $scope.slides = [
       {
        image: '/img/0.jpg',
        text: 'test'
      },{
        image: '/img/1.jpg',
        text: 'test'
      },{
        image: '/img/2.jpg',
        text: 'test'
      },{
        image: '/img/3.jpg',
        text: 'test'
      },{
        image: '/img/4.jpg',
        text: 'test'
      },{
        image: '/img/5.jpg',
        text: 'test'
      }
    ];
  }
]);
