(function () {
  'use strict';

  angular
    .module('core')
    .factory('_', Lodash);

  Lodash.$inject = ['$window'];

  function Lodash($window) {
  
    return $window._;
    
  }
})();
