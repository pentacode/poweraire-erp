(function () {
  'use strict';

  angular
    .module('core')
    .factory('Util', Util);

  Util.$inject = ['$filter', '$location', '_'];

  function Util($filter, $location, _) {
    return {
      formatDateTime : formatDateTime,
      confirmationBox: confirmationBox 
    };

    function formatDateTime(date){
      return $filter('date')(date, 'medium');
    }

    function confirmationBox(title, text, callbackFn, id){
        var modalId = id ? id : 'modalWindow';
        var titleText  = (title) ? 
                      '<div class="modal-header">' +
                          '<h4 class="modal-title">' + title +'</h4>' +
                      '</div>' : '';
        var messageBox = ''+
                '<div class="modal fade" id="'+ modalId+'" tabindex="-1" role="dialog">' +
                  '<div class="modal-dialog" role="document">' +
                    '<div class="modal-content">' +
                      titleText +
                      '<div class="modal-body">'+
                         text +
                      '</div>' +
                      '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>' +
                        '<button type="button" id="confirmDialog'+modalId+'" class="btn btn-primary" data-dismiss="modal">Okay</button>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>';
        if( !_.isEmpty($('#'+modalId +'')) ){
          $('#'+modalId +'').remove();
        }    
        $('body').append(messageBox);                
        $('#confirmDialog'+modalId +'').on("click",callbackFn);
        $('#'+modalId +'').modal('show');
    }

    
  }
})();
