'use strict';

angular.module('core.admin').run(['Menus',
  function (Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Inventory',
      state: 'inventory',
      type: 'dropdown',
      roles: ['On-hand Quantity', 'Transactions']
    });

    Menus.addMenuItem('topbar', {
      title: 'Purchasing',
      state: 'purchasing',
      type: 'dropdown',
      roles: ['Purchase Orders', 'Receipts']
    });

    // Menus.addMenuItem('topbar', {
    //   title: 'Accounts Payable',
    //   state: 'accountsPayable',
    //   type: 'dropdown',
    //   roles: ['Invoices', 'Payments']
    // });

    // Menus.addMenuItem('topbar', {
    //   title: 'Accounts Receivable',
    //   state: 'accountsReceivable',
    //   // type: 'dropdown',
    //   roles: ['Accounts Receivable']
    // });

    // Menus.addMenuItem('topbar', {
    //   title: 'General Ledger',
    //   state: 'generalLedger',
    //   type: 'dropdown',
    //   roles: ['Journal Vouchers', 'General Ledger']
    // });

    Menus.addMenuItem('topbar', {
      title: 'Maintenance',
      state: 'maintenance',
      type: 'dropdown',
      roles: ['Bank Accounts', 'Calendar', 'Checks', 'Combinations', 'Company Profile', 'Customers', 'Items', 'References', 'Suppliers', 'Taxes', 'Users']
    });
  }
]);