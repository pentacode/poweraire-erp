(function () {
  'use strict';

  angular
    .module('general-ledger')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('general-ledger', {
        abstract: true,
        url: '/general-ledger',
        template: '<ui-view/>'
      })
      .state('general-ledger.inquiry', {
        url: '/inquiry',
        templateUrl: 'modules/general-ledger/client/views/inquiry-general-ledger.client.view.html',
        controller: 'GeneralLedgerInquiryController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'General Ledger Inquiry'
        }
      })
      .state('general-ledger.summary', {
        url: '/summary?accountCode&periodName',
        templateUrl: 'modules/general-ledger/client/views/summary-general-ledger.client.view.html',
        controller: 'GeneralLedgerSummaryController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'General Ledger Summary'
        }
      });
  }

}());
