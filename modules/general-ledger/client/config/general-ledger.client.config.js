(function () {
  'use strict';

  angular
    .module('general-ledger')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(menuService) {
    // Set top bar menu items
    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'generalLedger', {
      title: 'Inquiry',
      state: 'general-ledger.inquiry',
      roles: ['General Ledger']
    });
  }
}());
