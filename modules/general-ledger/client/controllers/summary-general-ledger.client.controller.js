(function () {
  'use strict';

  angular
    .module('general-ledger')
    .controller('GeneralLedgerSummaryController', GeneralLedgerSummaryController);

  GeneralLedgerSummaryController.$inject = ['$scope', '$state', 'Authentication', '$filter', 'alertify', '$http', 'CombinationsService'];

  function GeneralLedgerSummaryController($scope, $state, Authentication, $filter, alertify, $http, CombinationsService) {
    var vm = this;

    
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.generalLedgers = {};
    vm.accountCode = null;
    vm.period = null;

    init();

    function init() {
        $http.get('/api/general-ledger/summary?accountCode=' + $state.params.accountCode + '&periodName=' + $state.params.periodName)
          .then(function(data) {
            if (data.status === 200 && data.data.length > 0) {
              vm.generalLedgers = data.data;
              CombinationsService.query({ status : true }, function(combinations){
                combinations.forEach(function(combination){
                  if(combination._id == $state.params.accountCode) {
                    vm.accountCode = combination.accountDescription;
                  }
                });
              });
              vm.period = $state.params.periodName;
            } else {
              // error
               alertify.error('No data retrieved');
            }
          })
          .catch(function(err) {
            alertify.error('Error in connecting to General Ledger Inquiry');
          });
      };

    // Remove existing Generalledger
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.generalLedgers.$remove($state.go('general-ledgers.inquiry'));
      }
    }

    // Save Generalledger
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.generalledgerForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.generalLedgers._id) {
        vm.generalLedgers.$update(successCallback, errorCallback);
      } else {
        vm.generalLedgers.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('general-ledgers.summary', {
          generalledgerId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
