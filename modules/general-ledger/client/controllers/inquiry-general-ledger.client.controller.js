(function () {
  'use strict';

  // General ledgers controller
  angular
    .module('general-ledger')
    .controller('GeneralLedgerInquiryController', GeneralLedgerInquiryController);

  GeneralLedgerInquiryController.$inject = ['Authentication', '$scope', '$filter', 'alertify', '$http', 'CombinationsService', '$state', '$window'];

    function GeneralLedgerInquiryController(Authentication, $scope, $filter, alertify, $http, CombinationsService, $state, $window) {
      var vm = this;

      vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
      vm.searchRec = searchRec;
      vm.getTotalAmount = getTotalAmount;
      vm.viewSummary = viewSummary;
      vm.accountCodes = [];
      vm.generalLedgers = {};

      // vm.generalLedgers = [{_id: 123, period: 'February 22, 2018', balance: 300}];
      CombinationsService.query({ status : true }, function(combinations){
        combinations.forEach(function(combination){
          vm.accountCodes.push(combination);
        });
      });

      function searchRec() {
        $http.get('/api/general-ledger/inquiry?accountCode=' + vm.glAccountCode._id)
          .then(function(data) {
            if (data.status === 200 && data.data.length > 0) {
              vm.generalLedgers = data.data;
              buildPager();
            } else {
              // error
               alertify.error('No data retrieved');
            }
          })
          .catch(function(err) {
            alertify.error('Error in connecting to General Ledger Inquiry');
          });
      };

       function buildPager() {
        vm.pagedItems = [];
        vm.itemsPerPage = 10;
        vm.currentPage = 1;
        vm.figureOutItemsToDisplay();
      };

      function figureOutItemsToDisplay() {
        vm.filteredItems = $filter('filter')(vm.generalLedgers, function(obj) {
          if (vm.glAccountCode && !(new RegExp(vm.glAccountCode).test(obj.accountCode))) {
            return false;
          }
          return true;
        });

        vm.filterLength = vm.filteredItems.length;
        var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
        var end = begin + vm.itemsPerPage;
        vm.pagedItems = vm.filteredItems.slice(begin, end);
      };

      function getTotalAmount(record) {
        if(record){
          return record.balance;
        }
      }

      function viewSummary(account, ledger) {
        var url = $state.href('general-ledger.summary', {periodName: ledger.period, accountCode: account._id});
        $window.open(url,'_blank');
      }
  }
}());
