'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Calendar = mongoose.model('Calendar'),
  JournalVoucher = mongoose.model('JournalVoucher'),
  User = mongoose.model('User'),
  async = require('async'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * General Ledger Inquiry
 */
exports.inquiry = function(req, res) {
  var accountCode = req.param('accountCode');

  async.autoInject({
    calendars: function(next) {
      Calendar.find().sort('fromDate').exec(next);
    },
    journalVouchers: function(next) {
      JournalVoucher.find({ 'lines.accountCode': accountCode }).exec(next);
    },
    payload: function(calendars, journalVouchers, next) {
      var payload = [];
      _.each(calendars, function(calendar) {
        if (calendar.status === 'Future') {
          payload.push({
            period: calendar.periodName,
            balance: 0
          });
          return false;
        }
        var balance = 0;
        _.each(journalVouchers, function (jv) {
          if (jv.period == calendar.id) {
            var line = _.find(jv.lines, function(ln) {
              return ln.accountCode == accountCode;
            });
            if (line) {
              balance += line.credit;
              balance += line.debit;
            }
          }
        });
        var record = _.find(payload, { period: calendar.periodName });
        if (record) {
          record.balance += balance;
        } else {
          payload.push({
            period: calendar.periodName,
            balance: balance
          });
        }
      });
      next(null, payload);
    }
  }, function(err, result) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    var payload = _.get(result, 'payload') || [];
    res.jsonp(payload);
  });
};

/**
 * General Ledger Summary
 */
exports.summary = function(req, res) {
  var accountCode = req.param('accountCode');
  var period = req.param('periodName');
  async.autoInject({
    calendars: function(next) {
      Calendar.find({ periodName: period }).exec(next);
    },
    journalVouchers: function(calendars, next) {
      JournalVoucher.find({ period: { $in: _.map(calendars, '_id') }, 'lines.accountCode': accountCode }).exec(next);
    },
    userCreate: function(journalVouchers, next) {
      User.find({ _id: { $in: _.map(journalVouchers, 'createdBy') }}).exec(next);
    },
    userUpdate: function(journalVouchers, next) {
      User.find({ _id: { $in: _.map(journalVouchers, 'updatedBy') }}).exec(next);
    },
    payload: function(journalVouchers, userCreate, userUpdate, next) {
      var payload = [];
      _.each(journalVouchers, function(jv, index) {
        var line = _.find(jv.lines, function(ln) {
          return ln.accountCode == accountCode;
        }) || {};
        payload.push({
          source: jv.source,
          name: jv.name,
          number: jv.number,
          glDate: jv.glDate,
          debit: line.debit,
          credit: line.credit,
          reverseJv: jv.reverseJv,
          createdBy: userCreate[0].username,
          updatedBy: userUpdate[0].username
        });
      });
      next (null, payload);
    }
  }, function(err, result) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    var payload = _.get(result, 'payload') || [];
    res.jsonp(payload);
  });
};