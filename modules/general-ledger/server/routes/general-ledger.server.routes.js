'use strict';

/**
 * Module dependencies
 */
var policy = require('../policies/general-ledger.server.policy'),
  controller = require('../controllers/general-ledger.server.controller');

module.exports = function(app) {
  app.route('/api/general-ledger/inquiry').all(policy.isAllowed)
    .get(controller.inquiry);

  app.route('/api/general-ledger/summary').all(policy.isAllowed)
    .get(controller.summary);
};
