'use strict';

/**
 * Module dependencies
 */
var banksPolicy = require('../policies/banks.server.policy'),
  banks = require('../controllers/banks.server.controller');

module.exports = function(app) {
  // Banks Routes
  app.route('/api/banks').all(banksPolicy.isAllowed)
    .get(banks.list)
    .post(banks.create);

  app.route('/api/banks/:bankId').all(banksPolicy.isAllowed)
    .get(banks.read)
    .put(banks.update);

  // Finish by binding the Bank middleware
  app.param('bankId', banks.bankByID);
};
