'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Bank = mongoose.model('Bank'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Bank
 */
exports.create = function(req, res) {
  var bank = new Bank(req.body);
  bank.user = req.user;

  bank.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(bank);
    }
  });
};

/**
 * Show the current Bank
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var bank = req.bank ? req.bank.toJSON() : {};

  res.jsonp(bank);
};

/**
 * Update a Bank
 */
exports.update = function(req, res) {
  var bank = req.bank;
  bank.user = req.user;

  bank = _.extend(bank , req.body);

  bank.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(bank);
    }
  });
};

/**
 * List of Banks
 */
exports.list = function(req, res) { 
  var query = {};
  if (req.query.status) {
    query.status = req.query.status;
  }
  if (req.query.code) {
    query.code = new RegExp('^' + req.query.code, 'i');
  }
  if (req.query.name) {
    query.name = new RegExp(req.query.name, 'i');
  }
  if (req.query.accountNumber) {
    query.accountNumber = new RegExp(req.query.accountNumber);
  } 
  Bank.find(query)
  .sort('code')
  .populate({
    path : 'accountCode',
    model : 'Combination',
    populate : {
      path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
      select : 'descriptionOne',
      model : 'Reference'           
    }
  })
  .populate('currency')
  .exec(function(err, banks) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(banks);
    }
  });
};

/**
 * Bank middleware
 */
exports.bankByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Bank is invalid'
    });
  }

  Bank.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, bank) {
    if (err) {
      return next(err);
    } else if (!bank) {
      return res.status(404).send({
        message: 'No Bank with that identifier has been found'
      });
    }
    req.bank = bank;
    next();
  });
};
