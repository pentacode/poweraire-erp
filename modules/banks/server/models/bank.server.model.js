'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Bank Schema
 */
var BankSchema = new Schema({
  code: {
    type: String,
    required: 'Bank code is required',
    unique: true,
    uppercase: true
  },
  name: {
    type: String,
    required: 'Bank name is required'
  },
  accountNumber: {
    type: String,
    required: 'Account number is required'
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  addressOne : String,
  addressTwo : String,
  branch : String,
  country : {
    type : Schema.ObjectId,
    ref : 'Reference'
  },
  currency : {
    type : Schema.ObjectId,
    ref : 'Reference'
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

BankSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate;
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  if (this.status) {
    this.disabled = null;
  } else if (!this.disabled) {
    this.disabled = currentDate;
  }
  next();
});

mongoose.model('Bank', BankSchema);
