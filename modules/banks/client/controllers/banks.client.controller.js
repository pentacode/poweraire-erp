(function () {
  'use strict';

  // Banks controller
  angular
    .module('banks')
    .controller('BanksController', BanksController);

  BanksController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'ReferencesService', 'CombinationsService', 'bankResolve', 'alertify', 'Util', '$modal'];

  function BanksController ($scope, $filter, $state, Authentication, ReferencesService, CombinationsService, bank, alertify, Util, $modal) {
    var vm = this;
    vm.formatDateTime = Util.formatDateTime;
    vm.authentication = Authentication;
    vm.accountCodes = [];
    vm.bank = bank;
    vm.error = null;
    vm.form = {};
    vm.countries = ReferencesService.query({ code : 'COUNTRY', status : true });
    vm.currencies = ReferencesService.query({ code : 'CURRENCY', status : true });
    vm.save = save;

    vm.openActionCodeLookup = openActionCodeLookup;

    
    function openActionCodeLookup(index){
      var modalInstance = $modal.open({
        animation : true,
        templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
        controller : 'CombinationLookupController',
        controllerAs: 'vm',
        size : 'lg',
        resolve : {
          combinations : function(){
            return vm.combinations;
          },
          index : index
        }
      });

      modalInstance.result.then(function (selected) {
        vm.bank.accountCode = selected.combination._id;
      }, null);
    }

    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.bankForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.bankForm.$dirty = false;
            $state.go(toState);
          });
        }
        
    });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    // Save Bank
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.bankForm');
        return false;
      }

      var isCreate = vm.bank._id ? false : true;
      if (isCreate) {
        vm.bank.$save(successCallback, errorCallback);
      } else {
        vm.bank.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Bank successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.bankForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        if(vm.error === '11000 duplicate key error collection: poweraire-erp.banks index: code already exists'){
          alertify.error('Bank code already exists');
          return;
        }
        alertify.error(vm.error);
      }
    }
      
    
  }
})();
