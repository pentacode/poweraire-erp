(function () {
  'use strict';

  angular
    .module('banks')
    .controller('BanksListController', BanksListController);

  BanksListController.$inject = ['$scope', '$filter','BanksService'];

  function BanksListController($scope, $filter, BanksService) {
    BanksService.query(function (data) {
      $scope.banks = data;
      $scope.codes = [];
      $scope.names = [];
      $scope.accountNumbers = [];
      for (var i = 0; i < $scope.banks.length; i++) {
        var bank = $scope.banks[i];
        bank.address = '';
        if (bank.addressOne && bank.addressTwo){
          bank.address = bank.addressOne + ', ' + bank.addressTwo;
        }        
        if ($scope.codes.indexOf(bank.code) === -1) {
          $scope.codes.push(bank.code);
        }
        if ($scope.names.indexOf(bank.name) === -1) {
          $scope.names.push(bank.name);
        }
        if ($scope.accountNumbers.indexOf(bank.accountNumber) === -1) {
          $scope.accountNumbers.push(bank.accountNumber);
        }
        var combination = bank.accountCode;
        if (!combination)
          continue;
        combination.accountDescription = 'N/A';
        if (combination.company) {
          combination.accountDescription = combination.company.descriptionOne;
        }
        if (combination.location) {
          combination.accountDescription += '-' + combination.location.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.serviceType) {
          combination.accountDescription += '-' + combination.serviceType.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.majorAccount) {
          combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.minorAccount) {
          combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedOne) {
          combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedTwo) {
          combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
      }
      $scope.buildPager();
    });
      
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.banks, function(item){
        if ($scope.code && !(new RegExp('^' + $scope.code).test(item.code))) { //prefix search
          return false;
        }
        if ($scope.name && !(new RegExp($scope.name, 'i').test(item.name))) { //ignore case
          return false;
        }
        if ($scope.accountNumber && !(new RegExp($scope.accountNumber).test(item.accountNumber))) {
          return false;
        } 
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.code = '';
      $scope.name = '';
      $scope.accountNumber = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.searchCode = function () {
      $scope.cs = [];
      $scope.cs = $filter('filter')($scope.codes, function(code){
        if ($scope.code && !(new RegExp('^' + $scope.code, 'i').test(code))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchName = function () {
      $scope.ns = [];
      $scope.ns = $filter('filter')($scope.names, function(name){
        if ($scope.name && !(new RegExp($scope.name, 'i').test(name))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchAccount = function () {
      $scope.ans = [];
      $scope.ans = $filter('filter')($scope.accountNumbers, function(accountNumber){
        if ($scope.accountNumber && !(new RegExp($scope.accountNumber, 'i').test(accountNumber))) {
          return false;
        }
        return true;
      });
    };
  }
})();
