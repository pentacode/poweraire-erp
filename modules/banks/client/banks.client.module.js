(function (app) {
  'use strict';

  app.registerModule('banks');
  app.registerModule('banks.admin', ['core.admin']);
})(ApplicationConfiguration);
