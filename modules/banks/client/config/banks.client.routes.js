(function () {
  'use strict';

  angular
    .module('banks')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('banks', {
        abstract: true,
        url: '/banks',
        template: '<ui-view/>'
      })
      .state('banks.list', {
        url: '',
        templateUrl: 'modules/banks/client/views/list-banks.client.view.html',
        controller: 'BanksListController',
        controllerAs: 'vm',
        data: {
          roles: ['Bank Accounts'],
          pageTitle: 'Banks List'
        }
      })
      .state('banks.create', {
        url: '/create',
        templateUrl: 'modules/banks/client/views/form-bank.client.view.html',
        controller: 'BanksController',
        controllerAs: 'vm',
        resolve: {
          bankResolve: newBank
        },
        data: {
          roles: ['Bank Accounts'],
          pageTitle : 'Banks Create'
        }
      })
      .state('banks.edit', {
        url: '/:bankId/edit',
        templateUrl: 'modules/banks/client/views/form-bank.client.view.html',
        controller: 'BanksController',
        controllerAs: 'vm',
        resolve: {
          bankResolve: getBank
        },
        data: {
          roles: ['Bank Accounts'],
          pageTitle: 'Edit Bank {{ bankResolve.name }}'
        }
      });
  }

  getBank.$inject = ['$stateParams', 'BanksService'];

  function getBank($stateParams, BanksService) {
    return BanksService.get({
      bankId: $stateParams.bankId
    }).$promise;
  }

  newBank.$inject = ['BanksService'];

  function newBank(BanksService) {
    return new BanksService();
  }
})();
