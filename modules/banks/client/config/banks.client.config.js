(function () {
  'use strict';

  angular
    .module('banks.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Bank Accounts',
      state: 'banks.list',
      roles: ['Bank Accounts']
    });
  }
})();
