(function (app) {
  'use strict';

  app.registerModule('combinations');
  app.registerModule('combinations.admin', ['core.admin']);
})(ApplicationConfiguration);
