(function () {
  'use strict';

  angular
    .module('combinations.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Combinations',
      state: 'combinations.list',
      roles: ['Combinations']
    });
  }
})();
