(function () {
  'use strict';

  angular
    .module('combinations')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('combinations', {
        abstract: true,
        url: '/combinations',
        template: '<ui-view/>'
      })
      .state('combinations.list', {
        url: '',
        templateUrl: 'modules/combinations/client/views/list-combinations.client.view.html',
        controller: 'CombinationsListController',
        controllerAs: 'vm',
        data: {
          roles: ['Combinations'],
          pageTitle: 'Combinations List'
        }
      })
      .state('combinations.create', {
        url: '/create',
        templateUrl: 'modules/combinations/client/views/form-combination.client.view.html',
        controller: 'CombinationsController',
        controllerAs: 'vm',
        resolve: {
          combinationResolve: newCombination
        },
        data: {
          roles: ['Combinations'],
          pageTitle : 'Combinations Create'
        }
      })
      .state('combinations.edit', {
        url: '/:combinationId/edit',
        templateUrl: 'modules/combinations/client/views/form-combination.client.view.html',
        controller: 'CombinationsController',
        controllerAs: 'vm',
        resolve: {
          combinationResolve: getCombination
        },
        data: {
          roles: ['Combinations'],
          pageTitle: 'Edit Combination {{ combinationResolve.name }}'
        }
      });
  }

  getCombination.$inject = ['$stateParams', 'CombinationsService'];

  function getCombination($stateParams, CombinationsService) {
    return CombinationsService.get({
      combinationId: $stateParams.combinationId
    }).$promise;
  }

  newCombination.$inject = ['CombinationsService'];

  function newCombination(CombinationsService) {
    return new CombinationsService();
  }
})();
