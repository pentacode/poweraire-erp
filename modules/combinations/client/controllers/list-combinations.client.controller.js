(function () {
  'use strict';

  angular
    .module('combinations')
    .controller('CombinationsListController', CombinationsListController);

  CombinationsListController.$inject = ['$scope', '$filter','CombinationsService', 'ReferencesService'];

  function CombinationsListController($scope, $filter, CombinationsService, ReferencesService) {
    CombinationsService.query(function (data) {
      $scope.combinations = data;
      $scope.buildPager();
    });

    ReferencesService.query(function (data){
      $scope.companies = [];
      $scope.locations = [];
      $scope.serviceTypes = [];
      $scope.majorAccounts = [];
      $scope.minorAccounts = [];
      $scope.reserveds = [];
      for (var i = 0; i < data.length; i++) {
        var reference = data[i];
        if (reference.code === 'COMBINATION') {
          if (reference.segmentNumber === 1) {
            $scope.companies.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 2) {
            $scope.locations.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 3) {
            $scope.serviceTypes.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 4) {
            $scope.majorAccounts.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 5) {
            $scope.minorAccounts.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 6) {
            $scope.reserveds.push(reference.descriptionOne);
          } else if (reference.segmentNumber === 7) {
            $scope.reserveds.push(reference.descriptionOne);
          }
        }
      }
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.combinations, function(combination){
        if ($scope.accountType && !(new RegExp($scope.accountType).test(combination.majorAccount.accountType))) {
          return false;
        }
        if ($scope.company && !(new RegExp($scope.company, 'i').test(combination.company.valueDescription))) {
          return false;
        }
        if ($scope.location && !(new RegExp($scope.location, 'i').test(combination.location.valueDescription))) {
          return false;
        }
        if ($scope.serviceType && !(new RegExp($scope.serviceType, 'i').test(combination.serviceType.valueDescription))) {
          return false;
        }
        if ($scope.majorAccount && !(new RegExp($scope.majorAccount, 'i').test(combination.majorAccount.valueDescription))) {
          return false;
        }
        if ($scope.minorAccount && !(new RegExp($scope.minorAccount, 'i').test(combination.minorAccount.valueDescription))) {
          return false;
        }
        if ($scope.reserved && !(new RegExp($scope.reserved, 'i').test(combination.reservedOne.valueDescription)) && !(new RegExp($scope.reserved, 'i').test(combination.reservedTwo.valueDescription))) {
          return false;
        }
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.accountType = '';
      $scope.company = '';
      $scope.location = '';
      $scope.serviceType = '';
      $scope.majorAccount = '';
      $scope.minorAccount = '';
      $scope.reserved = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.searchType = function (){
      $scope.ats = [];
      $scope.ats = $filter('filter')(['Asset','Expense','Liability','Ownership','Revenue'], function(accountType){
        if ($scope.accountType && !(new RegExp($scope.accountType, 'i').test(accountType))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchCompany = function (){
      $scope.cs = [];
      $scope.cs = $filter('filter')($scope.companies, function(company){
        if ($scope.company && !(new RegExp($scope.company, 'i').test(company))) {
          return false;
        }
        return true;
      });
    };

    $scope.searchLocation = function (){
      $scope.ls = [];
      $scope.ls = $filter('filter')($scope.locations, function(location){
        if ($scope.location && !(new RegExp($scope.location, 'i').test(location))) {
          return false;
        }
        return true;
      });
    };
      
    $scope.searchService = function (){
      $scope.sts = [];
      $scope.sts = $filter('filter')($scope.serviceTypes, function(serviceType){
        if ($scope.serviceType && !(new RegExp($scope.serviceType, 'i').test(serviceType))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchMajor = function (){
      $scope.mjs = [];
      $scope.mjs = $filter('filter')($scope.majorAccounts, function(majorAccount){
        if ($scope.majorAccount && !(new RegExp($scope.majorAccount, 'i').test(majorAccount))) {
          return false;
        }
        return true;
      });
    };
      
    $scope.searchMinor = function (){
      $scope.mns = [];
      $scope.mns = $filter('filter')($scope.minorAccounts, function(minorAccount){
        if ($scope.minorAccount && !(new RegExp($scope.minorAccount, 'i').test(minorAccount))) {
          return false;
        }
        return true;
      });
    };

    $scope.searchReserved = function (){
      $scope.rs = [];
      $scope.rs = $filter('filter')($scope.reserveds, function(reserved){
        if ($scope.reserved && !(new RegExp($scope.reserved, 'i').test(reserved))) {
          return false;
        }
        return true;
      });
    };
  }
})();
