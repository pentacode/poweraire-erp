(function() {
  'use strict';

  angular
    .module('combinations')
    .controller('CombinationLookupController', CombinationLookupController);

  CombinationLookupController.$inject = ['$scope', '$modalInstance','combinations','index','$filter', '_'];

  function CombinationLookupController($scope, $modalInstance,combinations,index,$filter, _) {
    $scope._ = _;
    var vm = this;
    vm.lineIndex = index;
    vm.combinations = combinations;

    vm.pageChanged = pageChanged;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.clear = clear;

    vm.selectRow = selectRow;

    function selectRow(combination){
      $modalInstance.close({ combination : combination, lineIndex: vm.lineIndex });
    }

    buildPager();

    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 10;
      vm.currentPage = 1;

      figureOutItemsToDisplay();
    }
    
    function figureOutItemsToDisplay() {
      vm.filtered = $filter('filter')(vm.combinations, function(combination){
        if(vm.lookupCompany && !(new RegExp(vm.lookupCompany, 'i').test(combination.company.valueDescription))){
          return false;
        }
        if(vm.lookupLocation && !(new RegExp(vm.lookupLocation, 'i').test(combination.location.valueDescription))){
          return false;
        }
        if(vm.lookupServiceTypes && !(new RegExp(vm.lookupServiceTypes, 'i').test(combination.serviceType.valueDescription))){
          return false;
        }
        if(vm.lookupMajorAccount && !(new RegExp(vm.lookupMajorAccount, 'i').test(combination.majorAccount.valueDescription))){
          return false;
        }
        if(vm.lookupMinorAccount && !(new RegExp(vm.lookupMinorAccount, 'i').test(combination.minorAccount.valueDescription))){
          return false;
        }
        if(vm.lookupReservedOnes && !(new RegExp(vm.lookupReservedOnes, 'i').test(combination.reservedOne.valueDescription))){
          return false;
        }
        if(vm.lookupReservedTwos && !(new RegExp(vm.lookupReservedTwos, 'i').test(combination.reservedTwo.valueDescription))){
          return false;
        }
        
        return true;
      });

      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filtered.slice(begin, end);
    }
    
    function pageChanged() {
      figureOutItemsToDisplay();
    }

    function clear() {
      vm.lookupCompany = '';
      vm.lookupLocation = '';
      vm.lookupServiceType = '';
      vm.lookupMajorAccount = '';
      vm.lookupMinorAccount = '';
      vm.lookupReservedOnes = '';
      vm.lookupReservedTwos = '';
      figureOutItemsToDisplay();
    }
  }
}());