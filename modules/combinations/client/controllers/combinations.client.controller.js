(function () {
  'use strict';

  // Combinations controller
  angular
    .module('combinations')
    .controller('CombinationsController', CombinationsController);

  CombinationsController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'ReferencesService', 'combinationResolve','alertify','Util'];

  function CombinationsController ($scope, $filter, $state, Authentication, ReferencesService, combination, alertify, Util) {
    var vm = this;

    vm.authentication = Authentication;
    vm.companies = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 1, status : true });
    vm.locations = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 2, status : true });
    vm.serviceTypes = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 3, status : true });
    vm.majorAccounts = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 4, status : true });
    vm.minorAccounts = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 5, status : true });
    vm.reservedOnes = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 6, status : true });
    vm.reservedTwos = ReferencesService.query({ code : 'COMBINATION', segmentNumber : 7, status : true });
    vm.combination = combination;
    vm.error = null;
    vm.form = {};

    vm.save = save;
    
    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.combinationForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.combinationForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    // Save Combination
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.combinationForm');
        return false;
      }

      var isCreate = vm.combination._id ? false : true;
      if (isCreate) {
        vm.combination.$save(successCallback, errorCallback);
      } else {
        vm.combination.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Combination successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.combinationForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
