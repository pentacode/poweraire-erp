//Combinations service used to communicate Combinations REST endpoints
(function () {
  'use strict';

  angular
    .module('combinations')
    .factory('CombinationsService', CombinationsService);

  CombinationsService.$inject = ['$resource'];

  function getAccountDescription(combination){
      
      var accountDescription ="";
      accountDescription += (combination.company) ? combination.company.descriptionOne : 'N/A'; 
      accountDescription += "-" + ((combination.location) ? combination.location.descriptionOne : 'N/A'); 
      accountDescription += "-" + ((combination.serviceType) ? combination.serviceType.descriptionOne : 'N/A'); 
      accountDescription += "-" + ((combination.majorAccount) ? combination.majorAccount.descriptionOne : 'N/A'); 
      accountDescription += "-" + ((combination.minorAccount) ? combination.minorAccount.descriptionOne : 'N/A'); 
      accountDescription += "-" + ((combination.reservedOne) ? combination.reservedOne.descriptionOne : 'N/A'); 
      accountDescription += "-" + ((combination.reservedTwo) ? combination.reservedTwo.descriptionOne : 'N/A'); 

      return accountDescription;
  }

  function getAccountCode(combination){
    var accountCode = "";
    accountCode += (combination.company) ? combination.company.value : 'N/A'; 
    accountCode += "-" + ((combination.location) ? combination.location.value : 'N/A'); 
    accountCode += "-" + ((combination.serviceType) ? combination.serviceType.value : 'N/A'); 
    accountCode += "-" + ((combination.majorAccount) ? combination.majorAccount.value : 'N/A'); 
    accountCode += "-" + ((combination.minorAccount) ? combination.minorAccount.value : 'N/A'); 
    accountCode += "-" + ((combination.reservedOne) ? combination.reservedOne.value : 'N/A'); 
    accountCode += "-" + ((combination.reservedTwo) ? combination.reservedTwo.value : 'N/A'); 

    return accountCode;
  }

  function getValueDescription(account){
      return account.value + ' - ' + account.descriptionOne;
  }

  function CombinationsService($resource) {
    return $resource('api/combinations/:combinationId', {
      combinationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      query: {
            method: 'GET',
            isArray: true,
            interceptor: {
                response: function(response) {
                    var resource = response.resource;
                    resource.forEach(function(data){
                      data.accountDescription = getAccountDescription(data);
                      data.accountCode = getAccountCode(data);
                      data.company.valueDescription = getValueDescription(data.company);
                      data.location.valueDescription = getValueDescription(data.location);
                      data.serviceType.valueDescription = getValueDescription(data.serviceType);
                      data.majorAccount.valueDescription = getValueDescription(data.majorAccount);
                      data.minorAccount.valueDescription = getValueDescription(data.minorAccount);
                      data.reservedOne.valueDescription = getValueDescription(data.reservedOne);
                      data.reservedTwo.valueDescription = getValueDescription(data.reservedTwo);
                    });
                    return resource;
                }
            }
        }
    });
  }
})();
