'use strict';

/**
 * Module dependencies
 */
var combinationsPolicy = require('../policies/combinations.server.policy'),
  combinations = require('../controllers/combinations.server.controller');

module.exports = function(app) {
  // Combinations Routes
  app.route('/api/combinations').all(combinationsPolicy.isAllowed)
    .get(combinations.list)
    .post(combinations.create);

  app.route('/api/combinations/:combinationId').all(combinationsPolicy.isAllowed)
    .get(combinations.read)
    .put(combinations.update);

  // Finish by binding the Combination middleware
  app.param('combinationId', combinations.combinationByID);
};
