'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  _ = require('lodash');

/**
 * Combination Schema
 */
var CombinationSchema = new Schema({
  accountType: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  company: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  location: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  serviceType: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  majorAccount: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  minorAccount: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  reservedOne: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  reservedTwo: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  status: {
    type: Boolean,
    default: false
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

CombinationSchema.pre('save', function(next) {
  var combinationRecord = this;
  var currentDate = new Date();
  var user = combinationRecord.user;
  combinationRecord.updated = currentDate;
  combinationRecord.updatedBy = user;

  if (combinationRecord.status) {
    combinationRecord.disabled = null;
  } else if (!combinationRecord.disabled) {
    combinationRecord.disabled = currentDate;
  }

  //check if update or create

  combination.findOne({
    company: combinationRecord.company,
    location: combinationRecord.location,
    serviceType: combinationRecord.serviceType,
    majorAccount: combinationRecord.majorAccount,
    minorAccount: combinationRecord.minorAccount,
    reservedOne: combinationRecord.reservedOne,
    reservedTwo: combinationRecord.reservedTwo
  }).then(function(result) {
    if (result) {

      if (combinationRecord.isCreate) {
        throwDuplicateError(next);
      } else {
        
        if (result._id.equals(combinationRecord._id)) {
          //update
          next();
        } else {
          throwDuplicateError(next);
        }

      }

    } else {
      if (combinationRecord.isCreate) {
        combinationRecord.created = currentDate;
        combinationRecord.createdBy = user;
      }
      //create or update
      next();
    }
  });
});

function throwDuplicateError(next) {
  var err = new Error();
  err.code = 11002;
  next(err);
}

var combination = mongoose.model('Combination', CombinationSchema);
module.exports = combination;