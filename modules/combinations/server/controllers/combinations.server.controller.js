'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Combination = mongoose.model('Combination'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Combination
 */
exports.create = function(req, res) {
  var combination = new Combination(req.body);
  combination.user = req.user;
  combination.isCreate = true;

  combination.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(combination);
    }
  });
};

/**
 * Show the current Combination
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var combination = req.combination ? req.combination.toJSON() : {};

  res.jsonp(combination);
};

/**
 * Update a Combination
 */
exports.update = function(req, res) {
  var combination = req.combination;
  combination.user = req.user;

  combination = _.extend(combination , req.body);

  combination.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(combination);
    }
  });
};

/**
 * List of Combinations
 */
exports.list = function(req, res) { 
  var query = {};
  if (req.query.status !== undefined) {
    query.status = req.query.status;
  }
  Combination.find(query)
    .sort({ 'accountType.descriptionOne' : 1, 'company.descriptionOne' : 1 })
    .populate('accountType')
    .populate('company')
    .populate('location')
    .populate('serviceType')
    .populate('majorAccount')
    .populate('minorAccount')
    .populate('reservedOne')
    .populate('reservedTwo')
    .exec(function(err, combinations) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.jsonp(combinations);
      }
    });
};

/**
 * Combination middleware
 */
exports.combinationByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Combination is invalid'
    });
  }

  Combination.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, combination) {
    if (err) {
      return next(err);
    } else if (!combination) {
      return res.status(404).send({
        message: 'No Combination with that identifier has been found'
      });
    }
    req.combination = combination;
    next();
  });
};
