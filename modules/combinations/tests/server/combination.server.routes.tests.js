'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Combination = mongoose.model('Combination'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, combination;

/**
 * Combination routes tests
 */
describe('Combination CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Combination
    user.save(function () {
      combination = {
        name: 'Combination name'
      };

      done();
    });
  });

  it('should be able to save a Combination if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Combination
        agent.post('/api/combinations')
          .send(combination)
          .expect(200)
          .end(function (combinationSaveErr, combinationSaveRes) {
            // Handle Combination save error
            if (combinationSaveErr) {
              return done(combinationSaveErr);
            }

            // Get a list of Combinations
            agent.get('/api/combinations')
              .end(function (combinationsGetErr, combinationsGetRes) {
                // Handle Combination save error
                if (combinationsGetErr) {
                  return done(combinationsGetErr);
                }

                // Get Combinations list
                var combinations = combinationsGetRes.body;

                // Set assertions
                (combinations[0].user._id).should.equal(userId);
                (combinations[0].name).should.match('Combination name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Combination if not logged in', function (done) {
    agent.post('/api/combinations')
      .send(combination)
      .expect(403)
      .end(function (combinationSaveErr, combinationSaveRes) {
        // Call the assertion callback
        done(combinationSaveErr);
      });
  });

  it('should not be able to save an Combination if no name is provided', function (done) {
    // Invalidate name field
    combination.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Combination
        agent.post('/api/combinations')
          .send(combination)
          .expect(400)
          .end(function (combinationSaveErr, combinationSaveRes) {
            // Set message assertion
            (combinationSaveRes.body.message).should.match('Please fill Combination name');

            // Handle Combination save error
            done(combinationSaveErr);
          });
      });
  });

  it('should be able to update an Combination if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Combination
        agent.post('/api/combinations')
          .send(combination)
          .expect(200)
          .end(function (combinationSaveErr, combinationSaveRes) {
            // Handle Combination save error
            if (combinationSaveErr) {
              return done(combinationSaveErr);
            }

            // Update Combination name
            combination.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Combination
            agent.put('/api/combinations/' + combinationSaveRes.body._id)
              .send(combination)
              .expect(200)
              .end(function (combinationUpdateErr, combinationUpdateRes) {
                // Handle Combination update error
                if (combinationUpdateErr) {
                  return done(combinationUpdateErr);
                }

                // Set assertions
                (combinationUpdateRes.body._id).should.equal(combinationSaveRes.body._id);
                (combinationUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Combinations if not signed in', function (done) {
    // Create new Combination model instance
    var combinationObj = new Combination(combination);

    // Save the combination
    combinationObj.save(function () {
      // Request Combinations
      request(app).get('/api/combinations')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Combination if not signed in', function (done) {
    // Create new Combination model instance
    var combinationObj = new Combination(combination);

    // Save the Combination
    combinationObj.save(function () {
      request(app).get('/api/combinations/' + combinationObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', combination.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Combination with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/combinations/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Combination is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Combination which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Combination
    request(app).get('/api/combinations/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Combination with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Combination if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Combination
        agent.post('/api/combinations')
          .send(combination)
          .expect(200)
          .end(function (combinationSaveErr, combinationSaveRes) {
            // Handle Combination save error
            if (combinationSaveErr) {
              return done(combinationSaveErr);
            }

            // Delete an existing Combination
            agent.delete('/api/combinations/' + combinationSaveRes.body._id)
              .send(combination)
              .expect(200)
              .end(function (combinationDeleteErr, combinationDeleteRes) {
                // Handle combination error error
                if (combinationDeleteErr) {
                  return done(combinationDeleteErr);
                }

                // Set assertions
                (combinationDeleteRes.body._id).should.equal(combinationSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Combination if not signed in', function (done) {
    // Set Combination user
    combination.user = user;

    // Create new Combination model instance
    var combinationObj = new Combination(combination);

    // Save the Combination
    combinationObj.save(function () {
      // Try deleting Combination
      request(app).delete('/api/combinations/' + combinationObj._id)
        .expect(403)
        .end(function (combinationDeleteErr, combinationDeleteRes) {
          // Set message assertion
          (combinationDeleteRes.body.message).should.match('User is not authorized');

          // Handle Combination error error
          done(combinationDeleteErr);
        });

    });
  });

  it('should be able to get a single Combination that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Combination
          agent.post('/api/combinations')
            .send(combination)
            .expect(200)
            .end(function (combinationSaveErr, combinationSaveRes) {
              // Handle Combination save error
              if (combinationSaveErr) {
                return done(combinationSaveErr);
              }

              // Set assertions on new Combination
              (combinationSaveRes.body.name).should.equal(combination.name);
              should.exist(combinationSaveRes.body.user);
              should.equal(combinationSaveRes.body.user._id, orphanId);

              // force the Combination to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Combination
                    agent.get('/api/combinations/' + combinationSaveRes.body._id)
                      .expect(200)
                      .end(function (combinationInfoErr, combinationInfoRes) {
                        // Handle Combination error
                        if (combinationInfoErr) {
                          return done(combinationInfoErr);
                        }

                        // Set assertions
                        (combinationInfoRes.body._id).should.equal(combinationSaveRes.body._id);
                        (combinationInfoRes.body.name).should.equal(combination.name);
                        should.equal(combinationInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Combination.remove().exec(done);
    });
  });
});
