(function () {
  'use strict';

  describe('Combinations Route Tests', function () {
    // Initialize global variables
    var $scope,
      CombinationsService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _CombinationsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      CombinationsService = _CombinationsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('combinations');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/combinations');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          CombinationsController,
          mockCombination;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('combinations.view');
          $templateCache.put('modules/combinations/client/views/view-combination.client.view.html', '');

          // create mock Combination
          mockCombination = new CombinationsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Combination Name'
          });

          //Initialize Controller
          CombinationsController = $controller('CombinationsController as vm', {
            $scope: $scope,
            combinationResolve: mockCombination
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:combinationId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.combinationResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            combinationId: 1
          })).toEqual('/combinations/1');
        }));

        it('should attach an Combination to the controller scope', function () {
          expect($scope.vm.combination._id).toBe(mockCombination._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/combinations/client/views/view-combination.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          CombinationsController,
          mockCombination;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('combinations.create');
          $templateCache.put('modules/combinations/client/views/form-combination.client.view.html', '');

          // create mock Combination
          mockCombination = new CombinationsService();

          //Initialize Controller
          CombinationsController = $controller('CombinationsController as vm', {
            $scope: $scope,
            combinationResolve: mockCombination
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.combinationResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/combinations/create');
        }));

        it('should attach an Combination to the controller scope', function () {
          expect($scope.vm.combination._id).toBe(mockCombination._id);
          expect($scope.vm.combination._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/combinations/client/views/form-combination.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          CombinationsController,
          mockCombination;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('combinations.edit');
          $templateCache.put('modules/combinations/client/views/form-combination.client.view.html', '');

          // create mock Combination
          mockCombination = new CombinationsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Combination Name'
          });

          //Initialize Controller
          CombinationsController = $controller('CombinationsController as vm', {
            $scope: $scope,
            combinationResolve: mockCombination
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:combinationId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.combinationResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            combinationId: 1
          })).toEqual('/combinations/1/edit');
        }));

        it('should attach an Combination to the controller scope', function () {
          expect($scope.vm.combination._id).toBe(mockCombination._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/combinations/client/views/form-combination.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
