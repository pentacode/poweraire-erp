(function (app) {
  'use strict';

  app.registerModule('reports');
  app.registerModule('reports.admin', ['core.admin']);
})(ApplicationConfiguration);
