'use strict';

/**
 * Module dependencies.
 */
 var path = require('path'),
 mongoose = require('mongoose'),
 Invoice = mongoose.model('Invoice'),
 Calendar = mongoose.model('Calendar'),
 async = require('async'),
 errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
 _ = require('lodash');


/**
 * Manually create a Invoice
 */
 exports.create = function (req, res) {
 	async.each(req.body, function(invRec, next){
 		var invoiceObj = new Invoice(invRec);
 		invoiceObj.user = req.user;
 		var query = {
 			module: {
 				$elemMatch: {
 					moduleName: 'Accounts Payable'
 				}
 			},
 			fromDate: {
 				$lte: invoiceObj.date
 			},
 			toDate: {
 				$gte: invoiceObj.date
 			}
 		};
 		Calendar.find(query).exec(function (err, record) {
 			if (err) {
 				return next(err);
 			}

 			if (record.length === 0) {
 				return next('Cannot create Invoice (No existing period for date: ' + invoiceObj.date + ')');
 			} else {
 				var calendar = record[0].module.find(function (obj) {
 					return obj.moduleName == 'Accounts Payable';
 				});
 				if (calendar.status === 'Closed' || calendar.status === 'Future') {
 					return next('Accounts Payable Calendar has status of ' + calendar.status + '.\nCannot create Invoice for the specified date, calendar must have OPEN status');
 				} else {
 					console.log(invoiceObj);
 					invoiceObj.save(function(err) {
 						if (err) {
 							next(err);
 						} else {
 							next();
 						}
 					});
 				}
 			}
 		});
 	}, function(err){
 		if (err) {
 			return res.status(400).send({
 				message: 'Error in creation'
 			});
 		} else {
 			res.jsonp('Success on creating Invoice!');
 		}
 	});
 };



/**
 * Show the current Invoice
 */
 exports.read = function (req, res) {
 	// convert mongoose document to JSON
 	var invoice = req.invoice ? req.invoice.toJSON() : {};

 	// Add a custom field to the Article, for determining if the current User is the "owner".
 	// NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
 	invoice.isCurrentUserOwner = req.user && invoice.user && invoice.user._id.toString() === req.user._id.toString();

 	res.jsonp(invoice);
 };

/**
 * Update a Invoice
 */
 exports.update = function (req, res) {
 	var invoice = req.invoice;

 	invoice = _.extend(invoice, req.body);

 	invoice.save(function (err) {
 		if (err) {
 			return res.status(400).send({
 				message: errorHandler.getErrorMessage(err)
 			});
 		} else {
      console.log(invoice);
 			res.jsonp(invoice);
 		}
 	});
 };

/**
 * List of Invoices
 */
 exports.list = function (req, res) {
 	Invoice.find()
 	.sort('-created')
 	.populate({
        path : 'supplier',
        model : 'Supplier',
        populate : {
          path : 'withholdingTax',
          model : 'Tax',
      },
      populate : {
          path : 'vat',
          model : 'Tax',
      }
    }).populate({
 		path: 'paymentTerms',
 		model: 'Reference'
 	})
 	.populate({
 		path: 'currency',
 		model: 'Reference'
 	})
 	.populate({
 		path: 'purchaseOrder',
 		model: 'PurchaseOrder'
 	})
 	.populate({
 		path: 'lines',
 		populate: [{
 			path: 'item',
 			model: 'Item'
 		}, {
 			path: 'uom',
 			model: 'Reference'
 		}, {
 			path: 'vat',
 			model: 'Tax'
 		}, {
 			path: 'withholdingTax',
 			model: 'Tax'
 		}, {
 			path: 'chargeAccount',
 			model: 'Combination'
 		}, {
      path: 'linePurchaseOrder',
      model: 'PurchaseOrder'
    }]
 	})
 	.exec(function (err, invoices) {
 		if (err) {
 			return res.status(400).send({
 				message: errorHandler.getErrorMessage(err)
 			});
 		} else {
 			console.log(invoices);
 			res.jsonp(invoices);
 		}
 	});

 };

/**
 * Invoice middleware
 */
 exports.invoiceByID = function (req, res, next, id) {

 	if (!mongoose.Types.ObjectId.isValid(id)) {
 		return res.status(400).send({
 			message: 'Invoice is invalid'
 		});
 	}

 	Invoice.findById(id)
 	.populate({
 		path: 'supplier',
 		model: 'Supplier'
 	})
 	.populate({
 		path: 'paymentTerms',
 		model: 'Reference'
 	})
 	.populate({
 		path: 'currency',
 		model: 'Reference'
 	})
 	.populate({
 		path: 'purchaseOrder',
 		model: 'PurchaseOrder'
 	})
 	.populate({
 		path: 'lines',
 		populate: [{
 			path: 'item',
 			model: 'Item',
 			populate : {
 				path : 'uom',
 				model : 'Reference'
 			}
 		}, {
 			path: 'uom',
 			model: 'Reference'
 		}, {
 			path: 'vat',
 			model: 'Tax'
 		}, {
 			path: 'withholdingTax',
 			model: 'Tax'
 		}, {
 			path: 'chargeAccount',
 			model: 'Combination',
 			populate : {
 				path : 'majorAccount minorAccount company reservedOne reservedTwo serviceType location',
 				model : 'Reference'
 			}
 		}]
 	}).exec(function (err, invoice) {
 		if (err) {
 			return next(err);
 		} else if (!invoice) {
 			return res.status(404).send({
 				message: 'No Invoice with that identifier has been found'
 			});
 		}
 		console.log(invoice);
 		req.invoice = invoice;
 		next();
 	});
 };