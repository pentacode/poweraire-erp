'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Invoice Schema
 */
var InvoiceSchema = new Schema({
  type: {
    type: String,
    enum: ['Standard','Debit','Credit', 'Cancelled']
  },
  number : String,
  date: Date,
  supplier : {
    type : Schema.ObjectId,
    ref : 'Supplier'
  },
  paymentTerms :{
    type : Schema.ObjectId,
    ref : 'Reference'
  },
  amount : Number,
  amountPaid: Number,
  items : Number,
  withholdingTax : {
      type: Schema.ObjectId,
      ref : 'Tax'
    },
  tax: {
      type: Schema.ObjectId,
      ref : 'Tax'
    },
  status : {
    type: String,
    enum: ['Never Validated','Validated','Posted','Cancelled']
  },
  purchaseOrder : {
    type: Schema.ObjectId,
    ref: 'PurchaseOrder'
  },
  voucher : String,
  referenceInvoice: {
    type: Schema.ObjectId,
    ref : 'Invoice'
  },
  currency: {
    type: Schema.ObjectId,
    ref: 'Reference'
  },
  lines : [{
    item : {
      type : Schema.ObjectId,
      ref : 'Item'
    },
    description: String,
    received : Number,
    uom: {
      type: Schema.ObjectId,
      ref: 'Reference'
    },
	  purchasedPrice : Number,
    amount : Number,
    vat : {
      type: Schema.ObjectId,
      ref : 'Tax'
    },
    withholdingTax:{
      type: Schema.ObjectId,
      ref : 'Tax'
    },
    chargeAccount : {
      type : Schema.ObjectId,
      ref : 'Combination'
    },
    linePurchaseOrder : {
      type: Schema.ObjectId,
      ref: 'PurchaseOrder'
    }
  }],
  distributions : [{
    accountCode : String,
    accountDescription : String,
    amount : Number,
    remarks: String,
    referenceLine : String,
    validationDate : Date,
    glDate : Date
  }],
  created: Date,
  updated : Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

InvoiceSchema.pre('save', function(next) {
  var invoice = this;
  var currentDate = new Date();
  var user = invoice.user;

  invoice.updated = currentDate;
  invoice.updatedBy = user;
  if (!invoice.created){
    invoice.created = currentDate;
    invoice.createdBy = user;
    
  } 

  next();
});

mongoose.model('Invoice', InvoiceSchema);
