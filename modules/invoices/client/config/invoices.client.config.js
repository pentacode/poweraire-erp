(function () {
  'use strict';

  angular
    .module('invoices')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {

    Menus.addSubMenuItem('topbar', 'accountsPayable', {
      title: 'Invoices',
      state: 'invoices.list',
      roles: ['Invoices']
    });

  
  }
}());
