(function (app) {
  'use strict';

  app.registerModule('invoices');
  app.registerModule('invoices.admin', ['core.admin']);
}(ApplicationConfiguration));
