(function () {
	'use strict';

	angular
		.module('invoices')
		.controller('InvoicesListController', InvoicesListController);

	InvoicesListController.$inject = ['$scope', '$filter', '$ngConfirm', 'InvoicesService', '_', 'CalendarsService', 'alertify'];

	function InvoicesListController($scope, $filter, $ngConfirm, InvoicesService, _, CalendarsService, alertify) {
		$scope._ = _;
		var vm = this;
		vm.pageChanged = pageChanged;
		vm.openDate = openDate;
		vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
		vm.clear = clear;
		vm.openCalendar = openCalendar;
		vm.confirmDialog = confirmDialog;
		vm.module = 'Accounts Payable';

		InvoicesService.query(function (data) {
			vm.invoices = data;
			buildPager();
		});

		CalendarsService.query(function (data) {
			vm.calendars = data;
			vm.periods = [];
			for (var i = 0; i < vm.calendars.length; i++) {
				var calendar = vm.calendars[i];
				if (vm.periods.indexOf(calendar.periodName) === -1) {
					angular.forEach(calendar.module, function (modName) {
						if (modName.moduleName == 'Accounts Payable') {
							vm.periods.push(calendar);
						}
					})
				}
			}
		});

		function openDate($event) {
			vm.invoiceDate.opened = true;
		}

		vm.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		vm.invoiceDate = {
			opened: false
		};

		function buildPager() {
			vm.pagedItems = [];
			vm.itemsPerPage = 10;
			vm.currentPage = 1;

			figureOutItemsToDisplay();
		}


		function figureOutItemsToDisplay() {

			vm.filtered = $filter('filter')(vm.invoices, function (invoice) {
				if (vm.supplier && !(new RegExp('^' + vm.supplier).test(invoice.supplier.name))) {
					return false;
				}
				if (vm.supplierSite && !(new RegExp('^' + vm.supplierSite).test(invoice.supplier.addressOne))) {
					return false;
				}
				if (vm.invoiceNumber && !(new RegExp('^' + vm.invoiceNumber).test(invoice.number))) {
					return false;
				}
				if (vm.date && (Date.parse(invoice.date) === Date.parse(vm.date))) {
					return false;
				}
				if (vm.invoiceStatus && !(new RegExp('^' + vm.invoiceStatus).test(invoice.status))) {
					return false;
				}
				if (vm.voucher && !(new RegExp('^' + vm.voucher).test(invoice.voucher))) {
					return false;
				}
				if (vm.invoiceType && !(new RegExp('^' + vm.invoiceType).test(invoice.type))) {
					return false;
				}
				if (vm.purchaseOrderNum && !(new RegExp('^' + vm.purchaseOrderNum).test(invoice.purchaseOrder.number))) {
					return false;
				}
				return true;
			});

			var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
			var end = begin + vm.itemsPerPage;
			vm.pagedItems = vm.filtered.slice(begin, end);
		}

		function pageChanged() {
			figureOutItemsToDisplay();
		}

		function clear() {
			vm.supplier = '';
			vm.supplierSite = '';
			vm.purchaseOrderNum = '';
			vm.invoiceNumber = '';
			vm.invoiceType = '';
			vm.date = '';
			vm.invoiceCurrency = '';
			vm.invoiceStatus = '';
			vm.voucher = '';
			figureOutItemsToDisplay();
		}


		function openCalendar(event, calendar) {
			event.stopPropagation();
			var modInventory = calendar.module.find(o => o.moduleName == vm.module);
			if (modInventory.moduleName == 'Accounts Payable' && modInventory.status == 'Future') {
				CalendarsService.get({
					calendarId: calendar._id
				}, function (c) {
					c.module.find(x => x.moduleName === 'Accounts Payable').status = 'Open';
					c.$update(function () {
						calendar.module.find(x => x.moduleName === 'Accounts Payable').status = 'Open';
						alertify.success('Period successfully opened');
					}, function () {
						alertify.error('Failed to open period');
					});
				});
			} else {
				alertify.error('Cannot re-open Purchasing calendar');
			}
		};

		function confirmDialog(event, calendar) {

			event.stopPropagation();
			var stringContent = 'Are you sure you want to close "' + calendar.periodName + '" period?';
			//      if (calendar.module == 'Accounts Payable') {
			//        stringContent += " You will not be able to reopen this period.";
			//      }

			$ngConfirm({
				title: 'Confirmation!',
				content: stringContent,
				columnClass: 'medium',
				scope: $scope,
				buttons: {
					closePeriod: {
						text: 'Close Period',
						btnClass: 'btn-orange',
						action: function (scope, button) {
							$scope.closeCalendar(calendar);
						}
					},
					close: function (scope, button) {
						// closes the modal
					}
				}
			});
		};

		function closeCalendar(calendar) {
			CalendarsService.get({
				calendarId: calendar._id
			}, function (c) {
				c.module.find(x => x.moduleName === 'Accounts Payable').status = 'Closed';
				c.$update(function () {
					calendar.module.find(x => x.moduleName === 'Accounts Payable').status = 'Closed';
					alertify.success('Period successfully closed');
				}, function () {
					alertify.error('Failed to close period');
				});
			});
		};
	}
}());