(function () {
	'use strict';

	angular
	.module('invoices')
	.controller('InvoicesController', InvoicesController);

	InvoicesController.$inject = ['$scope', '$state', 'Authentication', '$http', 'invoiceResolve', 'SuppliersService', 'ReferencesService', 'PurchaseOrdersService', 'InvoicesService', 'ItemsService', 'TaxesService', 'CombinationsService', 'alertify', '_', '$location', '$alert', '$modal'];

	function InvoicesController($scope, $state, Authentication, $http, invoice, SuppliersService, ReferencesService, PurchaseOrdersService, InvoicesService, ItemsService, TaxesService, CombinationsService, alertify, _, $location, $alert, $modal) {
		$scope._ = _;
		var vm = this;
		vm.accountCodes = [];
		vm.invOpenDate = invOpenDate;
		vm.validateTotal = validateTotal;
		vm.selectPONumber = selectPONumber;
		vm.addRow = addRow;
		vm.calculateInvoice = calculateInvoice;
		vm.fillSupplierInfo = fillSupplierInfo;
		vm.suppSite = [];
		vm.invoiceLines = {};
		vm.setSelected = setSelected;
		vm.authentication = Authentication;
		vm.error = null;
		vm.form = {};
		vm.resetTax = resetTax;
		vm.total = [];
		vm.subtotal = [];
		vm.save = save;
		vm.fillItemDesc = fillItemDesc;
		vm.addLine = addLine;
		vm.deleteLine = deleteLine;
		vm.computeLineAmount = computeLineAmount;
		vm.cancelInvoice = cancelInvoice;
		vm.compute = compute;
		vm.validateInvoice = validateInvoice;
		vm.openDate = openDate;
		vm.isEnabled = false;
		vm.postToGL = postToGL;
		vm.openActionCodeLookup = openActionCodeLookup;
		vm.getPopOverDescription = getPopOverDescription;


		init();

		function init() {
			vm.invoice = [invoice];
			vm.accounts = [];

			vm.suppliers = SuppliersService.query({
				status: true
			});
			vm.paymentTerms = ReferencesService.query({
				code: 'PYMNT_TERM',
				status: true
			});
			CombinationsService.query({
				status: true
			}, function (combinations) {
				combinations.forEach(function (combination) {
					vm.accountCodes.push(combination);
				});
			});
			vm.vats = TaxesService.query({
				type: 'VAT',
				status: true
			});
			vm.withholdingTaxes = TaxesService.query({
				type: 'WTAX',
				status: true
			});
			vm.uoms = ReferencesService.query({
				code: 'UOM',
				status: true
			});
			vm.currencies = ReferencesService.query({
				code: 'CURRENCY',
				status: true
			});
			vm.paymentMethods = ReferencesService.query({
				code: 'PAYMENT_METHOD',
				status: true
			});
			vm.countryCodes = ReferencesService.query({
				code: 'COUNTRY',
				status: true
			});
			vm.supplierTypes = ReferencesService.query({
				code: 'SUPPLIER_TYPE',
				status: true
			});
			vm.companies = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 1,
				status: true
			});
			vm.locations = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 2,
				status: true
			});
			vm.serviceTypes = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 3,
				status: true
			});
			vm.majorAccounts = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 4,
				status: true
			});
			vm.minorAccounts = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 5,
				status: true
			});
			vm.reservedOnes = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 6,
				status: true
			});
			vm.reservedTwos = ReferencesService.query({
				code: 'COMBINATION',
				segmentNumber: 7,
				status: true
			});
			vm.purchaseOrders = PurchaseOrdersService.query({});
			
			vm.items = ItemsService.query({
				status: true
			});

			vm.invDateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

			vm.invoice.forEach(function (invoice, index) {
				invoice.status = invoice._id ? invoice.status : 'Never Validated';
				if (invoice._id) {
					setSelected(invoice, index);
					validateTotal(invoice.amount, index);
					fillSupplierInfo(invoice.supplier, index);
					_.forEach(invoice.lines, function (data, cntr) {
						fillLinesItem(data, cntr);
						data.chargeAccount.accountDescription = getAccountDescription(data.chargeAccount);
						if((invoice.withholdingTax && invoice.vat) && data.lines){
							vm.calculatedInvoice = true;
						}
					})
				}
				if (!_.isEmpty(invoice.date)) {
					invoice.date = new Date(invoice.date);
				}
				console.log(invoice);
			});

			vm.invoiceDateForm = [{
				opened: false
			}];

		}



		function getAccountDescription(combination) {

			var accountDescription = "";
			accountDescription += (combination.company) ? combination.company.descriptionOne : 'N/A';
			accountDescription += "-" + ((combination.location) ? combination.location.descriptionOne : 'N/A');
			accountDescription += "-" + ((combination.serviceType) ? combination.serviceType.descriptionOne : 'N/A');
			accountDescription += "-" + ((combination.majorAccount) ? combination.majorAccount.descriptionOne : 'N/A');
			accountDescription += "-" + ((combination.minorAccount) ? combination.minorAccount.descriptionOne : 'N/A');
			accountDescription += "-" + ((combination.reservedOne) ? combination.reservedOne.descriptionOne : 'N/A');
			accountDescription += "-" + ((combination.reservedTwo) ? combination.reservedTwo.descriptionOne : 'N/A');

			return accountDescription;
		}


		function invOpenDate($event, idx) {
			vm.invoiceDateForm[idx] = [];
			vm.invoiceDateForm[idx].opened = true;
		}



		function validateTotal(total, cntr) {
			if (total) {
				vm.invoice[cntr].items = total;
				vm.total[cntr] = vm.invoice[cntr].items;
				vm.subtotal[cntr] = vm.total[cntr];
			}
		}

		function selectPONumber() {
			var totalLines;
			vm.invoice.forEach(function (inv, index) {
				totalLines = 0;
				if (inv.purchaseOrder) {
					inv.supplier = inv.purchaseOrder.supplier;
					inv.lines = [];
					inv.lines = inv.purchaseOrder.lines;
					inv.lines.forEach(function (lineRec) {
						lineRec.linePurchaseOrder = inv.purchaseOrder.number;
						if (lineRec.purchasedPrice && lineRec.received) {
							lineRec.amount = (lineRec.purchasedPrice * lineRec.received);
							totalLines += lineRec.amount;
						}
					});
					inv.amount = totalLines;
					fillSupplierInfo(inv.supplier, index);
					// fillTaxFromSupplier(inv.supplier, index)
					validateTotal(inv.amount, index);
				}
			});
		}

		function addRow(invoiceDetail) {
			if (!invoiceDetail) {
				invoiceDetail = {};
			}
			var headers = {
				type: invoiceDetail.type,
				purchaseOrder: invoiceDetail.purchaseOrder,
				supplier: invoiceDetail.supplier,
				date: invoiceDetail.date,
				number: invoiceDetail.number,
				currency: invoiceDetail.currency,
				amount: invoiceDetail.amount,
				status: 'Never Validated'
			};
			// vm.invoice.push(headers);
			vm.invoice.push(headers);
		}

		function openActionCodeLookup(index) {
			var modalInstance = $modal.open({
				animation: true,
				templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
				controller: 'CombinationLookupController',
				controllerAs: 'vm',
				size: 'lg',
				resolve: {
					combinations: function () {
						return vm.combinations;
					},
					index: index
				}
			});

			modalInstance.result.then(function (selected) {
				vm.invoice.lines[selected.lineIndex].chargeAccount = selected.combination._id;
			}, null);

		}

		function fillLinesItem(line, cntr) {
			fillItem
		}

		function fillSupplierInfo(supplier, idx) {
			vm.suppSite[idx] = {
				address: 'No available address'
			};
			if (supplier) {
				SuppliersService.query({
					_id: supplier
				}, function (result) {
					result.forEach(function (supplier) {
						vm.suppSite[idx].address = supplier.addressOne + ', ' + supplier.addressTwo;
						vm.invoice[idx].paymentTerms = supplier.paymentTerms;
						vm.invoice[idx].lines[0].withholdingTax = supplier.withholdingTax;
						vm.invoice[idx].lines[0].vat = supplier.vat;
					})
				});
			}
		}

		function fillLinesItem(line, cntr) {
			fillItem(line, cntr);
			fillPO(line, cntr);
		}

		function fillSupplierInfo(supplier, idx) {
			vm.suppSite[idx] = {
				address: 'No available address'
			};
			if (supplier) {
				SuppliersService.query({
					_id: supplier
				}, function (result) {
					result.forEach(function (supplier) {
						vm.suppSite[idx].address = supplier.addressOne + ', ' + supplier.addressTwo;
						vm.invoice[idx].paymentTerms = supplier.paymentTerms;
						vm.invoice[idx].lines[0].withholdingTax = supplier.withholdingTax;
						vm.invoice[idx].lines[0].vat = supplier.vat;
					})
				});
			}
		}

		function fillItem(line, cntr) {
			ItemsService.query({}, function (result){
				result.forEach(function(item){
					if(_.isEqual(item._id,line.item._id)){
						vm.invoice[0].lines[cntr].item = item;
					}
				})
			});
		}

		function fillPO(data, index) {
			PurchaseOrdersService.query({}, function(result){
				result.forEach(function(purchaseOrder){
					if(_.isEqual(purchaseOrder._id, data.linePurchaseOrder)){
						vm.invoice[0].lines[cntr].linePurchaseOrder = purchaseOrder;
					}
				})
			})
		}

		function fillTaxFromSupplier(supplier, idx){
			var lineRec = vm.invoice[idx].lines;
			lineRec.forEach(function(object, cntr){
				object.withholdingTax = supplier.withholdingTax;
				object.vat = supplier.vat;
			});
		}

		$scope.$on('$stateChangeStart', function (event, toState) {
			if (vm.form.invoiceForm.$dirty) {
				event.preventDefault();
				$alert.confirmationBox('', 'There are unsaved changes. Are you sure you want to leave this page?', function () {
					vm.form.invoiceForm.$dirty = false;
					$state.go(toState);
				});
			}
		});


		vm.combinations = CombinationsService.query({
			status: true
		}, function (combinations) {
			combinations.forEach(function (combination) {
				vm.accounts.push(combination);
			});
		});

		function getPopOverDescription(chargeAccount) {
			var description = "";
			if (!_.isEmpty(chargeAccount)) {
				var combination = _.find(vm.combinations, {
					'_id': chargeAccount
				});
				if (!_.isEmpty(combination)) {
					description = combination.accountDescription;
				}
			}
			return description;
		}


		function setSelected(invoice, idx) {
			vm.invoice[idx] = invoice;
			vm.index = idx;
		}

		function fillItemDesc(item, index) {
			vm.invoice[0].lines[index].item = item;
			item = item.code + ' - ' + item.description;
		}

		function addLine() {
			vm.invoice.lines.push({
				item: '',
				description: '',
				quantity: 0,
				unitPrice: 0.00,
				amount: 0
			});
		}

		function deleteLine(index) {
			vm.invoice.lines.splice(index, 1);
			compute();
		}

		function computeLineAmount(list) {
			var item;
			if (_.isObject(list.item)) {
				item = list.item;
			} else {
				item = _.find(vm.items, {
					'_id': list.item
				});

			}
			list.amount = item.sellingPrice * list.quantity || 0;
			compute();
		}

		function computeTotalTax(tax) {

			var amounts = _.map(vm.invoice.lines, 'amount'),
			taxesId = _.map(vm.invoice.lines, tax),
			taxes = [];


			if (_.isObject(taxesId[0])) {
				taxes = taxesId;
			} else {
				taxesId.forEach(function (taxId) {
					taxes.push(_.find(vm.withholdingTaxes, {
						'_id': taxId
					}));
				});
			}

			var isError = false,
			taxesAmount = [];

			amounts.forEach(function (amount, index) {
				if (!_.isEmpty(taxes[index])) {
					var percentRate = taxes[index].rate / 100;
					taxesAmount.push(percentRate * amount);
				} else {
					isError = true;
					return false;
				}
			});

			if (isError) {
				return 'Cannot compute';
			}

			return _.round(_.sum(taxesAmount), 2) || 0;
		}


		function computeInvoiceAmount() {

			if (_.isNumber(vm.invoice.withholdingTax) && _.isNumber(vm.invoice.tax) && _.isNumber(vm.invoice.items)) {
				return _.round((vm.invoice.items - vm.invoice.withholdingTax) + vm.invoice.tax, 2);
			}

			return 'Cannot Compute';
		}

		function resetTax() {
			var total = 0;
			_.each(vm.invoice, function(invoice, index){
				_.each(invoice.lines, function(line, index){
					if(!_.isNil(line.item) && !_.isNil(line.amount)){
						invoice.amount = line.amount;
						invoice.amountPaid = line.amount;
						validateTotal(line.amount, index);
					} else {
						line = {};
					}
				});
				_.remove(invoice.lines, function(lineRec) {
					return _.isNil(lineRec.item);
				});
				vm.calculatedInvoice = false;
				delete invoice.withholdingTax;
				delete invoice.withholdingTax;
			});
		}

		function calculateInvoice() {
			if(vm.invoice[vm.index].withholdingTax && vm.invoice[vm.index].tax && !vm.calculatedInvoice){
				addTax(vm.invoice[vm.index].withholdingTax, vm.invoice[vm.index].tax);
				vm.calculatedInvoice = true;
			}  else {
				alertify.error("No Withholding or VAT Tax");
			}
		}

		function compute(wtax, vat) {
			var total = 0;
			_.each(vm.invoice, function(invoice){
				_.each(invoice.lines, function(record){
					if(!_.isUndefined(record.item) && !_.isNull(record.item)){
						total += record.item.purchasedPrice - (parseInt(record.item.purchasedPrice) * (wtax.rate/100)) + (parseInt(record.item.purchasedPrice) * (vat.rate/100));
					}
				});
				invoice.amount = total;
				invoice.amountPaid = total;
			})
			
			validateTotal(total, 0);
		}

		function addTax(wtax, vat) {
			_.each(vm.invoice, function(invoice, index){
				invoice.lines.push({
					description: 'Item Type - VAT',
					vat : vat,
					chargeAccount : vat.accountCode
				},{
					description: 'Item Type - WTAX',
					withholdingTax: wtax,
					chargeAccount : wtax.accountCode
				})
			});
			compute(wtax, vat);
		}

		function cancelInvoice() {

			$alert.confirmationBox('Confirm Cancellation', 'Are you sure you want to cancel this invoice?', function () {

				vm.invoice.status = 'Cancelled';
				$scope.$apply();
				save(vm.form.invoiceForm.$valid);

				if (vm.form.invoiceForm.$valid) {
					$state.go('invoices.list');
				} else {
					vm.invoice.status = 'Never Validated';
					$scope.$apply();
				}

			}, 'CancelInvoice');


		}

		function validateInvoice() {

			$alert.confirmationBox('Confirm Validation', 'Are you sure you want to validate this invoice?', function () {
				vm.invoice.status = 'Validated';
				generateDistributions();
				$scope.$apply();

				if (vm.form.invoiceForm.$valid) {
					save(vm.form.invoiceForm.$valid);
				} else {
					vm.invoice.status = 'Validated';
					$scope.$apply();
				}


			}, 'ValidateInvoice');

		}

		function generateAccountDescription(accountCode) {

			var accountDescription = "";
			accountDescription += (accountCode.company) ? accountCode.company.descriptionOne : 'N/A';
			accountDescription += "-" + ((accountCode.location) ? accountCode.location.descriptionOne : 'N/A');
			accountDescription += "-" + ((accountCode.serviceType) ? accountCode.serviceType.descriptionOne : 'N/A');
			accountDescription += "-" + ((accountCode.majorAccount) ? accountCode.majorAccount.descriptionOne : 'N/A');
			accountDescription += "-" + ((accountCode.minorAccount) ? accountCode.minorAccount.descriptionOne : 'N/A');
			accountDescription += "-" + ((accountCode.reservedOne) ? accountCode.reservedOne.descriptionOne : 'N/A');
			accountDescription += "-" + ((accountCode.reservedTwo) ? accountCode.reservedTwo.descriptionOne : 'N/A');

			return accountDescription;
		}

		function generateAccountCode(accountCode) {

			var companyCode = _.find(vm.companies, {
				'_id': accountCode.company._id
			}).value,
			locationCode = _.find(vm.locations, {
				'_id': accountCode.location._id
			}).value,
			serviceTypeCode = _.find(vm.serviceTypes, {
				'_id': accountCode.serviceType._id
			}).value,
			majorAccountCode = _.find(vm.majorAccounts, {
				'_id': accountCode.majorAccount._id
			}).value,
			minorAccountCode = _.find(vm.minorAccounts, {
				'_id': accountCode.minorAccount._id
			}).value,
			reserveOneCode = _.find(vm.reservedOnes, {
				'_id': accountCode.reservedOne._id
			}).value,
			reserveTwoCode = _.find(vm.reservedTwos, {
				'_id': accountCode.reservedTwo._id
			}).value;


			return companyCode + '-' + locationCode + '-' + serviceTypeCode + '-' + majorAccountCode + '-' + minorAccountCode +
			'-' + reserveOneCode + '-' + reserveTwoCode;
		}

		function generateDistributions() {
			vm.invoice.distributions = [];

			_.each(vm.invoice.lines, function (line) {

				var lineItem = _.find(vm.items, {
					'_id': line.item
				}),
				lineVat = _.find(vm.vats, {
					'_id': line.vat
				}),
				lineWithholdingTax = _.find(vm.withholdingTaxes, {
					'_id': line.withholdingTax
				}),
				supplier = _.find(vm.supplier, {
					'_id': vm.invoice.supplier
				});

				var lineOne = {
					accountCode: generateAccountCode(lineItem.accountCode),
					accountDescription: generateAccountDescription(lineItem.accountCode),
					amount: 100,
					remarks: 'Item',
					referenceLine: '#1',
					validationDate: new Date(),
					glDate: ''
				},
				lineTwo = {
					accountCode: generateAccountCode(lineVat.accountCode),
					accountDescription: generateAccountDescription(lineVat.accountCode),
					amount: 100,
					remarks: 'VAT',
					referenceLine: '#2',
					validationDate: new Date(),
					glDate: ''
				},
				lineThree = {
					accountCode: generateAccountCode(lineWithholdingTax.accountCode),
					accountDescription: generateAccountDescription(lineWithholdingTax.accountCode),
					amount: -20,
					remarks: 'Tax',
					referenceLine: '#3',
					validationDate: new Date(),
					glDate: ''
				},
				lineFour = {
					accountCode: generateAccountCode(supplier.accountCode),
					accountDescription: generateAccountDescription(supplier.accountCode),
					amount: -20,
					remarks: 'Account Payable',
					referenceLine: '#4',
					validationDate: new Date(),
					glDate: ''
				};

				vm.invoice.distributions.push(lineOne);
				vm.invoice.distributions.push(lineTwo);
				vm.invoice.distributions.push(lineThree);
				vm.invoice.distributions.push(lineFour);
			});

		}

		function postToGL() {

			$alert.confirmationBox('Confirm Posting', 'Are you sure you want to post this invoice?', function () {
				vm.invoice.status = 'Posted';
				_.map(vm.invoice.distributions, function (distribution) {
					distribution.glDate = new Date();
					return distribution;
				});
				$scope.$apply();
				save(true);
			}, 'PostInvoice');

		}

		function openDate($event) {
			vm.invoiceDate.opened = true;
		}

		vm.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		vm.invoiceDate = {
			opened: false
		};


		function save(isValid) {
			if((vm.invoice[0].type == 'Debit' || vm.invoice[0].type == 'Credit') && vm.invoice[0].referenceInvoice){
				if (!isValid) {
					$scope.$broadcast('show-errors-check-validity', 'vm.form.invoiceForm');
					alertify.error("Not a valid form");
					return false;
				}
			} else {
				if (!isValid) {
					if(!checkLines()){
						$scope.$broadcast('show-errors-check-validity', 'vm.form.invoiceForm');
						alertify.error("Not a valid form");
						return false;
					}
				}
			}

			

			var isCreate = vm.invoice[0]._id ? false : true;

			if (isCreate) {
				$http.post('/api/invoices/multiCreate', vm.invoice)
				.then(function () {
					alertify.success('Invoice successfully created!');
				})
				.catch(function (err) {
					alertify.error(err.message);
				});
			} else {
				vm.invoice[0].$update(successCallback, errorCallback);
			}

			init();

			function successCallback(res) {
				vm.success = 'Invoice successfully ';
				if (isCreate) {
					vm.success = vm.success + 'created';
				} else {
					vm.success = vm.success + 'updated';
				}
				vm.form.invoiceForm.$dirty = false;
				alertify.success(vm.success);
			}

			function errorCallback(res) {
				vm.error = res.message;
				alertify.error(vm.error);
			}

			function checkLines() {
				var isValid = true;
				_.each(vm.invoice, function(invoice){
					_.each(invoice.lines, function(invLines) {
						if(_.isNil(invLines.item) || _.isNil(invLines.linePurchaseOrder) || _.isNil(invLines.amount)){
							if(_.isUndefined(invLines.withholdingTax) && _.isUndefined(invLines.vat)){
								return isValid = false;
							}
						} else {
							if(_.isNil(invLines.item)  && _.isNil(invLines.linePurchaseOrder)  && _.isNil(invLines.received) && _.isNil(invLines.amount)
								&& _.isNil(invLines.withholdingTax) && _.isNil(invLines.vat) && _.isNil(invLines.chargeAccount)){
								return isValid = false;
						}
					}
				})
				});
				return isValid;
			}
		}
	}
}());