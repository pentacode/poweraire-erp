'use strict';

/**
 * Module dependencies
 */
var journalVouchersPolicy = require('../policies/journal-vouchers.server.policy'),
  journalVouchers = require('../controllers/journal-vouchers.server.controller');

module.exports = function(app) {
  // Auto Journal vouchers Routes
  app.route('/api/journal-vouchers/autoCreate').all(journalVouchersPolicy.isAllowed)
    .post(journalVouchers.autoCreate);

  app.route('/api/journal-vouchers/batchUpdate').all(journalVouchersPolicy.isAllowed)
    .post(journalVouchers.batchUpdate);

  app.route('/api/journal-vouchers/batchPost').all(journalVouchersPolicy.isAllowed)
    .post(journalVouchers.batchPost); 

  app.route('/api/journal-vouchers/batchReverse').all(journalVouchersPolicy.isAllowed)
    .post(journalVouchers.batchReverse);
  // Manual Journal vouchers Routes
  app.route('/api/journal-vouchers').all(journalVouchersPolicy.isAllowed)
    .get(journalVouchers.list)
    .post(journalVouchers.create);

  app.route('/api/journal-vouchers/:journalVoucherId').all(journalVouchersPolicy.isAllowed)
    .get(journalVouchers.read)
    .put(journalVouchers.update);

  // Finish by binding the Journal voucher middleware
  app.param('journalVoucherId', journalVouchers.journalVoucherByID);
};
