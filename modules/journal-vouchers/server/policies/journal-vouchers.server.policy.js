'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Journal vouchers Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['Staff', 'Admin'],
    allows: [{
      resources: '/api/journal-vouchers',
      permissions: '*'
    }, {
      resources: '/api/journal-vouchers/calendar',
      permissions: '*'
    }, {
      resources: '/api/journal-vouchers/:journalVoucherId',
      permissions: '*'
    },{
      resources: '/api/journal-vouchers/autoCreate',
      permissions: '*'
    },{
      resources: '/api/journal-vouchers/batchUpdate',
      permissions: '*'
    },{
      resources: '/api/journal-vouchers/batchPost',
      permissions: '*'
    },{
      resources: '/api/journal-vouchers/batchReverse',
      permissions: '*'
    }]
  }]);
};

/**
 * Check If Journal vouchers Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? ['Staff', 'Admin'] : ['guest'];

  // If an Journal voucher is being processed and the current user created it then allow any manipulation
  if (req.journalVoucher && req.user && req.journalVoucher.user && req.journalVoucher.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
