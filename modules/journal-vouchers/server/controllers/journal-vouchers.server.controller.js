'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  JournalVoucher = mongoose.model('JournalVoucher'),
  Calendar = mongoose.model('Calendar'),
  processHandler = require(path.resolve('./modules/core/server/controllers/process.server.controller')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Manually create a Journal voucher
 */
exports.create = function(req, res) {
  var journalVoucher = new JournalVoucher(req.body);
  journalVoucher.user = req.user;
  var query = { module : {$elemMatch:{moduleName: 'General Ledger'}} , fromDate : { $lte : journalVoucher.glDate }, toDate : { $gte : journalVoucher.glDate } };

  Calendar.find(query).exec(function(err, record){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    if (record.length === 0) {
      return res.status(400).send({
        message: 'Cannot create JV (No existing period for date: ' + journalVoucher.glDate + ')'
      });
    } else {
      var calendar = record[0].module.find(function( obj ) {
       return obj.moduleName == 'General Ledger'; 
     });
      if(calendar.status === 'Closed' || calendar.status === 'Future'){
        res.status(400).send({
          message: 'General Ledger Calendar has status of ' +  calendar.status + '.\nCannot create JV for the specified date, calendar must have OPEN status'
        });
      } else {
        journalVoucher.save(function(err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            res.jsonp(journalVoucher);
          }
        });
      }
    }
  });
};


/**
 * Automatically create a Journal voucher
 */
exports.autoCreate = function(req, res) {
  processHandler.postToGL(req.body, function(success) {
    if (success) {
      return res.status(200).end();
    } else {
      return res.status(400).send({
        message: 'Something went wrong.'
      });
    }
  });
};

/**
 * Show the current Journal voucher
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var journalVoucher = req.journalVoucher ? req.journalVoucher.toJSON() : {};

  res.jsonp(journalVoucher);
};

/**
 * Update a Journal voucher
 */
exports.update = function(req, res) {
  var journalVoucher = req.journalVoucher;

  journalVoucher = _.extend(journalVoucher, req.body);

  journalVoucher.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(journalVoucher);
    }
  });
};

/**
 * Delete an Journal voucher
 */
exports.delete = function(req, res) {
  var journalVoucher = req.journalVoucher;

  journalVoucher.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(journalVoucher);
    }
  });
};
/**
 * Get list of Journal vouchers by Id
 */
// exports.listJournalVoucherByIds = function(req, res){
//   var journalVoucherIds = req.journalVouchers;
//   JournalVoucher.find().sort('-created').populate('user', 'displayName').populate({ path: 'period', model: 'Calendar'}).exec(function(err, journalVouchers) {
//     if (err) {
//       return res.status(400).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     } else {
//       res.jsonp(journalVouchers);
//     }
//   });
// }

/**
 * List of Journal vouchers
 */
exports.list = function(req, res) {
  JournalVoucher.find().sort('-created').populate('user', 'displayName').populate({ path: 'period', model: 'Calendar'}).populate({path: 'batchId', model: 'JournalVoucherBatchSchema'}).exec(function(err, journalVouchers) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(journalVouchers);
    }
  });
};

/**
 * Journal voucher batch update
 */

exports.batchUpdate = function(req, res){
  processHandler.updateJournalVouchers(req.body, function(success) {
    if (success) {
      return res.status(200).end();
    } else {
      return res.status(400).send({
        message: 'Something went wrong.'
      });
    }
  });
}

/**
 * Journal voucher middleware
 */
exports.journalVoucherByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Journal voucher is invalid'
    });
  }

  JournalVoucher.findById(id).populate('user', 'displayName').exec(function (err, journalVoucher) {
    if (err) {
      return next(err);
    } else if (!journalVoucher) {
      return res.status(404).send({
        message: 'No Journal voucher with that identifier has been found'
      });
    }
    req.journalVoucher = journalVoucher;
    next();
  });
};

/**
* Batch Post Journal Vouchers
*/

exports.batchPost = function(req, res){
  var fromDate = req.param('fromDate');
  var toDate = req.param('toDate');
  var queryRec = { glDate : { $lte : toDate, $gte : fromDate}};
  processHandler.batchPostJV(queryRec, function(err, result) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    res.jsonp(result);
  });
};

/**
* Reverse Batch
*/
exports.batchReverse = function(req, res){
  var batchId = req.param('batchId');
  processHandler.reverseBatch(batchId, function(err, result){
    if(err){
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    res.jsonp(result);
  })
}