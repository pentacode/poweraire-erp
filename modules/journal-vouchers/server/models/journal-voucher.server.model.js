'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    sequenceModel = mongoose.model('Sequence'),
    Schema = mongoose.Schema;

/**
 * Journal voucher Schema
 */
var JournalVoucherSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Journal voucher name',
    trim: true
  },
  description: String,
  status: Boolean,
  reverseJv: Boolean,
  source: {
    type: String,
    enum: ['Accounts Payable','Accounts Receivable','Inventory','Purchasing','General Ledger']
  },
  number: Number,
  period: {
    type: Schema.ObjectId,
    ref: 'Calendar'
  },
  batchId: {
    type:  Schema.ObjectId,
    ref: 'JournalVoucherBatchSchema'
  },
  glDate: Date,
  manual: Boolean,
  transaction: {
    type: String,
    enum: ['Receipt','Issuance','Transfer','Invoice','Payment','Credit Memo','Debit Memo','Journal']
  },
  lines: [
    {
      lineNo: Number,
      lineDescription: String,
      accountCode: {
        type: Schema.ObjectId,
        ref: 'Combination'
      },
      debit: Number,
      credit: Number,
    }
  ],
  created: Date,
  updated : Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
});

JournalVoucherSchema.pre('save', function(next) {
  var journalVoucher = this;
  var currentDate = new Date();
  var user = journalVoucher.user;
  journalVoucher.updated = currentDate;
  journalVoucher.updatedBy = user;
  if (!journalVoucher.created){
    journalVoucher.created = currentDate;
    journalVoucher.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'JournalVoucher' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        console.log(err);
        return next(err);
      }
      journalVoucher.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('JournalVoucher', JournalVoucherSchema);
