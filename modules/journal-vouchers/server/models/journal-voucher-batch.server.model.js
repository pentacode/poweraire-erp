'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    sequenceModel = mongoose.model('Sequence'),
    Schema = mongoose.Schema,
    _ = require('lodash');

/**
 * Journal voucher Schema
 */
var JournalVoucherBatchSchema = new Schema({
  batchName: {
    type: String,
    default: '',
    required: 'Please fill Journal Voucher Batch name',
    trim: true
  },
  reverseJv: Boolean,
  batchNameSeq: Number,
  number: Number,
  source: {
    type: String,
    enum: ['Accounts Payable','Accounts Receivable','Inventory','Purchasing','General Ledger']
  },
  period: {
    type: Schema.ObjectId,
    ref: 'Calendar'
  },
  batchDate: Date,
  totalDebit: Number,
  totalCredit: Number,
  created: Date,
  updated : Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
});

JournalVoucherBatchSchema.pre('save', function(next) {
  var journalVoucherBatch = this;
  var currentDate = new Date();
  var user = journalVoucherBatch.user;
  journalVoucherBatch.updated = currentDate;
  journalVoucherBatch.updatedBy = user;
  if (!journalVoucherBatch.created){
    journalVoucherBatch.created = currentDate;
    journalVoucherBatch.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'JournalVoucherBatchSchema' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        console.log(err);
        return next(err);
      }
      journalVoucherBatch.number = sequence.value;
      var queryBatchSequence = {};
      var queryInc = {};
      if(journalVoucherBatch.source = 'Inventory'){
        queryBatchSequence = { model : 'BatchInventory' };
        queryInc = { $inc: { value : 1 } };
      } else if (journalVoucherBatch.source = 'General Ledger') {
        queryBatchSequence = { model : 'BatchGeneralLedger' };
        queryInc = { $inc: { value : 1 } };
      } else if (journalVoucherBatch.source = 'Accounts Receivable') {
        queryBatchSequence = { model : 'BatchAccReceivable' };
        queryInc = { $inc: { value : 1 } };
      } else {
        queryBatchSequence = { model : 'BatchAccPayable' };
        queryInc = { $inc: { value : 1 } };
      }

      sequenceModel.findOneAndUpdate(queryBatchSequence, queryInc, function(err, seqNum) {
        if (err) {
          console.log(err);
          return next(err);
        }
        var seqNumber = _.padLeft(seqNum.value.toString(), 3, '0');
        if(!_.includes(journalVoucherBatch.batchName, 'Reversed')){
          journalVoucherBatch.batchName = journalVoucherBatch.batchName + '' + seqNumber;
        }else{
          journalVoucherBatch.batchName = journalVoucherBatch.batchName;
        }
        next();
      });
    });
  } else {
    next();
  }
});

mongoose.model('JournalVoucherBatchSchema', JournalVoucherBatchSchema);
