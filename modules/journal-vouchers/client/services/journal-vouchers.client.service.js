// Journal vouchers service used to communicate Journal vouchers REST endpoints
(function () {
  'use strict';

  angular
  .module('journal-vouchers')
  .factory('JournalVouchersService', JournalVouchersService);

  JournalVouchersService.$inject = ['$resource'];

  function isManual(record) {
    return record ? 'Manual' : 'Automatic'
  }

  function isFinalPost(record) {
    return record ? 'Posted' : 'Unposted'
  }  

  function isReverse(record) {
    return record ? 'Reversed' : 'Not Reversed'
  }    

  function JournalVouchersService($resource) {
    return $resource('api/journal-vouchers/:journalVoucherId', {
      journalVoucherId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      query: {
        method: 'GET',
        isArray: true,
        interceptor: {
          response: function(response) {
            var resource = response.resource;
            resource.forEach(function(data){
              data.manual = isManual(data.manual);
              data.status = isFinalPost(data.status);
              data.reverseJv = isReverse(data.reverseJv);
              if(data.period){
                data.period = data.period.periodName;
              }
              if(data.batchId){
                data.batchName = data.batchId.batchName;
                data.batchId.reverseJv = data.batchId.reverseJv ? 'Reversed' : 'Not Reversed';
              }
            });
            return resource;
          }
        }
      }
    });
  }
}());
