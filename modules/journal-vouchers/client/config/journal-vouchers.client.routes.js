(function () {
  'use strict';

  angular
    .module('journal-vouchers')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('journal-vouchers', {
        abstract: true,
        url: '/journal-vouchers',
        template: '<ui-view/>'
      })
      .state('journal-vouchers.list', {
        url: '',
        templateUrl: 'modules/journal-vouchers/client/views/list-journal-vouchers.client.view.html',
        controller: 'JournalVouchersListController',
        controllerAs: 'vm',
        data: {
          roles: ['Journal Vouchers'],
          pageTitle: 'Journal Vouchers List'
        }
      })
      .state('journal-vouchers.calendar', {
        url: '',
        templateUrl: 'modules/journal-vouchers/client/views/calendar-journal-vouchers.client.view.html',
        controller: 'GeneralLedgerCalendarController',
        controllerAs: 'vm',
        data: {
          roles: ['Journal Vouchers'],
          pageTitle: 'General Ledger Calendar'
        }
      })
      .state('journal-vouchers.create', {
        url: '/create',
        templateUrl: 'modules/journal-vouchers/client/views/form-journal-voucher.client.view.html',
        controller: 'JournalVouchersController',
        controllerAs: 'vm',
        resolve: {
          journalVoucherResolve: newJournalVoucher
        },
        data: {
          roles: ['Journal Vouchers'],
          pageTitle: 'Journal Vouchers Create'
        }
      })
      .state('journal-vouchers.edit', {
        url: '/:journalVoucherId/edit',
        templateUrl: 'modules/journal-vouchers/client/views/form-journal-voucher.client.view.html',
        controller: 'JournalVouchersController',
        controllerAs: 'vm',
        params: {
          journalVouchers: null
        },
        resolve: {
          journalVoucherResolve: getJournalVoucher
        },
        data: {
          roles: ['Journal Vouchers'],
          pageTitle: 'Edit Journal Voucher {{ journal-voucherResolve.name }}'
        }
      })
      .state('journal-vouchers.view', {
        url: '/:journalVoucherId',
        templateUrl: 'modules/journal-vouchers/client/views/view-journal-voucher.client.view.html',
        controller: 'JournalVouchersController',
        controllerAs: 'vm',
        resolve: {
          journalVoucherResolve: getJournalVoucher
        },
        data: {
          roles: ['Journal Vouchers'],
          pageTitle: 'Journal Voucher {{ journal-voucherResolve.name }}'
        }
      });
  }

  getJournalVoucher.$inject = ['$stateParams', 'JournalVouchersService'];

  function getJournalVoucher($stateParams, JournalVouchersService) {
    return JournalVouchersService.get({
        journalVoucherId: $stateParams.journalVoucherId
      }).$promise;
  }

  newJournalVoucher.$inject = ['JournalVouchersService'];

  function newJournalVoucher(JournalVouchersService) {
    return new JournalVouchersService();
  }
}());
