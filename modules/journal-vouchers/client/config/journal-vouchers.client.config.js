(function () {
  'use strict';

  angular
    .module('journal-vouchers.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'generalLedger', {
      title: 'Journals',
      state: 'journal-vouchers.list',
      roles: ['Journal Vouchers']
    });

    Menus.addSubMenuItem('topbar', 'generalLedger', {
      title: 'General Ledger Calendar',
      state: 'journal-vouchers.calendar',
      roles: ['Journal Vouchers']
    });
  }
})();