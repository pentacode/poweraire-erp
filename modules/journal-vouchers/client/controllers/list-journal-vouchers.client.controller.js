(function () {
	'use strict';

	angular
		.module('journal-vouchers')
		.controller('JournalVouchersListController', JournalVouchersListController);

	JournalVouchersListController.$inject = ['JournalVouchersService', 'CalendarsService', 'CombinationsService', 'Authentication', '$scope', '$filter', 'alertify', '$http', '$ngConfirm', '$window'];

	function JournalVouchersListController(JournalVouchersService, CalendarsService, CombinationsService, Authentication, $scope, $filter, alertify, $http, $ngConfirm, $window) {
		var vm = this;
		vm.buildPager = buildPager;
		vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
		vm.pageChanged = pageChanged;
		vm.clear = clear;
		vm.viewRecord = viewRecord;
		vm.checkStatus = checkStatus;
		vm.checkReverseStatus = checkReverseStatus;
		vm.postSelectedJVs = postSelectedJVs;
		vm.getBatchIds = getBatchIds;
		vm.voucherRecord = null;
		vm.calendar = [];
		vm.batches = [];
		vm.minDate = null;
		vm.maxDate = null;
		vm.resizeMode = "BasicResizer";
		vm.onDatePickerChanged = onDatePickerChanged;
		vm.openDatePicker = openDatePicker;
		vm.setSelectedRec = setSelectedRec;
		// vm.batchUpdate = batchUpdate;
		vm.confirmDialog = confirmDialog;
		vm.isUIButtonDisabled = isUIButtonDisabled;
		vm.module = 'General Ledger';

		vm.journalVoucher = JournalVouchersService.query(function (data) {
			formatData();
			vm.batches = _.map(data, 'batchId');
		});

		CombinationsService.query(function (accounts) {
			vm.chargeAccount = accounts;
		});

		function viewRecord() {
			if (vm.voucherRecord) {
				alertify.success(vm.voucherRecord[0].name);
			}
		}

		CalendarsService.query(function (data) {
			vm.calendars = data;
			vm.periods = [];
			for (var i = 0; i < vm.calendars.length; i++) {
				var calendar = vm.calendars[i];
				if (vm.periods.indexOf(calendar.periodName) === -1) {
					angular.forEach(calendar.module, function (modName) {
						if (modName.moduleName == 'General Ledger') {
							vm.periods.push(calendar);
						}
					})
				}
			}
			buildPager();
		});

		function postToGL() {
			if (vm.period && vm.module) {
				var query = {
					module: vm.module,
				};
				for (var i = 0; i < vm.calendars.length; i++) {
					var calendar = vm.calendars[i];
					if (calendar.periodName === vm.period.periodName) {
						query.period = calendar;
						break;
					}
				}
				$http.post('/api/journal-vouchers/autoCreate', query)
				.then(function () {
					alertify.success('Journal Vouchers posted to GL');
				})
				.catch(function (err) {
					alertify.error(err.message);
				});
			} else {
				alertify.error('Please select a period and module.');
			}
		};

		function setSelectedRec(record) {
			vm.voucherRecord = record;
		}
		/*
		* Manual Sorting of Angular
		function sortField(orderBy){
		  if(orderBy === vm.sortType){
		    vm.sortType = "false";
		  } else {
		    vm.sortType = orderBy;
		  }
		}
		*
		*/

		function onDatePickerChanged(datePicker) {
			if ('toDate' === datePicker) {
				vm.maxDate = vm.calendar.toDate ? new Date(vm.calendar.toDate) : null;
			} else if ('fromDate' === datePicker) {
				vm.minDate = vm.calendar.fromDate ? new Date(vm.calendar.fromDate) : null;
				vm.calendar.periodNumber = vm.minDate.getMonth() + 1;
			}
		}

		function openDatePicker(datePicker) {
			if ('toDate' === datePicker) {
				vm.toDateOpened = true;
			} else if ('fromDate' === datePicker) {
				vm.fromDateOpened = true;
			}
		}

		function formatData() {
			angular.forEach(vm.journalVoucher, function (record) {
				var recordDate = new Date(record.glDate);
				record.glDate = recordDate.toLocaleDateString();
				record.debitTotal = 0;
				record.creditTotal = 0;
				if (!_.isEqual(record.reverseJv, 'Reversed')) {
					angular.forEach(record.lines, function (line) {
						record.debitTotal += line.debit;
						record.creditTotal += line.credit;
					});
				}
			});
			buildPager();
		}

		function buildPager() {
			vm.pagedItems = [];
			vm.itemsPerPage = 10;
			vm.currentPage = 1;
			vm.figureOutItemsToDisplay();
		};

		function figureOutItemsToDisplay() {
			vm.filteredItems = $filter('filter')(vm.journalVoucher, function (obj) {
				if (vm.source && !(new RegExp(vm.source).test(obj.source))) {
					return false;
				}
				if (vm.status && !(new RegExp(vm.status).test(obj.status))) {
					return false;
				}
				if (vm.period && !(new RegExp(vm.period).test(obj.period))) {
					return false;
				}
				if (vm.batches && !(new RegExp(vm.batches).test(obj.batchId))) {
					return false;
				}
				obj.lines.forEach(function (record) {
					if (vm.debit && !(new RegExp(vm.debit).test(record.accountCode))) {
						return false;
					}
					if (vm.credit && !(new RegExp(vm.credit).test(record.accountCode))) {
						return false;
					}
				})
				return true;
			});

			vm.filterLength = vm.filteredItems.length;
			var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
			var end = begin + vm.itemsPerPage;
			vm.pagedItems = vm.filteredItems.slice(begin, end);
		};

		function checkReverseStatus() {
			var isReversed = null,
				isPosted = null;
			if (!vm.voucherRecord) {
				return true;
			}
			angular.forEach(vm.voucherRecord, function (voucher) {
				if (voucher.status === 'Posted') {
					isPosted = true;
				}
				if (voucher.reverseJv === 'Reversed') {
					isReversed = true;
				}
			});
			if (isReversed || isPosted) {
				return true;
			}
		}

		function checkStatus() {
			var isPosted = null;
			if (!vm.voucherRecord) {
				return true;
			}
			angular.forEach(vm.voucherRecord, function (voucher) {
				if (voucher.status === 'Posted') {
					isPosted = true;
				}
			});
			if (isPosted) {
				return true;
			}
		}

		function pageChanged() {
			vm.figureOutItemsToDisplay();
		};

		function clear() {
			vm.source = '';
			vm.status = '';
			vm.period = '';
			vm.debit = '';
			vm.credit = '';
			vm.figureOutItemsToDisplay();
		};

		function getBatchIds(voucherRecords) {
			return _.map(voucherRecords, '_id');
		}

		function postSelectedJVs() {
			vm.voucherRecord.status = 'Posted';
		};

		function reverseSelectedJVs() {
			vm.voucherRecord.reverseJv = 'Reversed';
			vm.voucherRecord.debitTotal = 0;
			vm.voucherRecord.creditTotal = 0;
		}

		// function batchUpdate(selectedVouchers, updateAction){
		//   var user = Authentication.user;
		//   if (selectedVouchers){
		//     var query = {
		//       journalVoucherList : null,
		//       userId : user._id
		//     };
		//     var newJournalVouchersList = [];

		//     //depending on the action, do manual post or reverse
		//     var selectedVouchersClone = _.cloneDeep(selectedVouchers);
		//     if(_.isEqual(updateAction,'ReverseBatch')){
		//       _.forEach(selectedVouchersClone, function(voucher){
		//         voucher = reverseJv(voucher);
		//         voucher.status = voucher.status == 'Posted' ? true : false;
		//         newJournalVouchersList.push(voucher);
		//       })
		//     }
		//     if(_.isEqual(updateAction,'ManualPost')){
		//       _.forEach(selectedVouchersClone, function(voucher){
		//         voucher.status = true;
		//         voucher.reverseJv = voucher.reverseJv == 'Reversed' ? true : false;
		//         newJournalVouchersList.push(voucher);
		//       })
		//     }
		//     query.journalVoucherList = newJournalVouchersList;
		//     $http.post('/api/journal-vouchers/batchUpdate', query)
		//       .then(function() {
		//         if(_.isEqual(updateAction,'ReverseBatch')){
		//           reverseSelectedJVs();
		//         }else{
		//           manualPost();
		//         }
		//         alertify.success('Journal Vouchers successfully updated');
		//       })
		//       .catch(function(err){
		//         alertify.error(err.message);
		//       });
		//   } else {
		//     alertify.error('Please select vouchers');
		//   }
		// };

		function batchPost() {
			var query = {
				fromDate: vm.calendar.fromDate,
				toDate: vm.calendar.toDate
			};
			$http.post('/api/journal-vouchers/batchPost', query)
				.then(function (data) {
					console.log(data.status, data.data);
					if (data.status === 200) {
						alertify.success('Journal Vouchers successfully updated');
					} else {
						alertify.error('Something went wrong');
					}
					//update displayed items to post
				})
				.catch(function (err) {
					alertify.error(err.message);
				});
		}

		function reverseJvLines(journalVoucherLines) {
			var reversedLines = []
			_.forEach(journalVoucherLines, function (line) {
				delete line._id;
				var reversedLine = _.cloneDeep(line);
				reversedLine.debit = line.credit;
				reversedLine.credit = line.debit;
				reversedLines.push(reversedLine);
			})
			return createNewJvLines(journalVoucherLines, reversedLines);
		}

		function createNewJvLines(activeJvLines, reversedJvLines) {
			var newJvLines = [];
			var existingLine;
			_.forEach(reversedJvLines, function (reversedLine) {
				var lineType = reversedLine.debit == null ? 'credit' : 'debit';
				if (reversedLine.debit == null) {
					existingLine = _.find(activeJvLines, {
						'accountCode': reversedLine.accountCode,
						'debit': null
					});
				} else {
					existingLine = _.find(activeJvLines, {
						'accountCode': reversedLine.accountCode,
						'credit': null
					});
				}
				if (!_.isUndefined(existingLine)) {
					existingLine[lineType] = _.add(_.toInteger(existingLine[lineType]), _.toInteger(reversedLine[lineType]));
					newJvLines.push(existingLine);
				} else {
					newJvLines.push(reversedLine);
				}
			});

			return fixLinesGrid(newJvLines);
		}

		function fixLinesGrid(jvLines) {
			//Fixes line no
			_.forEach(jvLines, function (line, index) {
				line.lineNo = index + 1;
			})
			return jvLines;
		}

		function recomputeTotal() {
			vm.totalDebit = _.sumBy(vm.journalVoucher.lines, function (line) {
				return parseFloat(line.debit);
			});
			vm.totalCredit = _.sumBy(vm.journalVoucher.lines, function (line) {
				return parseFloat(line.credit);
			});
		}

		function reverseBatch(batchId) {
			var query = {
				batchId: batchId
			};
			$http.post('/api/journal-vouchers/batchReverse', query)
				.then(function (data) {
					if (data.status === 200) {
						$ngConfirm({
							title: 'Page Refresh!',
							content: 'Batch Reverse Success, please press ok to refresh page',
							columnClass: 'medium',
							scope: $scope,
							buttons: {
								ok: {
									text: 'Ok',
									action: function (scope, button) {
										$window.location.reload()
									}
								}
							}
						});
						// alertify.success('Succesfully reversed batch');
					} else {
						alertify.error('Something went wrong');
					}
					//update displayed items to post
				})
				.catch(function (err) {
					alertify.error(err.message);
				});
		}

		function confirmDialog(actionType, journalVoucher) {
			$ngConfirm({
				title: 'Confirmation!',
				content: 'Are you sure you want to ' + actionType + ' this journal? This cannot action cannot be undone.',
				columnClass: 'medium',
				scope: $scope,
				buttons: {
					yes: {
						text: 'Yes',
						action: function (scope, button) {
							if (actionType == 'batch post') {
								batchPost();
								return true;
							} else if (actionType == 'reverse batch') {
								reverseBatch(journalVoucher.batchId._id);
								return true;
							} else {
								JournalVouchersService.get({
									journalVoucherId: journalVoucher._id
								}, function (jv) {
									if (actionType == 'reverse') {
										jv.lines = reverseJvLines(journalVoucher.lines);
										jv.reverseJv = true;
									} else if (actionType == 'post') {
										jv.status = true;
									}
									jv.$update(function () {
										actionType == 'post' ? postSelectedJVs() : reverseSelectedJVs();
										alertify.success('JV updated successfully');
									}, function () {
										alertify.error('Failed to ' + actionType + ' JV');
									});
								});
							}
						}
					},
					no: {
						text: 'No',
						action: function (scope, button) {}
					}
				}
			});
		}

		function isUIButtonDisabled(buttonAction, voucher) {
			if (vm.journalVoucher == undefined || _.isEmpty(vm.journalVoucher)) {
				return true;
			}

			if (buttonAction == "BatchPostButton") {
				if (vm.calendar.fromDate != null && vm.calendar.fromDate != null) {
					return false;
				}
				return true;
			}
			if (vm.voucherRecord) {
				var result = false;
				if (vm.voucherRecord._id != undefined) {
					if (buttonAction == "BatchButton") {
						if (vm.voucherRecord.manual == 'Manual') {
							result = true;
						}
					}
					if (buttonAction == "BatchReverseButton") {
						if (voucher.batchId == undefined || voucher.batchId.reverseJv == 'Reversed') {
							result = true;
						}
					}
					if (buttonAction == "PostButton") {
						if (vm.voucherRecord.status == 'Posted') {
							result = true;
						}
					}
					if (buttonAction == "ReverseButton") {
						if (vm.voucherRecord.reverseJv == 'Reversed') {
							result = true;
						}
					}
				} else {
					result = true;
				}
				return result;
			}
		}
	}
}());