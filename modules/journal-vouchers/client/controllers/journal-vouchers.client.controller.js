(function () {
  'use strict';

  // Journal vouchers controller
  angular
  .module('journal-vouchers')
  .controller('JournalVouchersController', JournalVouchersController);

  JournalVouchersController.$inject = ['$scope', '$state', '$window', '$modal', 'Authentication', 'journalVoucherResolve','CombinationsService', 'CalendarsService', 'Util', 'alertify', '$ngConfirm'];

  function JournalVouchersController ($scope, $state, $window, $modal, Authentication, journalVoucher, CombinationsService, CalendarsService, Util, alertify, $ngConfirm) {
    var vm = this;

    vm.error = null;
    vm.form = {};
    vm.date = {};
    vm.accountCodes = [];
    vm.authentication = Authentication;
    vm.journalVoucher = journalVoucher;
    vm.remove = remove;
    vm.save = save;
    vm.openDate = openDate;
    vm.addLine = addLine;
    vm.addNewLine = addNewLine;
    vm.deleteLine = deleteLine;
    vm.openActionCodeLookup = openActionCodeLookup;
    vm.validateSource = validateSource;
    vm.recomputeTotal = recomputeTotal;
    vm.validateInput = validateInput;
    vm.totalDebit = 0;
    vm.totalCredit = 0;
    vm.finalPost = finalPost;
    vm.accountDesc = "";
    vm.showAccountDesc = showAccountDesc;
    vm.postingStatus = "";
    vm.periods = [];
    vm.journalVouchersList = _.cloneDeep($state.params.journalVouchers);
    vm.journalVoucherChange = journalVoucherChange;
    vm.selectedVoucher = vm.journalVouchersList ? vm.journalVouchersList[0] : null;
    vm.isUIButtonDisabled = isUIButtonDisabled;
    vm.reverseJv = reverseJv;
    vm.reloadPeriod = reloadPeriod;

    init();

    function init(){
      if(_.isUndefined(vm.journalVoucher.glDate) && _.isUndefined(vm.journalVoucher._id)){
        vm.journalVoucher.glDate = new Date().setHours(0,0,0,0);
      } else {
        vm.journalVoucher.glDate = new Date(vm.journalVoucher.glDate).setHours(0,0,0,0);
      }

      vm.reloadPeriod();

      if( _.isUndefined(vm.journalVoucher._id)){
        vm.journalVoucher.source = 'General Ledger';
        vm.journalVoucher.manual = true; 
        vm.journalVoucher.status = false;
        vm.postingStatus = 'Unposted';
        vm.journalVoucher.reverseJv = false;  
      }else{
        vm.postingStatus = vm.journalVoucher.status ? "Posted" : "Unposted";
      }

      if(!_.isEmpty(vm.journalVoucher.lines)){
        recomputeTotal();
      }

    }

    function reloadPeriod(){
      vm.periods = [];
      CalendarsService.query({queryDate : new Date(vm.journalVoucher.glDate), module: 'General Ledger'}, function(result){
        result.forEach(function(calendar, index){
          vm.periods.push(calendar);
          if(_.isUndefined(vm.journalVoucher.period) && _.isEqual(index, 0)){
            vm.journalVoucher.period = calendar._id;
          }
        })
        if(vm.periods.length == 0){
          $ngConfirm({
            title: 'Error!',
            content: 'No calendar setup for selected GL date: </br><strong>{{vm.journalVoucher.glDate | date : "mediumDate"}}</strong>',
            scope: $scope,
            buttons: {
              confirm: {
                text: 'Okay',
                btnClass: 'btn-blue',
                close: function(scope, button){
                       // prevent close;
                     }
                   }
                 }
               });
        }
      });
    }

    // Remove existing Journal voucher
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.journalVoucher.$remove($state.go('journal-vouchers.list'));
      }
    }

    function validateGlLines(){
      var lines = vm.journalVoucher.lines;
      var moneyTypeRe = /^\-?\d+\.?\d*$/;
      var faultyLines = [];
      if(lines.length < 2){
        faultyLines.push();
      } else {
        _.forEach(lines, function(line, index){
        //check if lines debit or credit is null
        if(_.isNull(line.debit) && _.isNull(line.credit)){
          faultyLines.push(index+1);
        }
        faultyLines = _.uniq(faultyLines);
      });
      }
      return faultyLines;
    }

    function checkDebitCredit(){
      var isValid = false;
      _.forEach(vm.journalVoucher.lines, function(line, index){
        if(vm.journalVoucher.lines[index+1] <= vm.journalVoucher.lines.length){
          if((_.isNull(line.debit) && !_.isNull(vm.journalVoucher.lines[index+1].credit)) || (!_.isNull(line.debit) && _.isNull(vm.journalVoucher.lines[index+1].credit))){
            isValid = true;
          }
        }
      });
      return isValid;
    }
    // Save Journal voucher
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.journalVoucherForm');

        vm.error = 'Error in form values, please check ' + _.toString(vm.form.journalVoucherForm.$error.required[0].$name);
        alertify.error(vm.error);
        return false;
      }

      var lineErrors = validateGlLines();
      if(lineErrors.length){
        alertify.error("Please correct the input values for lines "+ _.toString(lineErrors));
        return false;
      }
      if(!checkDebitCredit()) {
        alertify.error("Please make sure there is value for debit and credit");
        return false;
      }
      if(!_.isBoolean(vm.journalVoucher.status)){
        if(vm.journalVoucher.status === "Posted"){
          vm.journalVoucher.status = true;
        }else{
          vm.journalVoucher.status = false;
        }       
      }else{
        vm.journalVoucher.status = vm.journalVoucher.status ? true : false;
      }

      // vm.journalVoucher.status = vm.finalPostFlag
      // TODO: move create/update logic to service
      var isCreate = vm.journalVoucher._id ? false : true;
      if (vm.journalVoucher._id) {
        vm.journalVoucher.$update(successCallback, errorCallback);
      } else {
        vm.journalVoucher.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Journal Voucher successfully ';
        vm.error = '';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.journalVoucherForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }

    $scope.$on('$stateChangeStart', function(event, toState) {
      if(vm.form.journalVoucherForm.$dirty){
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
          vm.form.journalVoucherForm.$dirty = false;
          $state.go(toState);
        });
      } 
    });

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
      });
    });

    function openDate($event) {
      vm.date.opened = true;  
    };

    function addLine(){
      if(!vm.journalVoucher.reverseJv){
        if(_.isUndefined(vm.journalVoucher.lines)){
          vm.journalVoucher.lines = [];
        }
       //  if(!_.isEqual(vm.journalVoucher.lines.length, 0)){
       //    if(vm.journalVoucher.lines.length%2 != 0){
       //      for (var index=0; index < vm.journalVoucher.lines.length; index++){
       //        var content = vm.journalVoucher.lines[index];
       //        if(index+1 < vm.journalVoucher.lines.length){
       //          if((_.isNull(vm.journalVoucher.lines[index+1].debit) || _.isNull(content.debit)) ||  _.isEqual(vm.journalVoucher.lines[index+1].lineDescription, 'Debit - ') || _.isEqual(content.debit, 'Debit - ')){
       //           vm.journalVoucher.lines.push({
       //            lineNo: null,
       //            accountCode: null,
       //            debit: null,
       //            credit: null,
       //            lineDescription:"Credit - "
       //          });
       //           break;
       //         } else {
       //           vm.journalVoucher.lines.push({
       //            lineNo: null,
       //            accountCode: null,
       //            debit: null,
       //            credit: null,
       //            lineDescription:"Debit - "
       //          });
       //           break;
       //         } 
       //       }
             
       //     }
       //    } else {
       //     vm.journalVoucher.lines.push({
       //      lineNo: null,
       //      accountCode: null,
       //      debit: null,
       //      credit: null,
       //      lineDescription:"Debit - "
       //    },{
       //      lineNo: null,
       //      accountCode: null,
       //      debit: null,
       //      credit: null,
       //      lineDescription:"Credit - "
       //    });
       //   }
       // } else {
        vm.journalVoucher.lines.push({
          lineNo: null,
          accountCode: null,
          debit: null,
          credit: null,
          lineDescription:"Debit - "
        },{
          lineNo: null,
          accountCode: null,
          debit: null,
          credit: null,
          lineDescription:"Credit - "
        });
      // }
      fixLinesGrid();
    }
  };

  function addNewLine(line, reverseTriggered, originalJvLines){
    if(_.isUndefined(line)){
      return;
    }
    var lineErrors = [];
    var lineMerged = false;
    if(_.isNull(line.accountCode)){
      lineErrors.push("Account code cant be null");
    }
    if(_.isNaN(line.debit) && _.isNan(line.credit)){
      lineErrors.push("Debit or Credit can't be null");
    }

    if(_.isEmpty(lineErrors)){
      var lineType = line.debit == null ? 'credit' : 'debit';
      _.forEach(vm.journalVoucher.lines, function(jvLine, index){
        if(!lineMerged && vm.journalVoucher.lines.length > 1 && _.isEqual(jvLine.accountCode, line.accountCode)
         &&  !_.isNull(jvLine[lineType])){
          if(_.isUndefined(originalJvLines)){
            if(line != jvLine){
              jvLine[lineType] = _.add(_.toInteger(jvLine[lineType]),_.toInteger(line[lineType]));
              lineMerged = true;
            }
          }else{
            jvLine[lineType] = _.add(_.toInteger(originalJvLines[index][lineType]),_.toInteger(line[lineType]));
            lineMerged = true;
          }

          if(!reverseTriggered && lineMerged){
            deleteLine(vm.journalVoucher.lines.length-1);
            alertify.success("New account line has been merged to "+ lineType+" of line "+ (index+1)+ " because of same account code.");
          }
        }
      });
      if(!lineMerged && reverseTriggered){
        vm.journalVoucher.lines.push(line);
      }
    }else{
      alertify.error("Please correct line errors: " + _.join(lineErrors), ',');
    }  
  }

  function deleteLine(index){
    if(!vm.journalVoucher.reverseJv){
      if(!_.isEqual(index,0)){
        vm.journalVoucher.lines.splice(index-1, 2);
      } else {
        vm.journalVoucher.lines.splice(index, 2);
      }
      fixLinesGrid()        
    }
    if(_.isEmpty(vm.journalVoucher.lines)){
      vm.accountDesc = "";
    }
  }

  function validateSource(){
    if(_.isNull(vm.journalVoucher._id)){
      return true;
    }
    return _.isEqual(vm.journalVoucher.source, "General Ledger") ? true : false;
  }

  function recomputeTotal(){
    vm.totalDebit= _.sumBy(vm.journalVoucher.lines, function(line){
      if(!_.isNull(line.debit)) return parseFloat(line.debit);
      return 0;   
    });
    vm.totalCredit = _.sumBy(vm.journalVoucher.lines, function(line){
      if(!_.isNull(line.credit)) return parseFloat(line.credit);
      return 0;
    });    
  }

  function openCalendarLookup(index){
    var modalInstance = $modal.open({
      animation : true,
      templateUrl: 'modules/calendars/client/views/calendar-lookup.client.view.html',
      controller : 'CalendarLookupController',
      controllerAs: 'vm',
      size : 'lg',
      resolve : {
        periods : function(){
          return vm.periods;
        },
        index : index
      }
    });

    modalInstance.result.then(function (selected) {
      vm.journalVoucher.period = selected.period._id;
    }, null);
  }

  function openActionCodeLookup(index){
    var modalInstance = $modal.open({
      animation : true,
      templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
      controller : 'CombinationLookupController',
      controllerAs: 'vm',
      size : 'lg',
      resolve : {
        combinations : function(){
          return vm.combinations;
        },
        index : index
      }
    });

    modalInstance.result.then(function (selected) {
      vm.journalVoucher.lines[index].accountCode = selected.combination._id;
    }, null);
  }

    // function allowReverseJv(){
    //   if(vm.journalVoucher._id && !vm.journalVoucher.status && !vm.journalVoucher.reverseJv){
    //     return true;
    //   }
    //   return false;
    // }

    function createReversedLines(lines){
      _.forEach(lines, function(line){
        if(_.find(lines, {accountCode : line.accountCode})){

        }
      })
    }

    function reverseJv(){
      $ngConfirm({
        title: 'Confirmation!',
        content: 'Are you sure you want to reverse this journal? This cannot action cannot be undone.',
        columnClass: 'medium',
        scope: $scope,
        onDestroy: function () {
          $scope.$apply();
        },
        buttons: {
          yes: {
            text: 'Yes',
            action: function(scope, button){
              if(!vm.journalVoucher.reverseJv && vm.journalVoucher._id){
                var lineErrors = validateGlLines();
                var originalJvLines = _.cloneDeep(vm.journalVoucher.lines);
                if(lineErrors.length){
                  alertify.error("Please correct input values for lines "+ _.toString(lineErrors));
                  return true;
                }
                var reversedLines = [];
                _.forEach(originalJvLines, function(line){
                  var reversedLine = _.cloneDeep(line);
                  reversedLine._id = null;
                  reversedLine.credit = line.debit;
                  reversedLine.debit = line.credit;
                  addNewLine(reversedLine, true, originalJvLines);
                })
                vm.journalVoucher.lines = _.concat(vm.journalVoucher.lines, reversedLines);
                vm.journalVoucher.reverseJv = true;
                fixLinesGrid()     
              }
            }
          },
          no: {
            text: 'No',
            action: function(scope, button){
            }
          }
        }
      })
    }
    function fixLinesGrid(){
      //Fixes line no
      _.forEach(vm.journalVoucher.lines, function(line, index){
        line.lineNo = index + 1;
      })
      recomputeTotal();
    }

    function validateInput(value, type, index){
      if (value) {
        var moneyTypeRe = /^\d+\.?\d*$/;
        var isValid = moneyTypeRe.test(value);
        if(!isValid){
          vm.journalVoucher.lines[index][type] = null;
          alertify.error(value + " is not a valid input in GL line " + (index+1));
          return false
        }
        var reverseType = type == 'credit'? 'debit' : 'credit';
        if(!_.isNull(vm.journalVoucher.lines[index][reverseType])){
         vm.journalVoucher.lines[index][type] = null;
         alertify.error("Cannot input " + type + " because " + reverseType + " exists");
         return false
       }
       addNewLine(vm.journalVoucher.lines[index]);
       fixLinesGrid();
     }
   }

   function showAccountDesc(line){
    var selectedLineAccount = _.find(vm.accountCodes, ['_id', line.accountCode]);
    if(!_.isUndefined(selectedLineAccount)){
      vm.accountDesc = selectedLineAccount.accountDescription;
      line.isSelected = true;
    }
  }

  function finalPost(){
    vm.finalPostFlag = true;
    alertify.success("Journal Voucher has been final posted. Please save JV to apply posting status");
  }

  function journalVoucherChange(){
    $state.go('journal-vouchers.edit', { journalVoucherId: vm.selectedVoucher, journalVouchers: vm.journalVouchersList});
  }

  function isUIButtonDisabled(buttonAction){
    if(buttonAction == 'save'){
      return vm.journalVoucher.status;
    }
    if(buttonAction == 'lineAction'){
      return vm.journalVoucher.reverseJv;
    }
    if(buttonAction == 'addLine'){
      return vm.journalVoucher.reverseJv;
    }
    if(buttonAction == 'reverseJv'){
      if(vm.journalVoucher._id == null || vm.journalVoucher.lines.length == 0){
        return true;
      }
      return vm.journalVoucher.reverseJv;
    }
  }
}
}());
