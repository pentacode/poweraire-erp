(function (app) {
  'use strict';

  app.registerModule('journal-vouchers');
  app.registerModule('journal-vouchers.admin', ['core.admin']);
}(ApplicationConfiguration));
