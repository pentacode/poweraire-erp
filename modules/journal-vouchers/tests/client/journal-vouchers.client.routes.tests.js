(function () {
  'use strict';

  describe('Journal vouchers Route Tests', function () {
    // Initialize global variables
    var $scope,
      JournalVouchersService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _JournalVouchersService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      JournalVouchersService = _JournalVouchersService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('journal-vouchers');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/journal-vouchers');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          JournalVouchersController,
          mockJournalVoucher;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('journal-vouchers.view');
          $templateCache.put('modules/journal-vouchers/client/views/view-journal-voucher.client.view.html', '');

          // create mock Journal voucher
          mockJournalVoucher = new JournalVouchersService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Journal voucher Name'
          });

          // Initialize Controller
          JournalVouchersController = $controller('JournalVouchersController as vm', {
            $scope: $scope,
            journalVoucherResolve: mockJournalVoucher
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:journalVoucherId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.journalVoucherResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            journalVoucherId: 1
          })).toEqual('/journal-vouchers/1');
        }));

        it('should attach an Journal voucher to the controller scope', function () {
          expect($scope.vm.journalVoucher._id).toBe(mockJournalVoucher._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/journal-vouchers/client/views/view-journal-voucher.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          JournalVouchersController,
          mockJournalVoucher;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('journal-vouchers.create');
          $templateCache.put('modules/journal-vouchers/client/views/form-journal-voucher.client.view.html', '');

          // create mock Journal voucher
          mockJournalVoucher = new JournalVouchersService();

          // Initialize Controller
          JournalVouchersController = $controller('JournalVouchersController as vm', {
            $scope: $scope,
            journalVoucherResolve: mockJournalVoucher
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.journalVoucherResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/journal-vouchers/create');
        }));

        it('should attach an Journal voucher to the controller scope', function () {
          expect($scope.vm.journalVoucher._id).toBe(mockJournalVoucher._id);
          expect($scope.vm.journalVoucher._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/journal-vouchers/client/views/form-journal-voucher.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          JournalVouchersController,
          mockJournalVoucher;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('journal-vouchers.edit');
          $templateCache.put('modules/journal-vouchers/client/views/form-journal-voucher.client.view.html', '');

          // create mock Journal voucher
          mockJournalVoucher = new JournalVouchersService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Journal voucher Name'
          });

          // Initialize Controller
          JournalVouchersController = $controller('JournalVouchersController as vm', {
            $scope: $scope,
            journalVoucherResolve: mockJournalVoucher
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:journalVoucherId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.journalVoucherResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            journalVoucherId: 1
          })).toEqual('/journal-vouchers/1/edit');
        }));

        it('should attach an Journal voucher to the controller scope', function () {
          expect($scope.vm.journalVoucher._id).toBe(mockJournalVoucher._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/journal-vouchers/client/views/form-journalVoucher.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
