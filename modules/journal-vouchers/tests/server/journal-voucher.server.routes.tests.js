'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  JournalVoucher = mongoose.model('JournalVoucher'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  journalVoucher;

/**
 * Journal voucher routes tests
 */
describe('Journal voucher CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Journal voucher
    user.save(function () {
      journalVoucher = {
        name: 'Journal voucher name'
      };

      done();
    });
  });

  it('should be able to save a Journal voucher if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Journal voucher
        agent.post('/api/journalVouchers')
          .send(journalVoucher)
          .expect(200)
          .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
            // Handle Journal voucher save error
            if (journalVoucherSaveErr) {
              return done(journalVoucherSaveErr);
            }

            // Get a list of Journal vouchers
            agent.get('/api/journalVouchers')
              .end(function (journalVouchersGetErr, journalVouchersGetRes) {
                // Handle Journal vouchers save error
                if (journalVouchersGetErr) {
                  return done(journalVouchersGetErr);
                }

                // Get Journal vouchers list
                var journalVouchers = journalVouchersGetRes.body;

                // Set assertions
                (journalVouchers[0].user._id).should.equal(userId);
                (journalVouchers[0].name).should.match('Journal voucher name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Journal voucher if not logged in', function (done) {
    agent.post('/api/journalVouchers')
      .send(journalVoucher)
      .expect(403)
      .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
        // Call the assertion callback
        done(journalVoucherSaveErr);
      });
  });

  it('should not be able to save an Journal voucher if no name is provided', function (done) {
    // Invalidate name field
    journalVoucher.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Journal voucher
        agent.post('/api/journalVouchers')
          .send(journalVoucher)
          .expect(400)
          .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
            // Set message assertion
            (journalVoucherSaveRes.body.message).should.match('Please fill Journal voucher name');

            // Handle Journal voucher save error
            done(journalVoucherSaveErr);
          });
      });
  });

  it('should be able to update an Journal voucher if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Journal voucher
        agent.post('/api/journalVouchers')
          .send(journalVoucher)
          .expect(200)
          .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
            // Handle Journal voucher save error
            if (journalVoucherSaveErr) {
              return done(journalVoucherSaveErr);
            }

            // Update Journal voucher name
            journalVoucher.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Journal voucher
            agent.put('/api/journalVouchers/' + journalVoucherSaveRes.body._id)
              .send(journalVoucher)
              .expect(200)
              .end(function (journalVoucherUpdateErr, journalVoucherUpdateRes) {
                // Handle Journal voucher update error
                if (journalVoucherUpdateErr) {
                  return done(journalVoucherUpdateErr);
                }

                // Set assertions
                (journalVoucherUpdateRes.body._id).should.equal(journalVoucherSaveRes.body._id);
                (journalVoucherUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Journal vouchers if not signed in', function (done) {
    // Create new Journal voucher model instance
    var journalVoucherObj = new JournalVoucher(journalVoucher);

    // Save the journalVoucher
    journalVoucherObj.save(function () {
      // Request Journal vouchers
      request(app).get('/api/journalVouchers')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Journal voucher if not signed in', function (done) {
    // Create new Journal voucher model instance
    var journalVoucherObj = new JournalVoucher(journalVoucher);

    // Save the Journal voucher
    journalVoucherObj.save(function () {
      request(app).get('/api/journalVouchers/' + journalVoucherObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', journalVoucher.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Journal voucher with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/journalVouchers/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Journal voucher is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Journal voucher which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Journal voucher
    request(app).get('/api/journalVouchers/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Journal voucher with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Journal voucher if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Journal voucher
        agent.post('/api/journalVouchers')
          .send(journalVoucher)
          .expect(200)
          .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
            // Handle Journal voucher save error
            if (journalVoucherSaveErr) {
              return done(journalVoucherSaveErr);
            }

            // Delete an existing Journal voucher
            agent.delete('/api/journalVouchers/' + journalVoucherSaveRes.body._id)
              .send(journalVoucher)
              .expect(200)
              .end(function (journalVoucherDeleteErr, journalVoucherDeleteRes) {
                // Handle journalVoucher error error
                if (journalVoucherDeleteErr) {
                  return done(journalVoucherDeleteErr);
                }

                // Set assertions
                (journalVoucherDeleteRes.body._id).should.equal(journalVoucherSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Journal voucher if not signed in', function (done) {
    // Set Journal voucher user
    journalVoucher.user = user;

    // Create new Journal voucher model instance
    var journalVoucherObj = new JournalVoucher(journalVoucher);

    // Save the Journal voucher
    journalVoucherObj.save(function () {
      // Try deleting Journal voucher
      request(app).delete('/api/journalVouchers/' + journalVoucherObj._id)
        .expect(403)
        .end(function (journalVoucherDeleteErr, journalVoucherDeleteRes) {
          // Set message assertion
          (journalVoucherDeleteRes.body.message).should.match('User is not authorized');

          // Handle Journal voucher error error
          done(journalVoucherDeleteErr);
        });

    });
  });

  it('should be able to get a single Journal voucher that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Journal voucher
          agent.post('/api/journalVouchers')
            .send(journalVoucher)
            .expect(200)
            .end(function (journalVoucherSaveErr, journalVoucherSaveRes) {
              // Handle Journal voucher save error
              if (journalVoucherSaveErr) {
                return done(journalVoucherSaveErr);
              }

              // Set assertions on new Journal voucher
              (journalVoucherSaveRes.body.name).should.equal(journalVoucher.name);
              should.exist(journalVoucherSaveRes.body.user);
              should.equal(journalVoucherSaveRes.body.user._id, orphanId);

              // force the Journal voucher to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Journal voucher
                    agent.get('/api/journalVouchers/' + journalVoucherSaveRes.body._id)
                      .expect(200)
                      .end(function (journalVoucherInfoErr, journalVoucherInfoRes) {
                        // Handle Journal voucher error
                        if (journalVoucherInfoErr) {
                          return done(journalVoucherInfoErr);
                        }

                        // Set assertions
                        (journalVoucherInfoRes.body._id).should.equal(journalVoucherSaveRes.body._id);
                        (journalVoucherInfoRes.body.name).should.equal(journalVoucher.name);
                        should.equal(journalVoucherInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      JournalVoucher.remove().exec(done);
    });
  });
});
