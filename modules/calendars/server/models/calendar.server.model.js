'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Calendar Schema
 */
var CalendarSchema = new Schema({
  periodNumber: {
    type: Number,
    min: 1,
    max: 12
  },
  periodName: {
    type: String,
    required: 'Period name is required'
  },
  module: [{
    status: {
      type: String,
      enum: ['Open', 'Future', 'Closed']
    },
    moduleName: {
      type: String,
      enum: ['Inventory', 'General Ledger', 'Accounts Receivable', 'Accounts Payable', 'Purchasing']
    }
  }],
  fromDate: {
    type: Date,
    required: 'From date is required'
  },
  toDate: {
    type: Date,
    required: 'To date is required'
  },
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

CalendarSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate; 
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  next();
});

mongoose.model('Calendar', CalendarSchema);
