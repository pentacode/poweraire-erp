'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Calendar = mongoose.model('Calendar'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  async = require('async'),
  _ = require('lodash');

/**
 * Create a Calendar
 */
exports.create = function(req, res) {
  var modulePeriods = ['Inventory', 'General Ledger', 'Accounts Receivable', 'Accounts Payable', 'Purchasing'];
  var calendar = new Calendar(req.body);
  var query = createQueryForValidation(calendar.fromDate, calendar.toDate, calendar.module);
  Calendar.find(query).exec(function(err, record){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    if (record.length != 0) {
      return res.status(400).send({
        message: 'Cannot create calendar (Date overlaps existing calendar dates)'
      });
    } else{
      calendar = new Calendar(req.body);
      calendar.user = req.user;
      async.each(modulePeriods, function(period, next){
        var modulePeriod = {
        status: 'Future',
        moduleName: ''
      };
        modulePeriod.moduleName = period;
        calendar.module.push(modulePeriod);
      // calendar.periodName = calendar.periodName + '- ' + period;
      calendar.save(function(err) {
            if (err) {
              next(err);
            } else {
              next();
            }
          });
      }, function(err){
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            res.jsonp('Success');
          }
      });
    }
  });
};


/**
 * Show the current Calendar
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var calendar = req.calendar ? req.calendar.toJSON() : {};

  res.jsonp(calendar);
};

/**
 * Update a Calendar
 */
exports.update = function(req, res) {
  var calendar = req.calendar;
  calendar.user = req.user;

  calendar = _.extend(calendar, req.body);
console.log(calendar);
  calendar.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(calendar);
    }
  });
};

exports.delete = function(req, res) {
  var calendar = req.calendar;
  calendar.status = 'Closed';

  calendar.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    if (calendar.moduleName === 'General Ledger') {
      var query = {periodName : calendar.periodName, id : { $ne:  calendar.id}}
      Calendar.find(query).exec(function(err, records) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }

        // async
        async.each(records, function(data, next){
          data.status = 'Closed';
          data.save(function(err) {
            if (err) {
              next(err);
            } else {
              next();
            }
          });
        }, function(err){
            if (err) {
              res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            } else {
              res.jsonp('Success');
            }
        });
      });
    } else {
      res.jsonp('Period closed successfully');
    }
  });
};

/**
 * List of Calendars
 */
exports.list = function(req, res) {
  var query = {};
  if (req.query.periodName) {
    query.periodName = req.query.periodName;
  }
  if(req.query.module){
    query.module = {};
    query.module.$elemMatch = {moduleName:req.query.module};
  }

  if(req.query.queryDate){
    var currentDate = req.query.queryDate;
    var fromDateQuery = {
      $lte:currentDate
    };
    var toDateQuery = {
      $gte:currentDate
    };

    query.$and = [];
    query.$and.push({fromDate:fromDateQuery});
    query.$and.push({toDate:toDateQuery});
  }

  Calendar.find(query).exec(function(err, calendars) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(calendars);
    }
  });
};

function createQueryForValidation(fromDate, toDate, module){
    var query = {};
    var $or = [];
    //input from date to query
    var currentFDate = fromDate;
    var fromDateQuery = {
      $lte:currentFDate
    };
    var toDateQuery = {
      $gte:currentFDate
    };

    var fromQuery = {
      fromDate:fromDateQuery,
      toDate:toDateQuery
    };

    var moduleQuery = {module : {$elemMatch:{moduleName: module}}};

    $or.push(fromQuery);

    //input to date to query
    var currentTDate = fromDate;
    var fromDateQuery2 = {
      $lte:currentTDate
    };
    var toDateQuery2 = {
      $gte:currentTDate
    };

    var toQuery = {
      fromDate:fromDateQuery2,
      toDate:toDateQuery2
    }
    $or.push(toQuery);
    query.$or = $or;
    console.log
    return query; 
}

/**
 * Calendar middleware
 */
exports.calendarByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Calendar is invalid'
    });
  }

  Calendar.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, calendar) {
    if (err) {
      return next(err);
    } else if (!calendar) {
      return res.status(404).send({
        message: 'No Calendar with that identifier has been found'
      });
    }
    req.calendar = calendar;
    next();
  });
};
