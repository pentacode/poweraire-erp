(function () {
  'use strict';

  angular
    .module('calendars.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Calendar',
      state: 'calendars.list',
      roles: ['Calendar']
    });
  }
})();
