(function (app) {
  'use strict';

  app.registerModule('calendars');
  app.registerModule('calendars.admin', ['core.admin']);
})(ApplicationConfiguration);
