(function () {
  'use strict';

  angular
  .module('calendars')
  .controller('CalendarsListController', CalendarsListController);

  CalendarsListController.$inject = ['$scope', '$filter', 'CalendarsService', 'alertify', '$http', '$modal', '$ngConfirm'];

  function CalendarsListController($scope, $filter, CalendarsService, alertify, $http, $modal, $ngConfirm) {
  	CalendarsService.query(function (data) {
      $scope.calendars = data;
      $scope.periods = [];
      for (var i = 0; i < $scope.calendars.length; i++) {
        var calendar = $scope.calendars[i];
        if ($scope.periods.indexOf(calendar.periodName) === -1) {
          $scope.periods.push(calendar.periodName);
        }
      }
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 20;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.calendars, function(calendar){
        if ($scope.periodName && !(new RegExp('^' + $scope.periodName, 'i').test(calendar.periodName))) {
          return false;
        }
        if ($scope.periodNumber && ($scope.periodNumber !== calendar.periodNumber)) {
          return false;
        }
        if ($scope.status && ($scope.status !== calendar.status)) {
        	return false;
        }
        return true;
      });

      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.calendarModules = function (moduleArr) {
      var listModule = '';
      angular.forEach(moduleArr, function(modName){
        listModule += modName.moduleName + '   ';
      });
      return listModule;
    }

    $scope.moduleStatus = function (moduleArr) {
      var modStatus = '';
      angular.forEach(moduleArr, function(modName){
        modStatus += modName.status + '   ';
      });
      return modStatus;
    }

    $scope.calendarStatus = function (moduleArr) {
      var status = 'Future';
      var openFlag = 0;
      var closeFlag = 0;
      angular.forEach(moduleArr, function(module) {
        if (module.status == 'Open'){
          openFlag += 1;
        } else if (module.status == 'Closed'){
          closeFlag += 1;
        } else {
          openFlag += 0;
          closeFlag += 0;
        }
      });

      if (closeFlag != moduleArr.length && openFlag > 0){
        status = 'Open';
      } else if (closeFlag == moduleArr.length) {
        status = 'Closed';
      }

      return status;
    }

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.searchPeriod = function () {
      $scope.ps = [];
      $scope.ps = $filter('filter')($scope.periods, function(period) {
        if ($scope.periodName && !(new RegExp('^' + $scope.periodName, 'i').test(period))) {
          return false;
        }
        return true;
      });
    };

    $scope.clear = function () {
      $scope.periodName = '';
      $scope.periodNumber = '';
      $scope.status = '';
      $scope.figureOutItemsToDisplay();
    };

    $scope.openCalendar = function(event, calendar) {
      event.stopPropagation();
      if(calendar.status == 'Future'){
        CalendarsService.get({calendarId: calendar._id}, function(c){
          c.status = 'Open';
          c.$update(function(){
            calendar.status = 'Open';
            alertify.success('Period successfully opened');
          }, function(){
            alertify.error('Failed to open period');
          });
        });
      } else {
        alertify.error('Cannot re-open Inventory calendar');
      }
    };

    $scope.confirmDialog = function(event, calendar) {

      event.stopPropagation();
      var stringContent = 'Are you sure you want to close "' + calendar.periodName + '" period?';
      if (calendar.module == 'Inventory') {
        stringContent += " You will not be able to reopen this period.";
      }

      $ngConfirm({
          title: 'Confirmation!',
          content: stringContent,
          columnClass: 'medium',
          scope: $scope,
          buttons: {
              closePeriod: {
                  text: 'Close Period',
                  btnClass: 'btn-orange',
                  action: function(scope, button){
                      $scope.closeCalendar(calendar);
                  }
              },
              close: function(scope, button){
                  // closes the modal
              }
          }
        });
    };

    $scope.closeCalendar = function(calendar) {
      CalendarsService.get({
        calendarId: calendar._id
      }, function(c){
        c.$delete(function(){
          calendar.status = 'Closed';
          alertify.success('Period successfully closed');
        }, function(){
          alertify.error('Failed to close period');
        });
      });
    };
  }
}());
