(function () {
  'use strict';

  // Calendars controller
  angular
    .module('calendars')
    .controller('CalendarsController', CalendarsController);

  CalendarsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'CalendarsService', 'calendarResolve', 'alertify', 'Util'];

  function CalendarsController ($scope, $state, $window, Authentication, CalendarsService, calendar, alertify, Util) {
    var vm = this;

    vm.authentication = Authentication;
    vm.calendar = calendar;

    if(vm.calendar){
      var existingStatuses = _.map(vm.calendar.module, 'status');
      vm.calendar.status = getCalendarStatus(existingStatuses);
    }

    if (vm.calendar.fromDate) {
      vm.calendar.fromDate = new Date(vm.calendar.fromDate);
    }

    if (vm.calendar.toDate) {
      vm.calendar.toDate = new Date(vm.calendar.toDate);
    }

    vm.error = null;
    vm.form = {};
    vm.save = save;
    vm.fromDateOpened = false;
    vm.toDateOpened = false;
    vm.minDate = null;
    vm.maxDate = null;
    vm.onPeriodNameBlur = onPeriodNameBlur;
    vm.onDatePickerChanged = onDatePickerChanged;
    vm.openDatePicker = openDatePicker;

    function getCalendarStatus(existingStatuses){
      var uniqueStatuses = _.uniq(existingStatuses);
      if(_.isEqual(uniqueStatuses.length, 1)){
        return uniqueStatuses[0];
      }
      if(_.indexOf(uniqueStatuses, 'Open') > -1){
        return 'Open';
      }
      return 'Closed';
    }

    function onPeriodNameBlur() {
      CalendarsService.query({ periodName : vm.calendar.periodName }, function(data) {
        console.log(data);
        if (data.length > 0) {
          if(!vm.calendar._id && vm.calendar._id !== data[0]._id){
            vm.form.calendarForm.periodName.$setValidity('unique', false);
          } else {
            vm.form.calendarForm.periodName.$setValidity('unique', true);
          }
        } else {
          vm.form.calendarForm.periodName.$setValidity('unique', true);
        }
        $scope.$broadcast('show-errors-check-validity', 'vm.form.calendarForm');
      });
    }

    function onDatePickerChanged(datePicker) {
      if ('toDate' === datePicker) {
        vm.maxDate = vm.calendar.toDate ? new Date(vm.calendar.toDate) : null;
      } else if ('fromDate' === datePicker) {
        vm.minDate = vm.calendar.fromDate ? new Date(vm.calendar.fromDate) : null;
        vm.calendar.periodNumber = vm.minDate.getMonth() + 1;
      }
    }

    function openDatePicker(datePicker) {
      if ('toDate' === datePicker) {
        vm.toDateOpened = true;
      } else if ('fromDate' === datePicker) {
        vm.fromDateOpened = true;
      }
    }

    $scope.$on('$stateChangeStart', function(event, toState) {
      if(vm.form.calendarForm.$dirty){
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function() {
          vm.form.calendarForm.$dirty = false;
          $state.go(toState);
        });
      }
    });

    // Save Company
    function save(isValid) {

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.calendarForm');
        return false;
      }

      // TODO: move create/update logic to service
      var isCreate = vm.calendar._id ? false : true;
      if (isCreate) {
        vm.calendar.$save(successCallback, errorCallback);
      } else {
        vm.calendar.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Calendar successfully ';
        vm.error = '';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.calendarForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
}());
