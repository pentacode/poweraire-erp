'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Onhand Schema
 */
var OnhandSchema = new Schema({
  item: {
    type: Schema.ObjectId,
    ref: 'Item'
  },
  serials: [{
    serial: {
      type: String,
      default : 'N/A'
    },
    subInventory: {
      type: Schema.ObjectId,
      ref: 'Reference'
    },
    quantity: {
      type: Number,
      required: 'Quantity is required.',
      min: [0, 'Quantity cannot be less than 0.']
    }
  }],
  transactions : [{
    transactionNo : Number,
    purchaseNo : Number,
    date: Date,
    source : String,
    action : String,
    quantity : String
  }],
  unitPrice : Number,
  quantity : Number
});

mongoose.model('Onhand', OnhandSchema);
