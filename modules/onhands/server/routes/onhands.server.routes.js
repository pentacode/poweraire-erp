'use strict';

/**
 * Module dependencies
 */
var onhandsPolicy = require('../policies/onhands.server.policy'),
  onhands = require('../controllers/onhands.server.controller');

module.exports = function(app) {
  // Onhands Routes
  app.route('/api/onhands').all(onhandsPolicy.isAllowed)
    .get(onhands.list);

  // Finish by binding the Onhand middleware
  app.param('onhandId', onhands.onhandByID);
};
