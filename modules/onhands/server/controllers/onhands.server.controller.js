'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Onhand = mongoose.model('Onhand'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Onhand
 */
exports.create = function(req, res) {
  var onhand = new Onhand(req.body);
  onhand.user = req.user;

  onhand.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(onhand);
    }
  });
};

/**
 * Show the current Onhand
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var onhand = req.onhand ? req.onhand.toJSON() : {};

  res.jsonp(onhand);
};

/**
 * Update a Onhand
 */
exports.update = function(req, res) {
  var onhand = req.onhand ;

  onhand = _.extend(onhand , req.body);

  onhand.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(onhand);
    }
  });
};

/**
 * List of Onhands
 */
exports.list = function(req, res) { 
  Onhand.find().populate([{
    path: 'item',
    model: 'Item',
    populate: [{
      path: 'category uom',
      model: 'Reference'
    }, {
      path: 'accountCode',
      model: 'Combination',
      populate : {
        path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
        model : 'Reference',
        select : 'descriptionOne'
      }
    }, {
      path: 'subInventory',
      model: 'Reference'
    }, {
      path: 'uom',
      model: 'Reference'
    }]
  }, {
    path: 'serials',
    populate: {
      path: 'subInventory',
      model: 'Reference'
    }
  }]).exec(function(err, onhands) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(onhands);
    }
  });
};

/**
 * Onhand middleware
 */
exports.onhandByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Onhand is invalid'
    });
  }

  Onhand.findById(id).populate('user', 'displayName').exec(function (err, onhand) {
    if (err) {
      return next(err);
    } else if (!onhand) {
      return res.status(404).send({
        message: 'No Onhand with that identifier has been found'
      });
    }
    req.onhand = onhand;
    next();
  });
};
