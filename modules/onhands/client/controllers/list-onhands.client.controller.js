(function () {
  'use strict';

  angular
    .module('onhands')
    .controller('OnhandsListController', OnhandsListController);

  OnhandsListController.$inject = ['$scope', '$filter', 'OnhandsService'];

  function OnhandsListController($scope, $filter, OnhandsService) {
    OnhandsService.query(function (data) {
      $scope.onhand = data;
      $scope.items = [];
      $scope.serials = [];
      $scope.subs = [];
      $scope.accountNumbers = [];
      $scope.itemDescriptions = [];
      $scope.itemCodeAndDesc = [];
      for (var i = 0; i < $scope.onhand.length; i++) {
        var onhand = $scope.onhand[i];
        if ($scope.items.indexOf(onhand.item.code) === -1) {
          $scope.items.push(onhand.item.code);
        }
        if($scope.itemDescriptions.indexOf(onhand.item.description) === -1){
          $scope.itemDescriptions.push(onhand.item.description); 
        }
        $scope.itemCodeAndDesc.push(onhand.item.code + " - " + onhand.item.description); 
        for (var j = onhand.serials.length - 1; j >=0 ; j--) {
          var serial = onhand.serials[j];
          if(serial.quantity === 0){
            onhand.serials.splice(j, 1);
          }
          if ($scope.subs.indexOf(serial.subInventory.value) === -1) {
            $scope.subs.push(serial.subInventory.value);
          }
          if ($scope.subs.indexOf(serial.subInventory.descriptionOne) === -1) {
            $scope.subs.push(serial.subInventory.descriptionOne);
          }
          if ($scope.subs.indexOf(serial.subInventory.descriptionTwo) === -1) {
            $scope.subs.push(serial.subInventory.descriptionTwo);
          }
          if ($scope.subs.indexOf(serial.subInventory.descriptionThree) === -1) {
            $scope.subs.push(serial.subInventory.descriptionThree);
          }
        }
        if (!onhand.quantity)
          onhand.quantity = 0;
        if(onhand.quantity === 0)
          onhand.unitPrice = 0;
        var combination = onhand.item.accountCode;
        if (!combination)
          continue;
        combination.accountDescription = 'N/A';
        if (combination.company) {
          combination.accountDescription = combination.company.descriptionOne;
        }
        if (combination.location) {
          combination.accountDescription += '-' + combination.location.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.serviceType) {
          combination.accountDescription += '-' + combination.serviceType.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.majorAccount) {
          combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.minorAccount) {
          combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedOne) {
          combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
        if (combination.reservedTwo) {
          combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
        } else {
          combination.accountDescription += '-' + 'N/A';
        }
      }
      $scope.buildPager();
    });
    
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
    function matchWithDescription(subInventory) {
      if(new RegExp($scope.subInventory).test(subInventory.descriptionOne) || new RegExp($scope.subInventory).test(subInventory.descriptionTwo) || new RegExp($scope.subInventory).test(subInventory.descriptionThree)){
        return true;
      }
      return false;
    }
    function isSubInventoryExistInSerials(serials){
      if(serials && serials.length > 0){
        for (var i = 0; i < serials.length; i++) {
          var serial = serials[i];
          if((new RegExp($scope.subInventory).test(serial.subInventory.value)) || matchWithDescription(serial.subInventory)){
            return true;
          }
        }
      }
      return false;
    }  
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.onhand, function(hand){   
        /*if ($scope.item && !(new RegExp($scope.item).test(hand.item.code))) {
          return false;
        }*/
        if($scope.description){
          var codeDesc = $scope.description.split(" - ");
          if (codeDesc[1]){
            if (!(new RegExp(codeDesc[1]).test(hand.item.description)) && !(new RegExp(codeDesc[0]).test(hand.item.code))){
              return false;
            }
          }else{
            var ismatch = true;
            if (!(new RegExp(codeDesc[0], 'i').test(hand.item.description))){
              ismatch = false;
            }            
            if (!ismatch && !(new RegExp(codeDesc[0], 'i').test(hand.item.code))){
              return false;
            }
          }
        }
        if ($scope.subInventory && !isSubInventoryExistInSerials(hand.serials)) {
          return false;
        }
        if ($scope.serial){
          var hasSerial = false;
          for (var j = 0; j < hand.serials.length; j++) {
            var handSerial = hand.serials[j];
            if (new RegExp($scope.serial).test(handSerial.serial)) {
              hasSerial = true;
            }
          }
          return hasSerial;
        }
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.item = '';
      $scope.subInventory = '';
      $scope.serial = '';
      $scope.description = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.seachItem = function () {
      $scope.it = [];
      $scope.it = $filter('filter')($scope.items, function(item){
        if ($scope.item && !(new RegExp($scope.item, 'i').test(item))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchSubInv = function () {
      $scope.sub = [];
      $scope.sub = $filter('filter')($scope.subs, function(sub){
        if ($scope.subInventory && !(new RegExp($scope.subInventory, 'i').test(sub))) {
          return false;
        }
        return true;
      });
    };
    
    $scope.searchSerial = function () {
      $scope.ser = [];
      $scope.ser = $filter('filter')($scope.serials, function(serial){
        if ($scope.serial && !(new RegExp($scope.serial, 'i').test(serial))) {
          return false;
        }
        return true;
      });
    };

    $scope.setSelectedIndex = function(obj, indx){
      $scope.selectedIndex = indx;
    };

    $scope.searchDescription = function(){
      $scope.desc = [];
      $scope.desc = $filter('filter')($scope.itemCodeAndDesc, function(desc){
        if ($scope.description && !(new RegExp($scope.description, 'i').test(desc))) {
          return false;
        }
        return true;
      });
    };
    
    /*$scope.searchCodeDesc = function(){
      $scope.codeDesc = [];
      $scope.desc = $filter('filter')($scope.itemCodeAndDesc, function(desc){
        if ($scope.description && !(new RegExp($scope.description, 'i').test(desc))) {
          return false;
        }
        return true;
      });
    };*/
  }
})();
