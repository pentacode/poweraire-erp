(function () {
  'use strict';

  angular
    .module('onhands')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('onhands', {
        abstract: true,
        url: '/onhands',
        template: '<ui-view/>'
      })
      .state('onhands.list', {
        url: '',
        templateUrl: 'modules/onhands/client/views/list-onhands.client.view.html',
        controller: 'OnhandsListController',
        controllerAs: 'vm',
        data: {
          roles: ['On-hand Quantity'],
          pageTitle: 'Onhands List'
        }
      });
  }

  getOnhand.$inject = ['$stateParams', 'OnhandsService'];

  function getOnhand($stateParams, OnhandsService) {
    return OnhandsService.get({
      onhandId: $stateParams.onhandId
    }).$promise;
  }

  newOnhand.$inject = ['OnhandsService'];

  function newOnhand(OnhandsService) {
    return new OnhandsService();
  }
})();
