(function () {
  'use strict';

  angular
    .module('onhands.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'inventory', {
      title: 'On-hand Quantity',
      state: 'onhands.list',
      roles: ['On-hand Quantity']
    });
  }
})();