(function (app) {
  'use strict';

  app.registerModule('onhands');
  app.registerModule('onhands.admin', ['core.admin']);
})(ApplicationConfiguration);
