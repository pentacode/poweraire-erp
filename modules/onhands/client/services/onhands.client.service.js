//Onhands service used to communicate Onhands REST endpoints
(function () {
  'use strict';

  angular
    .module('onhands')
    .factory('OnhandsService', OnhandsService);

  OnhandsService.$inject = ['$resource'];

  function OnhandsService($resource) {
    return $resource('api/onhands/:onhandId', {
      onhandId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
