(function () {
  'use strict';

  describe('Onhands Controller Tests', function () {
    // Initialize global variables
    var OnhandsController,
      $scope,
      $httpBackend,
      $state,
      Authentication,
      OnhandsService,
      mockOnhand;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$state_, _$httpBackend_, _Authentication_, _OnhandsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();

      // Point global variables to injected services
      $httpBackend = _$httpBackend_;
      $state = _$state_;
      Authentication = _Authentication_;
      OnhandsService = _OnhandsService_;

      // create mock Onhand
      mockOnhand = new OnhandsService({
        _id: '525a8422f6d0f87f0e407a33',
        name: 'Onhand Name'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Onhands controller.
      OnhandsController = $controller('OnhandsController as vm', {
        $scope: $scope,
        onhandResolve: {}
      });

      //Spy on state go
      spyOn($state, 'go');
    }));

    describe('vm.save() as create', function () {
      var sampleOnhandPostData;

      beforeEach(function () {
        // Create a sample Onhand object
        sampleOnhandPostData = new OnhandsService({
          name: 'Onhand Name'
        });

        $scope.vm.onhand = sampleOnhandPostData;
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (OnhandsService) {
        // Set POST response
        $httpBackend.expectPOST('api/onhands', sampleOnhandPostData).respond(mockOnhand);

        // Run controller functionality
        $scope.vm.save(true);
        $httpBackend.flush();

        // Test URL redirection after the Onhand was created
        expect($state.go).toHaveBeenCalledWith('onhands.view', {
          onhandId: mockOnhand._id
        });
      }));

      it('should set $scope.vm.error if error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/onhands', sampleOnhandPostData).respond(400, {
          message: errorMessage
        });

        $scope.vm.save(true);
        $httpBackend.flush();

        expect($scope.vm.error).toBe(errorMessage);
      });
    });

    describe('vm.save() as update', function () {
      beforeEach(function () {
        // Mock Onhand in $scope
        $scope.vm.onhand = mockOnhand;
      });

      it('should update a valid Onhand', inject(function (OnhandsService) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/onhands\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        $scope.vm.save(true);
        $httpBackend.flush();

        // Test URL location to new object
        expect($state.go).toHaveBeenCalledWith('onhands.view', {
          onhandId: mockOnhand._id
        });
      }));

      it('should set $scope.vm.error if error', inject(function (OnhandsService) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/onhands\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        $scope.vm.save(true);
        $httpBackend.flush();

        expect($scope.vm.error).toBe(errorMessage);
      }));
    });

    describe('vm.remove()', function () {
      beforeEach(function () {
        //Setup Onhands
        $scope.vm.onhand = mockOnhand;
      });

      it('should delete the Onhand and redirect to Onhands', function () {
        //Return true on confirm message
        spyOn(window, 'confirm').and.returnValue(true);

        $httpBackend.expectDELETE(/api\/onhands\/([0-9a-fA-F]{24})$/).respond(204);

        $scope.vm.remove();
        $httpBackend.flush();

        expect($state.go).toHaveBeenCalledWith('onhands.list');
      });

      it('should should not delete the Onhand and not redirect', function () {
        //Return false on confirm message
        spyOn(window, 'confirm').and.returnValue(false);

        $scope.vm.remove();

        expect($state.go).not.toHaveBeenCalled();
      });
    });
  });
})();
