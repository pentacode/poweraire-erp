'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Onhand = mongoose.model('Onhand'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, onhand;

/**
 * Onhand routes tests
 */
describe('Onhand CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Onhand
    user.save(function () {
      onhand = {
        name: 'Onhand name'
      };

      done();
    });
  });

  it('should be able to save a Onhand if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Onhand
        agent.post('/api/onhands')
          .send(onhand)
          .expect(200)
          .end(function (onhandSaveErr, onhandSaveRes) {
            // Handle Onhand save error
            if (onhandSaveErr) {
              return done(onhandSaveErr);
            }

            // Get a list of Onhands
            agent.get('/api/onhands')
              .end(function (onhandsGetErr, onhandsGetRes) {
                // Handle Onhand save error
                if (onhandsGetErr) {
                  return done(onhandsGetErr);
                }

                // Get Onhands list
                var onhands = onhandsGetRes.body;

                // Set assertions
                (onhands[0].user._id).should.equal(userId);
                (onhands[0].name).should.match('Onhand name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Onhand if not logged in', function (done) {
    agent.post('/api/onhands')
      .send(onhand)
      .expect(403)
      .end(function (onhandSaveErr, onhandSaveRes) {
        // Call the assertion callback
        done(onhandSaveErr);
      });
  });

  it('should not be able to save an Onhand if no name is provided', function (done) {
    // Invalidate name field
    onhand.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Onhand
        agent.post('/api/onhands')
          .send(onhand)
          .expect(400)
          .end(function (onhandSaveErr, onhandSaveRes) {
            // Set message assertion
            (onhandSaveRes.body.message).should.match('Please fill Onhand name');

            // Handle Onhand save error
            done(onhandSaveErr);
          });
      });
  });

  it('should be able to update an Onhand if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Onhand
        agent.post('/api/onhands')
          .send(onhand)
          .expect(200)
          .end(function (onhandSaveErr, onhandSaveRes) {
            // Handle Onhand save error
            if (onhandSaveErr) {
              return done(onhandSaveErr);
            }

            // Update Onhand name
            onhand.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Onhand
            agent.put('/api/onhands/' + onhandSaveRes.body._id)
              .send(onhand)
              .expect(200)
              .end(function (onhandUpdateErr, onhandUpdateRes) {
                // Handle Onhand update error
                if (onhandUpdateErr) {
                  return done(onhandUpdateErr);
                }

                // Set assertions
                (onhandUpdateRes.body._id).should.equal(onhandSaveRes.body._id);
                (onhandUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Onhands if not signed in', function (done) {
    // Create new Onhand model instance
    var onhandObj = new Onhand(onhand);

    // Save the onhand
    onhandObj.save(function () {
      // Request Onhands
      request(app).get('/api/onhands')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Onhand if not signed in', function (done) {
    // Create new Onhand model instance
    var onhandObj = new Onhand(onhand);

    // Save the Onhand
    onhandObj.save(function () {
      request(app).get('/api/onhands/' + onhandObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', onhand.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Onhand with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/onhands/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Onhand is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Onhand which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Onhand
    request(app).get('/api/onhands/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Onhand with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Onhand if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Onhand
        agent.post('/api/onhands')
          .send(onhand)
          .expect(200)
          .end(function (onhandSaveErr, onhandSaveRes) {
            // Handle Onhand save error
            if (onhandSaveErr) {
              return done(onhandSaveErr);
            }

            // Delete an existing Onhand
            agent.delete('/api/onhands/' + onhandSaveRes.body._id)
              .send(onhand)
              .expect(200)
              .end(function (onhandDeleteErr, onhandDeleteRes) {
                // Handle onhand error error
                if (onhandDeleteErr) {
                  return done(onhandDeleteErr);
                }

                // Set assertions
                (onhandDeleteRes.body._id).should.equal(onhandSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Onhand if not signed in', function (done) {
    // Set Onhand user
    onhand.user = user;

    // Create new Onhand model instance
    var onhandObj = new Onhand(onhand);

    // Save the Onhand
    onhandObj.save(function () {
      // Try deleting Onhand
      request(app).delete('/api/onhands/' + onhandObj._id)
        .expect(403)
        .end(function (onhandDeleteErr, onhandDeleteRes) {
          // Set message assertion
          (onhandDeleteRes.body.message).should.match('User is not authorized');

          // Handle Onhand error error
          done(onhandDeleteErr);
        });

    });
  });

  it('should be able to get a single Onhand that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Onhand
          agent.post('/api/onhands')
            .send(onhand)
            .expect(200)
            .end(function (onhandSaveErr, onhandSaveRes) {
              // Handle Onhand save error
              if (onhandSaveErr) {
                return done(onhandSaveErr);
              }

              // Set assertions on new Onhand
              (onhandSaveRes.body.name).should.equal(onhand.name);
              should.exist(onhandSaveRes.body.user);
              should.equal(onhandSaveRes.body.user._id, orphanId);

              // force the Onhand to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Onhand
                    agent.get('/api/onhands/' + onhandSaveRes.body._id)
                      .expect(200)
                      .end(function (onhandInfoErr, onhandInfoRes) {
                        // Handle Onhand error
                        if (onhandInfoErr) {
                          return done(onhandInfoErr);
                        }

                        // Set assertions
                        (onhandInfoRes.body._id).should.equal(onhandSaveRes.body._id);
                        (onhandInfoRes.body.name).should.equal(onhand.name);
                        should.equal(onhandInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Onhand.remove().exec(done);
    });
  });
});
