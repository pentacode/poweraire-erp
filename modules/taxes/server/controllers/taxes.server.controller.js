'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Tax = mongoose.model('Tax'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Tax
 */
exports.create = function(req, res) {
  var tax = new Tax(req.body);
  tax.user = req.user;

  tax.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(tax);
    }
  });
};

/**
 * Show the current Tax
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var tax = req.tax ? req.tax.toJSON() : {};

  res.jsonp(tax);
};

/**
 * Update a Tax
 */
exports.update = function(req, res) {
  var tax = req.tax;
  tax.user = req.user;

  tax = _.extend(tax , req.body);

  tax.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(tax);
    }
  });
};

/**
 * List of Taxes
 */
exports.list = function(req, res) { 
  var query = {};
  if (req.query.code) {
    query.code = new RegExp(req.query.code, 'i');
  }
  if (req.query.type) {
    query.type = req.query.type;  
  }
  if (!!req.query.status) {
    query.status = req.query.status;
  }
  Tax.find(query)
    .populate({
      path : 'accountCode',
      model : 'Combination',
      populate : {
        path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
        select : 'descriptionOne',
        model : 'Reference'
      }
    })
  .populate({
    path : 'remitToAccount',
    model : 'Combination',
    populate : {
      path: 'company location serviceType majorAccount minorAccount reservedOne reservedTwo',
      select : 'descriptionOne',
      model : 'Reference'
    }
  })
  .populate('remitToSupplier')
  .sort('code').exec(function(err, taxes) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(taxes);
    }
  });
};

/**
 * Tax middleware
 */
exports.taxByID = function(req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Tax is invalid'
    });
  }

  Tax.findById(id).populate('createdBy').populate('updatedBy').exec(function (err, tax) {
    if (err) {
      return next(err);
    } else if (!tax) {
      return res.status(404).send({
        message: 'No Tax with that identifier has been found'
      });
    }
    req.tax = tax;
    next();
  });
};
