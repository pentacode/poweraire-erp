'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tax Schema
 */
var TaxSchema = new Schema({
  type: {
    type: String, // TODO make this enum
    required: 'Tax type is required'
  },
  code: {
    type: String,
    required: 'Tax code is required'
  },
  description: {
    type: String,
    default: ''
  },
  rate: {
    type: Number,
    min: [0, 'Tax rate cannot be negative'],
    required: 'Tax rate is required'
  },
  status: {
    type: Boolean,
    default: false
  },
  remitToSupplier: {
    type: Schema.ObjectId,
    ref: 'Supplier'
  },
  remitToAccount: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  accountCode: {
    type: Schema.ObjectId,
    ref: 'Combination'
  },
  disabled: Date,
  created: Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updated: Date,
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

TaxSchema.pre('save', function(next) {
  var currentDate = new Date();
  var user = this.user;
  this.updated = currentDate; 
  this.updatedBy = user;
  if(!this.created){
    this.created = currentDate;
    this.createdBy = user;
  }
  if (this.status) {
    this.disabled = null;
  } else if (!this.disabled) {
    this.disabled = currentDate;
  }
  next();
});

mongoose.model('Tax', TaxSchema);
