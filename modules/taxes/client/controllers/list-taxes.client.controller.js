(function () {
  'use strict';

  angular
    .module('taxes')
    .controller('TaxesListController', TaxesListController);

  TaxesListController.$inject = ['$scope', '$filter', 'TaxesService'];

  function TaxesListController($scope, $filter, TaxesService) {
    TaxesService.query(function (data) {
      $scope.taxes = data;
      $scope.codes = [];
      for (var i = 0; i < $scope.taxes.length; i++) {
        var tax = $scope.taxes[i];
        if ($scope.codes.indexOf(tax.code)) {
          $scope.codes.push(tax.code);
        }
        var combination = tax.remitToAccount;
        if (!combination)
          continue;
        combination = generateAccountDesc(combination);
        
        var accntCode = tax.accountCode;
        if (!accntCode)
          continue;
        accntCode = generateAccountDesc(accntCode);
        
      }
      $scope.buildPager();
    });
    
    function generateAccountDesc(combination){
      combination.accountDescription = 'N/A';
      if (combination.company) {
        combination.accountDescription = combination.company.descriptionOne;
      }
      if (combination.location) {
        combination.accountDescription += '-' + combination.location.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      if (combination.serviceType) {
        combination.accountDescription += '-' + combination.serviceType.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      if (combination.majorAccount) {
        combination.accountDescription += '-' + combination.majorAccount.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      if (combination.minorAccount) {
        combination.accountDescription += '-' + combination.minorAccount.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      if (combination.reservedOne) {
        combination.accountDescription += '-' + combination.reservedOne.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      if (combination.reservedTwo) {
        combination.accountDescription += '-' + combination.reservedTwo.descriptionOne;
      } else {
        combination.accountDescription += '-' + 'N/A';
      }
      return combination;
    };
      
    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };
      
    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.taxes, function(tax){
        if ($scope.code && !(new RegExp('^' + $scope.code).test(tax.code))) { //prefix search
          return false;
        }
        if ($scope.type && !(new RegExp($scope.type).test(tax.type))) {
          return false;
        }
        if ($scope.status && !(new RegExp($scope.status).test(tax.status))) {
          return false;
        } 
        return true;
      });
        
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.clear = function () {
      $scope.code = '';
      $scope.type = '';
      $scope.status = '';
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.searchCode = function (){
      $scope.cs = [];
      $scope.cs = $filter('filter')($scope.codes, function(code){
        if ($scope.code && !(new RegExp($scope.code, 'i').test(code))) {
          return false;
        }
        return true;
      });
    };
  }
})();
