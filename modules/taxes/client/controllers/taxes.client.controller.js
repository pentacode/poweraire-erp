(function () {
  'use strict';

  // Taxes controller
  angular
    .module('taxes')
    .controller('TaxesController', TaxesController);

  TaxesController.$inject = ['$scope', '$filter', '$state', 'Authentication', 'taxResolve','SuppliersService','alertify','CombinationsService', 'Util', '$modal'];

  function TaxesController ($scope, $filter, $state, Authentication, tax, SuppliersService, alertify, CombinationsService, Util, $modal) {
    var vm = this;

    vm.authentication = Authentication;
    vm.tax = tax;
    vm.error = null;
    vm.supplier = [];
    SuppliersService.query({ status : true }, function(supplier){
      supplier.forEach(function(supp){
        vm.supplier.push(supp);
        if(supp.name === 'Bureau of Internal Revenue'){
          vm.tax.remitToSupplier = supp._id;    
        }
      });
    });
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.accountCodes = [];

    vm.combinations = CombinationsService.query({ status : true }, function(combinations){
      combinations.forEach(function(combination){
        vm.accountCodes.push(combination);
        if(combination.company.descriptionOne === "BIR"){
          vm.tax.remitToAccount = combination._id;  
        }
      });
    });

    vm.openActionCodeLookup = openActionCodeLookup;

    
    function openActionCodeLookup(index){
       var modalInstance = $modal.open({
          animation : true,
          templateUrl: 'modules/combinations/client/views/combination-lookup.client.view.html',
          controller : 'CombinationLookupController',
          controllerAs: 'vm',
          size : 'lg',
          resolve : {
            combinations : function(){
                return vm.combinations;
            },
            index : index
          }
       });

       modalInstance.result.then(function (selected) {
          vm.tax.accountCode = selected.combination._id;
        }, null);

    }

    $scope.$on('$stateChangeStart', function(event, toState) {
        if(vm.form.taxForm.$dirty){
          event.preventDefault();
          Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
            vm.form.taxForm.$dirty = false;
            $state.go(toState);
          });
        }
    });

    // Remove existing Tax
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.tax.$remove($state.go('taxes.list'));
      }
    }

    // Save Tax
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.taxForm');
        return false;
      }

      var isCreate = vm.tax._id ? false : true;
      if (isCreate) {
        vm.tax.$save(successCallback, errorCallback);
      } else {
        vm.tax.$update(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.success = 'Tax successfully ';
        if (isCreate) {
          vm.success = vm.success + 'created';
        } else {
          vm.success = vm.success + 'updated';
        }
        vm.form.taxForm.$dirty = false;
        alertify.success(vm.success);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        alertify.error(vm.error);
      }
    }
  }
})();
