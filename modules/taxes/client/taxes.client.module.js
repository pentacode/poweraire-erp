(function (app) {
  'use strict';

  app.registerModule('taxes');
  app.registerModule('taxes.admin', ['core.admin']);
})(ApplicationConfiguration);
