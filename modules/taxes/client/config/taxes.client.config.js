(function () {
  'use strict';

  angular
    .module('taxes.admin')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addSubMenuItem('topbar', 'maintenance', {
      title: 'Taxes',
      state: 'taxes.list',
      roles: ['Taxes']
    });
  }
})();
