(function () {
  'use strict';

  // Payments controller
  angular
    .module('payments')
    .controller('PaymentsController', PaymentsController);

  PaymentsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'paymentResolve', 'BanksService', 'Util', 'InvoicesService'];

  function PaymentsController ($scope, $state, $window, Authentication, payment, BanksService, Util, InvoicesService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.payment = payment;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.autoFill = autoFill;
    vm.banks = BanksService.query({status:'Active'});
    vm.openPaymentDate = openPaymentDate;
    vm.addLine = addLine;
    vm.invoices = InvoicesService.query({});

    init();

    $scope.$on('$stateChangeStart', function(event, toState) {
      if(vm.form.paymentForm.$dirty){
        event.preventDefault();
        Util.confirmationBox('','There are unsaved changes. Are you sure you want to leave this page?', function(){
          vm.form.paymentForm.$dirty = false;
          $state.go(toState);
        });
      } 
    });

    vm.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    vm.paymentDate = {
      opened: false
    };

    function init(){
      if(!vm.payment._id){
        vm.payment.lines = [];
      }

      _.forEach(vm.payment.lines, function(line){
        autoFill(line.invoice);
      })
      
    }

    function autoFill(invoice, idx){
      if(invoice){
        vm.payment.lines[idx].total = invoice.amount;
        vm.payment.lines[idx].remaining = (invoice.amount - invoice.amountPaid);
        vm.payment.lines[idx].paid = invoice.amountPaid;
      }
    }

    function openPaymentDate(){
      vm.paymentDate.opened = true;
    }

    function addLine(){
      vm.payment.lines.push({
        invoice: null,
        total: null,
        ramining: null,
        paid: null
      });
    };

    // Remove existing Payment
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.payment.$remove($state.go('payments.list'));
      }
    }

    // Save Payment
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.paymentForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.payment._id) {
        vm.payment.$update(successCallback, errorCallback);
      } else {
        vm.payment.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('payments.view', {
          paymentId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
