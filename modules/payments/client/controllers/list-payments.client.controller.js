(function () {
  'use strict';

  angular
    .module('payments')
    .controller('PaymentsListController', PaymentsListController);

  PaymentsListController.$inject = ['PaymentsService'];

  function PaymentsListController(PaymentsService) {
    var vm = this;
    vm.clear = clear;
    vm.openDateFrom = openDateFrom;
    vm.openDateTo= openDateTo;
    vm.payments = PaymentsService.query();

	function clear() {
		vm.supplier = '';
		vm.number = '';
		vm.dateFrom = '';
		vm.dateTo = '';
		vm.invoiceNumber = '';
		vm.payor = '';
		// figureOutItemsToDisplay();
	}

	function openDateFrom($event) {
		vm.paymentDateFrom.opened = true;
	}

	function openDateTo($event) {
		vm.paymentDateTo.opened = true;
	}

	vm.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};

	vm.paymentDateFrom = {
		opened: false
	};

	vm.paymentDateTo = {
		opened: false
	};
  }
}());
