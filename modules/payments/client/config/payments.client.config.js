(function () {
  'use strict';

  angular
    .module('payments')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'accountsPayable', {
      title: 'Payments',
      state: 'payments.list',
      roles: ['Payments']
    });
  }
}());
