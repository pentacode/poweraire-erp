'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Payment Schema
 */
var PaymentSchema = new Schema({
  number: String,
  date: Date,
  bank: {
    type: Schema.ObjectId,
    ref: 'Bank'
  },
  checkSeriesName: String,
  documentNumber: Number,
  total: Number,
  lines:[{
    invoice:{
      type: Schema.ObjectId,
      ref: 'Invoice'
    },
    paidAmount: Number,
    remaining: Number
  }],
  distributions:[{
    debit: {
      type: Schema.ObjectId,
      ref: 'Supplier'
    },
    credit: {
      type: Schema.ObjectId,
      ref: 'Bank'
    }
  }],
  created: Date,
  updated : Date,
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  } 
});


PaymentSchema.pre('save', function(next) {
  var payment = this;
  var currentDate = new Date();
  var user = payment.user;
  payment.updated = currentDate;
  payment.updatedBy = user;
  if (!payment.created){
    payment.created = currentDate;
    payment.createdBy = user;
    sequenceModel.findOneAndUpdate({ model : 'Payment' }, { $inc: { value : 1 } }, function(err, sequence) {
      if (err) {
        console.log(err);
        return next(err);
      }
      payment.number = sequence.value;
      next();
    });
  } else {
    next();
  }
});

mongoose.model('Payment', PaymentSchema);
