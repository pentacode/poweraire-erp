'use strict';

var mongoose = require('./config/lib/mongoose.js'),
  seed = require('./config/lib/seed.js');

mongoose.connect(function() {
  mongoose.loadModels(function() {
    seed.start();
  });
});